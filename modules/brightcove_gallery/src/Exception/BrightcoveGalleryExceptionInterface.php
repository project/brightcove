<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Entity\Exception;

use Drupal\brightcove\Exception\BrightcoveExceptionInterface;

/**
 * Module specific base exception.
 */
interface BrightcoveGalleryExceptionInterface extends BrightcoveExceptionInterface {}
