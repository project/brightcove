<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access control handler for Brightcove In-Page Experience entity.
 */
final class InPageExperienceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  public function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view brightcove gallery in-page experience entity');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete brightcove gallery in-page experience entity');

      default:
        return parent::checkAccess($entity, $operation, $account);
    }
  }

}
