<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery;

use Drupal\brightcove_gallery\Entity\InPageExperience;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * In-Page Experience entity list builder.
 */
final class InPageExperienceListBuilder extends EntityListBuilder {

  /**
   * API Client storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $apiClientStorage;

  /**
   * Link generator.
   *
   * @var \Drupal\Core\Utility\LinkGeneratorInterface
   */
  private $linkGenerator;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  private $dateFormatter;

  /**
   * Initializes an In-Page Experience list builder.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   Entity storage.
   * @param \Drupal\Core\Entity\EntityStorageInterface $api_client_storage
   *   API Client storage.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Date formatter.
   * @param \Drupal\Core\Utility\LinkGeneratorInterface $link_generator
   *   Link generator.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String translation.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    EntityStorageInterface $api_client_storage,
    DateFormatterInterface $date_formatter,
    LinkGeneratorInterface $link_generator,
    TranslationInterface $string_translation,
  ) {
    parent::__construct($entity_type, $storage);

    $this->apiClientStorage = $api_client_storage;
    $this->dateFormatter = $date_formatter;
    $this->linkGenerator = $link_generator;
    $this->stringTranslation = $string_translation;

    // Pager not supported as of now.
    $this->limit = 0;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type): self {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('entity_type.manager')->getStorage('brightcove_api_client'),
      $container->get('date.formatter'),
      $container->get('link_generator'),
      $container->get('string_translation'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds(): array {
    $headers = $this->buildHeader();
    $query = $this->getStorage()->getQuery()
      ->accessCheck()
      ->tableSort($headers);

    if ($this->limit) {
      $query->pager($this->limit);
    }

    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $row = [
      'name' => [
        'data' => $this->t('Name'),
        'field' => 'name',
        'specifier' => 'name',
        'sort' => 'asc',
      ],
      'status' => [
        'data' => $this->t('Status'),
        'field' => 'published_status',
        'specifier' => 'publishedStatus',
      ],
      'api_client' => $this->t('API Client'),
      'created' => [
        'data' => $this->t('Created'),
        'field' => 'created_at',
        'specifier' => 'createdAt',
      ],
      'updated' => [
        'data' => $this->t('Updated'),
        'field' => 'updated_at',
        'specifier' => 'updatedAt',
      ],
      'id' => [
        'data' => $this->t('ID'),
        'field' => 'id',
        'specifier' => 'id',
      ],
    ];

    return $row + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\brightcove_gallery\Entity\InPageExperienceInterface $entity */

    $api_client = $this->apiClientStorage->load($entity->getApiClientId());
    if (!empty($api_client)) {
      $api_client_column = $api_client->access('edit') ? $this->linkGenerator->generate($api_client->label(), $api_client->toUrl('edit-form')) : '';
    }
    else {
      $api_client_column = $this->t('Invalid API Client');
    }

    $row = [
      'name' => $this->linkGenerator->generate($entity->getName(), $entity->toUrl('canonical')),
      'status' => InPageExperience::getPublishedStatusAllowedValues($this->stringTranslation)[$entity->getPublishedStatus()] ?? $this->t('Unknown'),
      'api_client' => $api_client_column,
      'created' => $this->dateFormatter->format($entity->getCreatedAt()),
      'updated' => $this->dateFormatter->format($entity->getUpdatedAt()),
      'id' => $entity->id(),
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity): array {
    $operations = [
      'view' => [
        'title' => $this->t('View'),
        'url' => $entity->toUrl('canonical'),
      ],
    ];

    return $operations + parent::getDefaultOperations($entity);
  }

}
