<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery;

/**
 * Stores module specific constant.
 */
final class Module {

  /**
   * The name of the module.
   */
  public const NAME = 'brightcove_gallery';

}
