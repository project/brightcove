<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Query;

use Drupal\brightcove_gallery\Services\InPageExperienceApiInterface;
use Drupal\brightcove_gallery\Services\InPageExperienceCacheInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryAggregateInterface;
use Drupal\Core\Entity\Query\QueryException;
use Drupal\Core\Entity\Query\QueryFactoryInterface;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Query factory for In-Page Experience remote entities.
 */
final class QueryFactory implements QueryFactoryInterface {

  /**
   * In-Page Experience entity cache.
   *
   * @var \Drupal\brightcove_gallery\Services\InPageExperienceCacheInterface
   */
  private $cache;

  /**
   * In-Page Experience API.
   *
   * @var \Drupal\brightcove_gallery\Services\InPageExperienceApiInterface
   */
  private $inPageExperienceApi;

  /**
   * List of namespaces.
   *
   * @var string[]
   */
  private $namespaces;

  /**
   * Initializes an In-Page Experience query factory.
   *
   * @param \Drupal\brightcove_gallery\Services\InPageExperienceCacheInterface $cache
   *   Cache.
   * @param \Drupal\brightcove_gallery\Services\InPageExperienceApiInterface $in_page_experience_api
   *   In-Page Experience API.
   */
  public function __construct(InPageExperienceCacheInterface $cache, InPageExperienceApiInterface $in_page_experience_api) {
    $this->cache = $cache;
    $this->inPageExperienceApi = $in_page_experience_api;

    $this->namespaces = Query::getNamespaces($this);
  }

  /**
   * {@inheritdoc}
   */
  public function get(EntityTypeInterface $entity_type, $conjunction): QueryInterface {
    return new Query(
      $entity_type,
      $conjunction,
      $this->namespaces,
      $this->cache,
      $this->inPageExperienceApi
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getAggregate(EntityTypeInterface $entity_type, $conjunction): QueryAggregateInterface {
    throw new QueryException('Aggregation over Brightcove In-Page Experience entity storage is not supported');
  }

}
