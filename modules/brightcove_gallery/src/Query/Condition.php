<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Query;

use Drupal\Core\Entity\Query\ConditionBase;
use Drupal\Core\Entity\Query\ConditionInterface;
use Drupal\Core\Entity\Query\QueryException;

/**
 * In-Page Experience condition.
 */
final class Condition extends ConditionBase {

  /**
   * {@inheritdoc}
   */
  public function exists($field, $langcode = NULL): ConditionInterface {
    throw new QueryException('Exists condition not supported.');
  }

  /**
   * {@inheritdoc}
   */
  public function notExists($field, $langcode = NULL): ConditionInterface {
    throw new QueryException('No exists condition not supported.');
  }

  /**
   * {@inheritdoc}
   */
  public function compile($query): array {
    $conditions = [];

    foreach ($this->conditions as $condition) {
      if (!isset($conditions['api_client']) && $condition['field'] == 'api_client') {
        $conditions['api_client'] = $condition['value'];
      }
      elseif (!isset($condition['q']) && in_array($condition['field'], [
        'q',
        'name',
      ])) {
        $conditions['q'] = $condition['value'];
      }
    }

    return $conditions;
  }

}
