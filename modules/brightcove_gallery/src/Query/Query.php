<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Query;

use Brightcove\Type\Sort;
use Brightcove\Type\SortInterface;
use Drupal\brightcove_gallery\Services\InPageExperienceApiInterface;
use Drupal\brightcove_gallery\Services\InPageExperienceCacheInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryBase;
use Drupal\Core\Entity\Query\QueryException;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Query handler for remote In-Page Experience entities.
 */
final class Query extends QueryBase {

  /**
   * In-Page Experience cache.
   *
   * @var \Drupal\brightcove_gallery\Services\InPageExperienceCacheInterface
   */
  private $cache;

  /**
   * In-Page Experience API handler.
   *
   * @var \Drupal\brightcove_gallery\Services\InPageExperienceApiInterface
   */
  private $inPageExperienceApi;

  /**
   * Initializes an In-Page Experience query handler.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Entity type.
   * @param string $conjunction
   *   Conjunction.
   * @param array $namespaces
   *   Namespaces.
   * @param \Drupal\brightcove_gallery\Services\InPageExperienceCacheInterface $cache
   *   Cache.
   * @param \Drupal\brightcove_gallery\Services\InPageExperienceApiInterface $in_page_experience_api
   *   In-Page Experience API.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    $conjunction,
    array $namespaces,
    InPageExperienceCacheInterface $cache,
    InPageExperienceApiInterface $in_page_experience_api,
  ) {
    parent::__construct($entity_type, $conjunction, $namespaces);

    $this->cache = $cache;
    $this->inPageExperienceApi = $in_page_experience_api;
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $conditions = $this->condition->compile($this);

    $api_clients = NULL;
    if (isset($conditions['api_client'])) {
      $api_clients = [$conditions['api_client']];
      unset($conditions['api_client']);
    }

    // Get only the first sort value (if set) as it is possible to sort by one
    // field at a time.
    $sort = [];
    if (!empty($this->sort)) {
      $sort = reset($this->sort);
      $sort['sort'] = new Sort($sort['field'], $sort['direction']);
    }

    $in_page_experiences = $this->inPageExperienceApi->getAll($conditions + $sort, $api_clients);

    // As there is no way to get only an ordered list of IDs by a specific
    // property let's cache every loaded entity immediately.
    $this->cache->set($in_page_experiences);

    // Get and return entity IDs.
    $entity_ids = array_keys($in_page_experiences);
    if (!empty($this->range)) {
      $entity_ids = array_slice($entity_ids, $this->range['start'], $this->range['length'], TRUE);
    }
    if ($this->count) {
      return count($entity_ids);
    }
    return $entity_ids;
  }

  /**
   * {@inheritdoc}
   */
  public function sort($field, $direction = 'ASC', $langcode = NULL) {
    // Translate direction for the API.
    return parent::sort($field, strtolower($direction) === 'asc' ? SortInterface::ORDER_DEFAULT : SortInterface::ORDER_REVERSE);
  }

  /**
   * {@inheritdoc}
   */
  public function currentRevision(): QueryInterface {
    throw new QueryException('Revisions are not supported.');
  }

  /**
   * {@inheritdoc}
   */
  public function latestRevision(): QueryInterface {
    throw new QueryException('Revisions are not supported.');
  }

  /**
   * {@inheritdoc}
   */
  public function allRevisions(): QueryInterface {
    throw new QueryException('Revisions are not supported.');
  }

  /**
   * {@inheritdoc}
   */
  public function pager($limit = 10, $element = NULL): QueryInterface {
    throw new QueryException('Pager is not supported.');
  }

  /**
   * {@inheritdoc}
   */
  public function groupBy($field, $langcode = NULL): QueryInterface {
    throw new QueryException('Group by is not supported.');
  }

}
