<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery;

use Drupal\brightcove_gallery\Entity\InPageExperienceInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity handler.
 */
final class EntityHandler implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * API Client storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $apiClientStorage;

  /**
   * Initializes an entity extra field info handler.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String translation.
   * @param \Drupal\Core\Entity\EntityStorageInterface $api_client_storage
   *   API Client storage.
   */
  public function __construct(TranslationInterface $string_translation, EntityStorageInterface $api_client_storage) {
    $this->stringTranslation = $string_translation;
    $this->apiClientStorage = $api_client_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('string_translation'),
      $container->get('entity_type.manager')->getStorage('brightcove_api_client')
    );
  }

  /**
   * Handles the entity extra field info hook callback.
   *
   * @return array
   *   The extra field definitions for the entity.
   *
   * @see \hook_entity_extra_field_info()
   * @see \brightcove_gallery_entity_extra_field_info()
   */
  public function extraFieldInfo(): array {
    return [
      'brightcove_in_page_experience' => [
        'brightcove_in_page_experience' => [
          'display' => [
            'brightcove_in_page_experience_preview' => [
              'label' => $this->t('Preview'),
              'description' => $this->t('Preview of the In-Page Experience.'),
              'weight' => -1000,
              'visible' => TRUE,
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * Handles entity view for In-Page Experience.
   *
   * @param array &$build
   *   A renderable array representing the entity content.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display holding the display options configured for the
   *   entity components.
   * @param string $view_mode
   *   The view mode the entity is rendered in.
   *
   * @see \hook_ENTITY_TYPE_view()
   * @see \brightcove_gallery_brightcove_in_page_experience_view()
   */
  public function inPageExperienceView(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, string $view_mode): void {
    if ($entity instanceof InPageExperienceInterface && !empty($display->getComponent('brightcove_in_page_experience_preview'))) {
      $build['brightcove_in_page_experience_preview'] = [
        '#theme' => 'brightcove_in_page_experience_preview',
        '#account_id' => $this->apiClientStorage->load($entity->getApiClientId())->getAccountId(),
        '#id' => $entity->id(),
      ];
    }
  }

}
