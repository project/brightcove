<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Form;

use Drupal\brightcove_gallery\Entity\Storage\InPageExperienceStorageInterface;
use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityDeleteFormTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Delete confirmation form builder for In-Page Experience entity.
 */
final class InPageExperienceDelete extends EntityConfirmFormBase {

  use EntityDeleteFormTrait {
    getCancelUrl as getDeleteCancelUrl;
    getQuestion as getDeleteQuestion;
  }

  /**
   * In-Page Experience storage.
   *
   * @var \Drupal\brightcove_gallery\Entity\Storage\InPageExperienceStorageInterface
   */
  private $inPageExperienceStorage;

  /**
   * Initializes an In-Page Experience entity delete confirmation form builder.
   *
   * @param \Drupal\brightcove_gallery\Entity\Storage\InPageExperienceStorageInterface $in_page_experience_storage
   *   In-Page Experience entity storage.
   */
  public function __construct(InPageExperienceStorageInterface $in_page_experience_storage) {
    $this->inPageExperienceStorage = $in_page_experience_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity_type.manager')->getStorage('brightcove_in_page_experience')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->getDeleteQuestion();
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return $this->getDeleteCancelUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    // Do not attach fields to the confirm form.
    return $form;
  }

}
