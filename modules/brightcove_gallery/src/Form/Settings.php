<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Form;

use Drupal\brightcove_gallery\Services\InPageExperienceSettingsInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form builder for In-Page Experience.
 */
final class Settings extends ConfigFormBase {

  /**
   * Initializes a settings form builder.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\brightcove_gallery\Services\InPageExperienceSettingsInterface $settings
   *   Settings handler.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    typedConfigManagerInterface $typedConfigManager,
    private readonly InPageExperienceSettingsInterface $settings,
  ) {
    parent::__construct($configFactory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('brightcove_gallery.settings.in_page_experience'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      InPageExperienceSettingsInterface::STORAGE . '.' . InPageExperienceSettingsInterface::SUB_SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'brightcove_gallery.settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['cache_seconds'] = [
      '#type' => 'number',
      '#title' => $this->t('Cache for'),
      '#field_suffix' => $this->t('seconds'),
      '#min' => -1,
      '#required' => TRUE,
      '#description' => $this->t('Cache entities until the given seconds.<br>If <strong>-1</strong> is given the entities will be cached until the next cache rebuild.'),
      '#default_value' => $this->settings->getCacheSeconds(),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->settings->setCacheSeconds((int) $form_state->getValue('cache_seconds'));
    parent::submitForm($form, $form_state);
  }

}
