<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Entity\Storage;

use Brightcove\API\Exception\APIException;
use Drupal\brightcove\BrightcoveUtil;
use Drupal\brightcove_gallery\Entity\InPageExperienceInterface;
use Drupal\brightcove_gallery\Entity\Storage\Exception\InPageExperienceStorageException;
use Drupal\brightcove_gallery\Services\InPageExperienceApiInterface;
use Drupal\brightcove_gallery\Services\InPageExperienceCacheInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * In-Page Experience entity storage.
 */
final class InPageExperienceStorage extends EntityStorageBase implements InPageExperienceStorageInterface {

  /**
   * API Client storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $apiClientStorage;

  /**
   * Cache backend.
   *
   * @var \Drupal\brightcove_gallery\Services\InPageExperienceCacheInterface
   */
  private $cache;

  /**
   * In-Page Experience API.
   *
   * @var \Drupal\brightcove_gallery\Services\InPageExperienceApiInterface
   */
  private $inPageExperienceApi;

  /**
   * Initializes an In-Page Experience storage.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $memory_cache
   *   The memory cache backend.
   * @param \Drupal\Core\Entity\EntityStorageInterface $api_client_storage
   *   API Client storage.
   * @param \Drupal\brightcove_gallery\Services\InPageExperienceCacheInterface $cache
   *   The cache backend to be used.
   * @param \Drupal\brightcove_gallery\Services\InPageExperienceApiInterface $in_page_experience_api
   *   In-Page Experience API.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    MemoryCacheInterface $memory_cache,
    EntityStorageInterface $api_client_storage,
    InPageExperienceCacheInterface $cache,
    InPageExperienceApiInterface $in_page_experience_api,
  ) {
    parent::__construct($entity_type, $memory_cache);

    $this->apiClientStorage = $api_client_storage;
    $this->cache = $cache;
    $this->inPageExperienceApi = $in_page_experience_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type): self {
    return new static(
      $entity_type,
      $container->get('entity.memory_cache'),
      $container->get('entity_type.manager')->getStorage('brightcove_api_client'),
      $container->get('brightcove_gallery.in_page_experience.cache'),
      $container->get('brightcove_gallery.in_page_experience.api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRevision($revision_id) {
    // Revisions are not supported.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function doCreate(array $values): InPageExperienceInterface {
    $entity_class = $this->getEntityClass();
    return new $entity_class(
      $values['api_client'],
      $values['id'],
      $values['name'],
      $values['description'],
      $values['createdAt'],
      $values['updatedAt'],
      $values['publishedStatus'],
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function doDelete($entities): void {
    /** @var \Drupal\brightcove_gallery\Entity\InPageExperienceInterface[] $entities */
    foreach ($entities as $entity) {
      if (!$entity->isNew()) {
        try {
          $this->inPageExperienceApi->delete($entity);
        }
        catch (APIException $e) {
          // Skip if the In-Page Experience not found remotely, otherwise
          // re-throw the exception.
          if ($e->getCode() !== 404) {
            throw $e;
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function doLoadMultiple(?array $ids = NULL): array {
    $in_page_experiences = [];
    $entities_from_cache = [];

    /** @var \Drupal\brightcove\BrightcoveAPIClientInterface $api_client */
    foreach ($this->apiClientStorage->loadMultiple() as $api_client) {
      $entities_from_cache = $this->cache->get($this->entityTypeId, $api_client->id(), $ids);

      if ($ids === []) {
        // Every requested entities with the given IDs are loaded, no need to
        // load any other.
        break;
      }
      elseif ($ids !== NULL) {
        // Try to load the remaining entities that are not yet cached.
        foreach ($ids as $index => $id) {
          try {
            $in_page_experience = $this->inPageExperienceApi->get($id, $api_client);
            $in_page_experiences[$in_page_experience->id()] = $in_page_experience;
            unset($ids[$index]);
          }
          catch (APIException $e) {
            if ($e->getCode() !== 404) {
              throw $e;
            }
          }
        }
      }
      else {
        $in_page_experiences = $this->inPageExperienceApi->getAll([], [$api_client]);
      }
    }

    if ($in_page_experiences !== []) {
      $this->cache->set($in_page_experiences);
    }

    return $entities_from_cache + $in_page_experiences;
  }

  /**
   * {@inheritdoc}
   */
  protected function doSave($id, EntityInterface $entity) {
    throw new InPageExperienceStorageException('Saving In-Page Experience entity is currently not supported.');
  }

  /**
   * {@inheritdoc}
   */
  protected function has($id, EntityInterface $entity): bool {
    $api_client = BrightcoveUtil::getApiClient($entity->getApiClientId());

    try {
      $this->inPageExperienceApi->get($id, $api_client);
    }
    catch (APIException $e) {
      if ($e->getCode() === 404) {
        return FALSE;
      }
      throw $e;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getQueryServiceName(): string {
    return 'brightcove_gallery.in_page_experience.query';
  }

  /**
   * {@inheritdoc}
   */
  public function loadRevision($revision_id): ?EntityInterface {
    // Revisions are not supported.
    return NULL;
  }

}
