<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Entity\Storage;

use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines In-Page Experience entity storage interface.
 */
interface InPageExperienceStorageInterface extends EntityStorageInterface, EntityHandlerInterface {}
