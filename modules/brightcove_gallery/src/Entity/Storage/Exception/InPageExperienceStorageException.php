<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Entity\Storage\Exception;

use Drupal\brightcove_gallery\Entity\Exception\BrightcoveGalleryExceptionInterface;

/**
 * In-Page Experience storage exception.
 */
final class InPageExperienceStorageException extends \Exception implements BrightcoveGalleryExceptionInterface {}
