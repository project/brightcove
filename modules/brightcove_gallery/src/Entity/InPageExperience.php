<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Entity;

use Drupal\brightcove\BrightcoveAPIClientInterface;
use Drupal\brightcove\Entity\BrightcoveEntity;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Brightcove In-Page Experience entity.
 *
 * @\Drupal\brightcove\Annotation\BrightcoveEntityType(
 *   id = "brightcove_in_page_experience",
 *   label = @Translation("Brightcove In-Page Experience"),
 *   label_collection = @Translation("In-Page Experiences"),
 *   label_singular = @Translation("In-Page Experience"),
 *   label_plural = @Translation("In-Page Experiences"),
 *   label_count = @PluralTranslation(
 *     singular = "@count In-Page Experience",
 *     plural = "@count In-Page Experiences"
 *   ),
 *   handlers = {
 *     "access" = "Drupal\brightcove_gallery\Access\InPageExperienceAccessControlHandler",
 *     "form" = {
 *       "delete" = "Drupal\brightcove_gallery\Form\InPageExperienceDelete",
 *     },
 *     "list_builder" = "Drupal\brightcove_gallery\InPageExperienceListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "storage" = "Drupal\brightcove_gallery\Entity\Storage\InPageExperienceStorage",
 *   },
 *   admin_permission = "administer brightcove gallery in-page experience",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *   },
 *   links = {
 *     "canonical" = "/brightcove-in-page-experience/{brightcove_in_page_experience}",
 *     "collection" = "/admin/content/brightcove-in-page-experiences",
 *     "delete-form" = "/admin/content/brightcove-in-page-experience/{brightcove_in_page_experience}/delete",
 *   },
 *   field_ui_base_route = "brightcove_in_page_experience.settings",
 * )
 */
final class InPageExperience extends BrightcoveEntity implements InPageExperienceInterface {

  /**
   * API Client ID.
   *
   * @var string
   */
  protected $apiClientId;

  /**
   * Created at, timestamp.
   *
   * @var int
   */
  protected $createdAt;

  /**
   * Description.
   *
   * @var string
   */
  protected $description;

  /**
   * ID.
   *
   * @var string
   */
  protected $id;

  /**
   * Name.
   *
   * @var string
   */
  protected $name;

  /**
   * Published status.
   *
   * @var string
   */
  protected $publishedStatus;

  /**
   * Updated at, timestamp.
   *
   * @var int
   */
  protected $updatedAt;

  /**
   * Initializes an In-Page Experience entity.
   *
   * @param \Drupal\brightcove\BrightcoveAPIClientInterface $api_client
   *   API Client.
   * @param string $id
   *   ID.
   * @param string $name
   *   Name.
   * @param string $description
   *   Description.
   * @param int $created_at
   *   Created at, timestamp.
   * @param int $updated_at
   *   Updated at, timestamp.
   * @param string $published_status
   *   Published status.
   */
  public function __construct(
    BrightcoveAPIClientInterface $api_client,
    string $id,
    string $name,
    string $description,
    int $created_at,
    int $updated_at,
    string $published_status,
  ) {
    parent::__construct('brightcove_in_page_experience');

    $this->apiClientId = $api_client->id();
    $this->id = $id;
    $this->name = $name;
    $this->description = $description;
    $this->createdAt = $created_at;
    $this->updatedAt = $updated_at;
    $this->publishedStatus = $published_status;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiClientId(): string {
    return $this->apiClientId;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedAt(): int {
    return $this->createdAt;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getPublishedStatus(): string {
    return $this->publishedStatus;
  }

  /**
   * {@inheritdoc}
   */
  public function getUpdatedAt(): ?int {
    return $this->updatedAt;
  }

  /**
   * {@inheritdoc}
   */
  public function id(): string {
    return $this->getId();
  }

  /**
   * {@inheritdoc}
   */
  public function setApiClientId(string $api_client_id): void {
    $this->apiClientId = $api_client_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedAt(int $created_at): void {
    $this->createdAt = $created_at;
  }

  /**
   * {@inheritdoc}
   */
  public function setPublishedStatus(string $published_status): void {
    $this->publishedStatus = $published_status;
  }

  /**
   * {@inheritdoc}
   */
  public function setUpdateAt(int $updated_at): void {
    $this->updatedAt = $updated_at;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = [];

    // Override the default ID field.
    $fields[$entity_type->getKey('id')] = BaseFieldDefinition::create('string')
      ->setLabel(t('ID'))
      ->setReadOnly(TRUE);

    $fields['apiClientId'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('API Client'))
      ->setDescription(t('Brightcove API credentials (account) to use.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'brightcove_api_client')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setDisplayOptions('view', [
        'type' => 'hidden',
        'region' => 'hidden',
        'label' => 'inline',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields[$entity_type->getKey('label')] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('Name of the experience.'))
      ->setRequired(TRUE)
      ->setSettings([
        'text_processing' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Description'))
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
      ])
      ->setDisplayOptions('view', [
        'type' => 'basic_string',
        'label' => 'above',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['createdAt'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created at'))
      ->setDescription(t('The time of the creation.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['updatedAt'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Update at'))
      ->setDescription(t('The time of the update.'));

    $fields['publishedStatus'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Published status'))
      ->setSetting('allowed_values', static::getPublishedStatusAllowedValues())
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function getPublishedStatusAllowedValues(?TranslationInterface $string_translation = NULL): array {
    if ($string_translation === NULL) {
      $string_translation = \Drupal::translation();
    }

    return [
      'unpublished' => $string_translation->translate('Unpublished'),
      'success' => $string_translation->translate('Success'),
      'publishing' => $string_translation->translate('Publishing'),
      'unpublishing' => $string_translation->translate('Unpublishing'),
      'inactive' => $string_translation->translate('Inactive'),
      'failed' => $string_translation->translate('Failed'),
    ];
  }

}
