<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Entity;

use Drupal\brightcove\Entity\BrightcoveEntityInterface;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Defines interface for an In-Page Experience content entity.
 */
interface InPageExperienceInterface extends BrightcoveEntityInterface {

  /**
   * Gets the related API Client's ID.
   *
   * @return string|null
   *   The related API Client's ID.
   */
  public function getApiClientId(): ?string;

  /**
   * Gets the creation time.
   *
   * @return int|null
   *   The creation time.
   */
  public function getCreatedAt(): ?int;

  /**
   * Gets published status.
   *
   * @return string|null
   *   The published status.
   */
  public function getPublishedStatus(): ?string;

  /**
   * Gets updated at time.
   *
   * @return int|null
   *   The updated at time.
   */
  public function getUpdatedAt(): ?int;

  /**
   * Sets the API Client's target ID.
   *
   * @param string $api_client_id
   *   The API Client target ID.
   */
  public function setApiClientId(string $api_client_id): void;

  /**
   * Sets the creation time.
   *
   * @param int $created_at
   *   The creation time.
   */
  public function setCreatedAt(int $created_at): void;

  /**
   * Sets the published status.
   *
   * @param string $published_status
   *   The published status, can be one of the following:
   *     - unpublished
   *     - success
   *     - publishing
   *     - unpublishing
   *     - inactive
   *     - failed
   *   .
   */
  public function setPublishedStatus(string $published_status): void;

  /**
   * Sets the update time.
   *
   * @param int $updated_at
   *   The update time.
   */
  public function setUpdateAt(int $updated_at): void;

  /**
   * Gets the allowed values for the published status.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface|null $string_translation
   *   The list of allowed published status values.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]
   *   The list of allowed published status values.
   */
  public static function getPublishedStatusAllowedValues(?TranslationInterface $string_translation = NULL): array;

}
