<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Services;

/**
 * In-Page Experience cache interface.
 */
interface InPageExperienceCacheInterface {

  /**
   * Gets entities from the cache backend.
   *
   * @param string $entity_type_id
   *   Entity type ID.
   * @param string $api_client_id
   *   API Client ID.
   * @param array|null &$ids
   *   List of IDs to get. If empty, an empty list will be returned.
   *   The found IDs are being removed from the $ids array.
   *
   * @return \Drupal\brightcove_gallery\Entity\InPageExperienceInterface[]
   *   List of In-Page Experiences entities from the cache.
   */
  public function get(string $entity_type_id, string $api_client_id, ?array &$ids = NULL): array;

  /**
   * Stores entities in the persistent cache backend.
   *
   * @param \Drupal\brightcove_gallery\Entity\InPageExperienceInterface[] $in_page_experiences
   *   In-Page Experiences to store in the cache.
   */
  public function set(array $in_page_experiences): void;

}
