<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Services;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Cache service for In-Page Experience entities.
 */
final class InPageExperienceCache implements InPageExperienceCacheInterface {

  /**
   * Cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cache;

  /**
   * Settings.
   *
   * @var \Drupal\brightcove_gallery\Services\InPageExperienceSettingsInterface
   */
  private $settings;

  /**
   * Time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Initializes a cache service for In-Page Experience entities.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache backend.
   * @param \Drupal\brightcove_gallery\Services\InPageExperienceSettingsInterface $settings
   *   Settings.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time.
   */
  public function __construct(CacheBackendInterface $cache, InPageExperienceSettingsInterface $settings, TimeInterface $time) {
    $this->cache = $cache;
    $this->settings = $settings;
    $this->time = $time;
  }

  /**
   * Helper function to build cache ID.
   *
   * @param string $entity_type_id
   *   Entity type ID.
   * @param string $api_client_id
   *   API Client ID.
   * @param string $id
   *   Entity ID.
   *
   * @return string
   *   Cache ID.
   */
  private function buildCacheId(string $entity_type_id, string $api_client_id, string $id): string {
    return "values:{$entity_type_id}:{$api_client_id}:{$id}";
  }

  /**
   * {@inheritdoc}
   */
  public function get(string $entity_type_id, string $api_client_id, ?array &$ids = NULL): array {
    if ($ids === NULL) {
      return [];
    }

    $cache_id_map = [];
    foreach ($ids as $id) {
      $cache_id_map[$id] = $this->buildCacheId($entity_type_id, $api_client_id, $id);
    }

    $cache_ids = array_values($cache_id_map);
    $entities = [];
    if ($cache = $this->cache->getMultiple($cache_ids)) {
      foreach ($ids as $index => $id) {
        $cache_id = $cache_id_map[$id];

        if (isset($cache[$cache_id])) {
          $entities[$id] = $cache[$cache_id]->data;
          unset($ids[$index]);
        }
      }
    }
    return $entities;
  }

  /**
   * Stores entities in the persistent cache backend.
   *
   * @param \Drupal\brightcove_gallery\Entity\InPageExperienceInterface[] $in_page_experiences
   *   In-Page Experiences to store in the cache.
   */
  public function set(array $in_page_experiences): void {
    foreach ($in_page_experiences as $id => $in_page_experience) {
      $entity_type_id = $in_page_experience->getEntityTypeId();
      $api_client_id = $in_page_experience->getApiClientId();

      $expire = $this->settings->getCacheSeconds();
      if ($expire !== CacheBackendInterface::CACHE_PERMANENT) {
        $expire = $this->time->getRequestTime() + $expire;
      }

      $this->cache->set($this->buildCacheId($entity_type_id, $api_client_id, $id), $in_page_experience, $expire, [
        "{$entity_type_id}_values",
        "{$api_client_id}_entities",
        'entity_field_info',
      ]);
    }
  }

}
