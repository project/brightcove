<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Services\Exception;

use Drupal\brightcove_gallery\Entity\Exception\BrightcoveGalleryExceptionInterface;

/**
 * In-Page Experience API exception.
 */
final class InPageExperienceApiException extends \Exception implements BrightcoveGalleryExceptionInterface {}
