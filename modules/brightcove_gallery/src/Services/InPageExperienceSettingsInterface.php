<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Services;

use Drupal\brightcove\Services\SettingsInterface;

/**
 * In-Page Experience settings interface.
 */
interface InPageExperienceSettingsInterface {

  /**
   * Settings storage name.
   */
  public const STORAGE = SettingsInterface::STORAGE;

  /**
   * Base name of the sub-settings.
   */
  public const SUB_SETTINGS = 'in_page_experience';

  /**
   * Setting for the cache seconds.
   */
  public const CACHE_SECONDS = self::SUB_SETTINGS . '.cache_seconds';

  /**
   * Gets cache seconds.
   *
   * @return int
   *   The cache seconds.
   */
  public function getCacheSeconds(): int;

  /**
   * Sets the cache seconds.
   *
   * @param int $seconds
   *   The cache seconds. Setting -1 means that the related entities will be
   *   cached until the related caches are invalidated.
   */
  public function setCacheSeconds(int $seconds): void;

}
