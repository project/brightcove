<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Services;

use Brightcove\API\InPageExperience as BrightcoveInPageExperienceApi;
use Brightcove\Item\InPageExperience\InPageExperienceInterface as InPageExperienceItemInterface;
use Drupal\brightcove\BrightcoveAPIClientInterface;
use Drupal\brightcove_gallery\Entity\InPageExperience;
use Drupal\brightcove_gallery\Entity\InPageExperienceInterface;
use Drupal\brightcove_gallery\Services\Exception\InPageExperienceApiException;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * In-Page Experience API handler.
 *
 * @see https://apis.support.brightcove.com/ipx/references/reference.html
 */
final class InPageExperienceApi implements InPageExperienceApiInterface {

  /**
   * API Client storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $apiClientStorage;

  /**
   * Brightcove API clients.
   *
   * @var \Brightcove\API\InPageExperience[]
   */
  private static $clients;

  /**
   * Initializes an In-Page Experience API handler.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->apiClientStorage = $entity_type_manager->getStorage('brightcove_api_client');
  }

  /**
   * Gets an In-Page Experience API client.
   *
   * @param \Drupal\brightcove\BrightcoveAPIClientInterface $api_client
   *   API Client entities.
   *
   * @return \Brightcove\API\InPageExperience
   *   An initialized In-Page Experience API client.
   */
  private function getClient(BrightcoveAPIClientInterface $api_client): BrightcoveInPageExperienceApi {
    $id = $api_client->id();
    if (!isset(static::$clients[$id])) {
      static::$clients[$id] = new BrightcoveInPageExperienceApi($api_client->getClient(), $api_client->getAccountId());
    }
    return static::$clients[$id];
  }

  /**
   * Gets a list of In-Page Experience API clients by the given API Clients.
   *
   * @param array $api_clients
   *   List of API Client entities.
   *
   * @return \Brightcove\API\InPageExperience[]
   *   List of In-Page Experience API clients.
   */
  private function getClients(array $api_clients): array {
    $clients = [];
    foreach ($api_clients as $api_client) {
      $clients[$api_client->id()] = $this->getClient($api_client);
    }
    return $clients;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(InPageExperienceInterface $in_page_experience): void {
    $this->getClient($this->apiClientStorage->load($in_page_experience->getApiClientId()))
      ->delete($in_page_experience->id());
  }

  /**
   * {@inheritdoc}
   */
  public function get(string $id, ?BrightcoveAPIClientInterface $api_client = NULL): InPageExperienceInterface {
    if ($api_client === NULL) {
      $api_clients = $this->apiClientStorage->loadMultiple();
    }
    else {
      $api_clients = [
        $api_client->id() => $api_client,
      ];
    }

    // Return the first match.
    $clients = $this->getClients($api_clients);
    foreach ($clients as $api_client_id => $client) {
      return $this->initializeEntity($api_clients[$api_client_id], $client->get($id));
    }

    throw new InPageExperienceApiException('No API Client is available.');
  }

  /**
   * {@inheritdoc}
   */
  public function getAll(array $query = [], ?array $api_clients = NULL): array {
    if ($api_clients === NULL) {
      /** @var \Drupal\brightcove\Entity\BrightcoveAPIClient[] $api_clients */
      $api_clients = $this->apiClientStorage->loadMultiple();
    }

    $in_page_experience_entities = [];
    $clients = $this->getClients($api_clients);
    foreach ($clients as $api_client_id => $client) {
      $in_page_experiences = $client->getAll($query['q'] ?? NULL, $query['sort'] ?? NULL);

      foreach ($in_page_experiences as $in_page_experience) {
        $in_page_experience_entities[$in_page_experience->getId()] = $this->initializeEntity($api_clients[$api_client_id], $in_page_experience);
      }
    }
    return $in_page_experience_entities;
  }

  /**
   * Helper function to initialize the entity.
   *
   * @param \Drupal\brightcove\BrightcoveAPIClientInterface $api_client
   *   API Client entity.
   * @param \Brightcove\Item\InPageExperience\InPageExperienceInterface $in_page_experience
   *   In-Page Experience values from Brightcove.
   *
   * @return \Drupal\brightcove_gallery\Entity\InPageExperienceInterface
   *   The initialized In-Page Experience entity.
   */
  private function initializeEntity(BrightcoveAPIClientInterface $api_client, InPageExperienceItemInterface $in_page_experience): InPageExperienceInterface {
    return new InPageExperience(
      $api_client,
      $in_page_experience->getId(),
      $in_page_experience->getName(),
      $in_page_experience->getName(),
      strtotime($in_page_experience->getCreatedAt()),
      strtotime($in_page_experience->getUpdatedAt()),
      $in_page_experience->getPublishedStatus(),
    );
  }

}
