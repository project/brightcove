<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Services;

use Drupal\brightcove\Services\SettingsBase;
use Drupal\brightcove_gallery\Services\Exception\SettingsInvalidArgumentException;

/**
 * Sub-settings handler for In-Page Experience.
 */
final class InPageExperienceSettings extends SettingsBase implements InPageExperienceSettingsInterface {

  /**
   * {@inheritdoc}
   */
  public function getCacheSeconds(): int {
    return $this->configFactory->get(self::STORAGE)->get(self::CACHE_SECONDS) ?? -1;
  }

  /**
   * {@inheritdoc}
   */
  public function setCacheSeconds(int $seconds): void {
    if ($seconds < -1) {
      throw new SettingsInvalidArgumentException('The given seconds cannot be lower than -1.');
    }
    $this->set(self::CACHE_SECONDS, $seconds);
  }

}
