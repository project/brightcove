<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Services;

use Drupal\brightcove\BrightcoveAPIClientInterface;
use Drupal\brightcove_gallery\Entity\InPageExperienceInterface;

/**
 * In-Page Experience API interface.
 */
interface InPageExperienceApiInterface {

  /**
   * Deletes an In-Page Experience entity.
   *
   * @param \Drupal\brightcove_gallery\Entity\InPageExperienceInterface $in_page_experience
   *   In-Page Experience entity.
   *
   * @throws \Brightcove\API\Exception\APIException
   *   API error. An exception will be thrown in case if the remote entity is no
   *   longer exist.
   */
  public function delete(InPageExperienceInterface $in_page_experience): void;

  /**
   * Gets an In-Page Experience entity.
   *
   * @param string $id
   *   The ID of the entity.
   * @param \Drupal\brightcove\BrightcoveAPIClientInterface|null $api_client
   *   The API Client that the ID should be checked against.
   *   If NULL given, all available API Clients will be called to try to get the
   *   entity.
   *
   * @return \Drupal\brightcove_gallery\Entity\InPageExperienceInterface
   *   The requested In-Page Experience entity or NULL if it cannot be found.
   *
   * @throws \Brightcove\API\Exception\APIException
   *   API error.
   * @throws \Drupal\brightcove_gallery\Services\Exception\InPageExperienceApiException
   *   If no API Client is available.
   */
  public function get(string $id, ?BrightcoveAPIClientInterface $api_client = NULL): InPageExperienceInterface;

  /**
   * Gets all In-Page Experience entities.
   *
   * @param array $query
   *   List of query parameters.
   * @param \Drupal\brightcove\BrightcoveAPIClientInterface[]|null $api_clients
   *   A list of API Clients for which to load the entities. If omitted or NULL
   *   given then all the API clients' entities will be downloaded.
   *
   * @return array
   *   The loaded entities.
   *
   * @throws \Brightcove\API\Exception\APIException
   *   API error.
   */
  public function getAll(array $query = [], ?array $api_clients = NULL): array;

}
