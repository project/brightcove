<?php

declare(strict_types = 1);

namespace Drupal\brightcove_gallery\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber.
 */
final class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    // Disable dynamic field routes as the In-Page Experience entity is not
    // dynamically field-able. The Drupal core doesn't check this by default.
    $routes_to_disable = [
      'entity.brightcove_in_page_experience.field_ui_fields',
      'field_ui.field_storage_config_add_brightcove_in_page_experience',
      'entity.field_config.brightcove_in_page_experience_field_delete_form',
      'entity.field_config.brightcove_in_page_experience_storage_edit_form',
      'entity.field_config.brightcove_in_page_experience_field_edit_form',
    ];

    foreach ($routes_to_disable as $route) {
      if ($route_definition = $collection->get($route)) {
        $route_definition->addRequirements([
          '_access' => 'FALSE',
        ]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -1000];
    return $events;
  }

}
