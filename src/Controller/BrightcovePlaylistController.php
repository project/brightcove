<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Controller;

use Drupal\brightcove\BrightcoveUtil;
use Drupal\brightcove\Entity\BrightcovePlaylist;
use Drupal\brightcove\Entity\Storage\PlaylistStorageInterface;
use Drupal\brightcove\Entity\Storage\VideoStorageInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides controller for playlist related callbacks.
 */
class BrightcovePlaylistController extends ControllerBase {

  /**
   * The brightcove_playlist storage.
   *
   * @var \Drupal\brightcove\Entity\Storage\PlaylistStorageInterface
   */
  protected $playlistStorage;

  /**
   * The brightcove_video storage.
   *
   * @var \Drupal\brightcove\Entity\Storage\VideoStorageInterface
   */
  protected $videoStorage;

  /**
   * Controller constructor.
   *
   * @param \Drupal\brightcove\Entity\Storage\PlaylistStorageInterface $playlist_storage
   *   Playlist entity storage.
   * @param \Drupal\brightcove\Entity\Storage\VideoStorageInterface $video_storage
   *   Video entity storage.
   */
  public function __construct(PlaylistStorageInterface $playlist_storage, VideoStorageInterface $video_storage) {
    $this->playlistStorage = $playlist_storage;
    $this->videoStorage = $video_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $entity_type_manager->getStorage('brightcove_playlist'),
      $entity_type_manager->getStorage('brightcove_video')
    );
  }

  /**
   * Menu callback to update the existing Playlist with the latest version.
   *
   * @param int $entity_id
   *   The ID of the playlist in Drupal.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirection response.
   *
   * @throws \Exception
   *   If BrightcoveAPIClient ID is missing when a new entity is being created.
   */
  public function update(int $entity_id): Response {
    /** @var \Drupal\brightcove\Entity\BrightcovePlaylist $playlist */
    $playlist = $this->playlistStorage->load($entity_id);
    $cms = BrightcoveUtil::getCmsApi($playlist->getApiClient());

    // Update playlist.
    BrightcovePlaylist::createOrUpdate($cms->getPlaylist($playlist->getBrightcoveId()), $this->playlistStorage, $this->videoStorage);

    // Redirect back to the playlist edit form.
    return $this->redirect('entity.brightcove_playlist.edit_form', ['brightcove_playlist' => $entity_id]);
  }

}
