<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Controller;

use Brightcove\Item\Video\Image;
use Drupal\brightcove\BrightcoveUtil;
use Drupal\brightcove\BrightcoveVideoInterface;
use Drupal\brightcove\Entity\BrightcoveTextTrack;
use Drupal\brightcove\Entity\Storage\VideoStorageInterface;
use Drupal\brightcove\Services\IngestionInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides controller for video related callbacks.
 */
class BrightcoveVideoController extends ControllerBase {

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The brightcove_video storage.
   *
   * @var \Drupal\brightcove\Entity\Storage\VideoStorageInterface
   */
  protected $videoStorage;

  /**
   * The brightcove_text_track storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $textTrackStorage;

  /**
   * The video queue object.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $videoQueue;

  /**
   * Key-value expirable store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface
   */
  protected $keyValueExpirable;

  /**
   * Lock backend.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * Ingestion.
   *
   * @var \Drupal\brightcove\Services\IngestionInterface
   */
  protected $ingestion;

  /**
   * Controller constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection.
   * @param \Drupal\brightcove\Entity\Storage\VideoStorageInterface $video_storage
   *   Brightcove Video entity storage.
   * @param \Drupal\Core\Entity\EntityStorageInterface $text_track_storage
   *   Brightcove Text Track entity storage.
   * @param \Drupal\Core\Queue\QueueInterface $video_queue
   *   The Video queue object.
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface $key_value_expirable
   *   Key-value expirable store.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   Lock backend.
   * @param \Drupal\brightcove\Services\IngestionInterface $ingestion
   *   Ingestion.
   */
  public function __construct(
    Connection $connection,
    VideoStorageInterface $video_storage,
    EntityStorageInterface $text_track_storage,
    QueueInterface $video_queue,
    KeyValueExpirableFactoryInterface $key_value_expirable,
    LockBackendInterface $lock,
    IngestionInterface $ingestion,
  ) {
    $this->connection = $connection;
    $this->videoStorage = $video_storage;
    $this->textTrackStorage = $text_track_storage;
    $this->videoQueue = $video_queue;
    $this->keyValueExpirable = $key_value_expirable;
    $this->lock = $lock;
    $this->ingestion = $ingestion;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $entity_type_manager = $container->get('entity_type.manager');

    return new static(
      $container->get('database'),
      $entity_type_manager->getStorage('brightcove_video'),
      $entity_type_manager->getStorage('brightcove_text_track'),
      $container->get('queue')->get('brightcove_video_queue_worker'),
      $container->get('keyvalue.expirable'),
      $container->get('lock'),
      $container->get('brightcove.ingestion'),
    );
  }

  /**
   * Menu callback to update the existing Video with the latest version.
   *
   * @param int $entity_id
   *   The ID of the video in Drupal.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|null
   *   Redirection response.
   */
  public function update(int $entity_id): ?RedirectResponse {
    /** @var \Drupal\brightcove\Entity\BrightcoveVideo $video_entity */
    $video_entity = $this->videoStorage->load($entity_id);
    $cms = BrightcoveUtil::getCmsApi($video_entity->getApiClient());

    // Update video.
    $video = $cms->getVideo($video_entity->getBrightcoveId());
    $this->videoQueue->createItem([
      'api_client_id' => $video_entity->getApiClient(),
      'video' => $video,
    ]);

    // Run batch.
    batch_set([
      'title' => $this->t('Brightcove sync'),
      'operations' => [
        [
          [BrightcoveUtil::class, 'runQueue'], ['brightcove_video_queue_worker'],
        ],
        [
          [BrightcoveUtil::class, 'runQueue'], ['brightcove_text_track_queue_worker'],
        ],
        [
          [BrightcoveUtil::class, 'runQueue'], ['brightcove_text_track_delete_queue_worker'],
        ],
      ],
    ]);

    // Run batch and redirect back to the video edit form.
    return batch_process(Url::fromRoute('entity.brightcove_video.edit_form', ['brightcove_video' => $entity_id]));
  }

  /**
   * Ingestion callback for Brightcove.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   * @param string $token
   *   The token that is based on the Video ID.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   An empty Response object.
   */
  public function ingestionCallback(Request $request, string $token): Response {
    $status = BrightcoveUtil::runWithSemaphore(function () use ($request, $token) {
      $content = Json::decode($request->getContent());
      if (json_last_error() !== JSON_ERROR_NONE) {
        throw new \Exception('Failed to decode JSON from response: ' . json_last_error_msg());
      }

      if (is_array($content) && $content['status'] === 'SUCCESS' && $content['version'] === '1' && $content['action'] === 'CREATE') {
        $video_id = $this->keyValueExpirable->get('brightcove_callback')->get($token);

        if (!empty($video_id)) {
          /** @var \Drupal\brightcove\Entity\BrightcoveVideo $video_entity */
          $video_entity = $this->videoStorage->load($video_id);

          if ($video_entity !== NULL) {
            switch ($content['entityType']) {
              // Video.
              case 'TITLE':
                $this->handleTitle($video_entity);
                break;

              // As per the documentation an asset can be either an image
              // or a text track (caption, subtitle, etc.).
              // @see https://apis.support.brightcove.com/dynamic-ingest/general/notifications-dynamic-delivery-video-cloud.html
              case 'ASSET':
                $this->handleAsset($content['entity'], $video_entity);
                break;

              // Don't do anything if we got something else, e.g. renderation.
              default:
                return NULL;
            }

            // Update video entity with the latest update date.
            $cms = BrightcoveUtil::getCmsApi($video_entity->getApiClient());
            $video = $cms->getVideo($video_entity->getBrightcoveId());
            $video_entity->setChangedTime(strtotime($video->getUpdatedAt()));
            $video_entity->save();
          }
        }
      }

      return TRUE;
    }, $this->lock);

    return new Response('', $status === FALSE ? 409 : 200);
  }

  /**
   * Handles ASSET entity type notification.
   *
   * @param string $entity
   *   The asset entity.
   * @param \Drupal\brightcove\BrightcoveVideoInterface $video
   *   The video.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  private function handleAsset(string $entity, BrightcoveVideoInterface $video): void {
    switch ($entity) {
      // Handle images.
      case BrightcoveVideoInterface::IMAGE_TYPE_THUMBNAIL:
      case BrightcoveVideoInterface::IMAGE_TYPE_POSTER:
        $this->handleImage($entity, $video);
        break;

      // Handle Text Tracks.
      default:
        $this->handleTextTrack($entity, $video);
        break;
    }
  }

  /**
   * Handles asset image entities.
   *
   * @param string $entity
   *   The image entity name, currently either thumbnail or poster.
   * @param \Drupal\brightcove\BrightcoveVideoInterface $video
   *   The video.
   *
   * @throws \Exception
   */
  private function handleImage(string $entity, BrightcoveVideoInterface $video): void {
    try {
      // Download image.
      $cms = BrightcoveUtil::getCmsApi($video->getApiClient());
      $images = $cms->getVideoImages($video->getBrightcoveId());
      $function = ucfirst($entity);
      $image = $images->{"get{$function}"}();
      if (!empty($image)) {
        if (is_array($image)) {
          $image_array = $image;
          $image = new Image();
          $image->applyJSON($image_array);
        }

        $video->saveImage($entity, $image);
      }
    }
    finally {
      $this->ingestion->setFieldMarkedForIngestion($video, $entity, FALSE);
    }
  }

  /**
   * Handles asset Text Track entities.
   *
   * @param string $entity
   *   Text Track entity name.
   * @param \Drupal\brightcove\BrightcoveVideoInterface $video
   *   The video.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function handleTextTrack(string $entity, BrightcoveVideoInterface $video): void {
    try {
      // Load Text Track entities.
      $text_tracks = $video->getTextTracks();
      $ids = [];
      foreach ($text_tracks as $text_track) {
        $ids[] = $text_track['target_id'];
      }
      /** @var \Drupal\brightcove\BrightcoveTextTrackInterface[] $text_tracks */
      $text_tracks = $this->textTrackStorage->loadMultiple($ids);

      $cms = BrightcoveUtil::getCmsApi($video->getApiClient());
      foreach ($text_tracks as $text_track) {
        if (empty($text_track->getTextTrackId())) {
          // Remove text track without Brightcove ID.
          $text_track->delete();

          // Try to find the ingested text track on the video object and
          // recreate it.
          $brightcove_video = $cms->getVideo($video->getBrightcoveId());
          $api_text_tracks = $brightcove_video->getTextTracks();
          $found_api_text_track = NULL;
          foreach ($api_text_tracks as $api_text_track) {
            if ($api_text_track->getId() === $entity) {
              $found_api_text_track = $api_text_track;
              break;
            }
          }

          // Create new text track.
          if ($found_api_text_track !== NULL) {
            BrightcoveTextTrack::createOrUpdate($found_api_text_track, $this->textTrackStorage, $video->id());
          }

          // We need to process only one per request.
          break;
        }
      }
    }
    finally {
      $this->ingestion->setFieldMarkedForIngestion($video, 'text_tracks', FALSE);
    }
  }

  /**
   * Handles TITLE entity type notification.
   *
   * @param \Drupal\brightcove\BrightcoveVideoInterface $video
   *   The video.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function handleTitle(BrightcoveVideoInterface $video): void {
    // Unset (delete) video from the entity.
    $video->setVideoFile(NULL);
    $video->setVideoUrl(NULL);
    $video->save();
  }

  /**
   * Destructor.
   */
  public function __destruct() {
    // Make sure that the semaphore gets released.
    if ($this->state()->get('brightcove_video_semaphore', FALSE) === TRUE) {
      $this->state()->set('brightcove_video_semaphore', FALSE);
    }
  }

}
