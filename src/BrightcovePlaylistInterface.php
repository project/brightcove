<?php

declare(strict_types = 1);

namespace Drupal\brightcove;

use Drupal\Core\Entity\EntityInterface;

/**
 * Provides an interface for defining Brightcove Playlists.
 */
interface BrightcovePlaylistInterface extends EntityInterface, BrightcoveCMSEntityInterface {

  /**
   * Returns the playlist type.
   *
   * @return string
   *   The type of the playlist.
   */
  public function getType(): string;

  /**
   * Sets the playlist's type.
   *
   * @param string $type
   *   The type of the playlist, either BrightcovePlaylist::TYPE_MANUAL or
   *   BrightcovePlaylist::SMART.
   *
   * @return \Drupal\brightcove\BrightcovePlaylistInterface
   *   The called Brightcove Playlist.
   *
   * @throws \InvalidArgumentException
   *   If the value input is inappropriate.
   */
  public function setType(string $type): BrightcovePlaylistInterface;

  /**
   * Returns the Brightcove Playlist favorite indicator.
   *
   * Favorite Brightcove Playlists are displayed in the sidebar.
   *
   * @return bool
   *   TRUE if the Brightcove Playlist is favorite.
   */
  public function isFavorite(): bool;

  /**
   * Returns the tags search condition for smart playlist.
   *
   * @return string
   *   The condition of the tag search.
   */
  public function getTagsSearchCondition(): string;

  /**
   * Sets the tags search condition for smart playlist.
   *
   * @param string $condition
   *   The condition of the tag search, possible values are:
   *     - TAG_SEARCH_CONTAINS_ONE_OR_MORE
   *     - TAG_SEARCH_CONTAINS_ALL
   *   .
   *
   * @return \Drupal\brightcove\BrightcovePlaylistInterface
   *   The called Brightcove Playlist.
   */
  public function setTagsSearchCondition(string $condition): BrightcovePlaylistInterface;

  /**
   * Returns the list of videos on the playlist.
   *
   * @return int[]
   *   The videos on the playlist.
   */
  public function getVideos(): array;

  /**
   * Sets the playlist's videos.
   *
   * @param array|null $videos
   *   The videos on the playlist. Array of ['target_id' => int] items.
   *
   * @return \Drupal\brightcove\BrightcovePlaylistInterface
   *   The called Brightcove Playlist.
   */
  public function setVideos(?array $videos): BrightcovePlaylistInterface;

}
