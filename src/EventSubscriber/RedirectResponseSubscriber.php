<?php

declare(strict_types = 1);

namespace Drupal\brightcove\EventSubscriber;

use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Drupal\brightcove\Services\SessionManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Redirect response event subscriber.
 */
final class RedirectResponseSubscriber implements EventSubscriberInterface {

  /**
   * Module specific session manager.
   *
   * @var \Drupal\brightcove\Services\SessionManagerInterface
   */
  private $sessionManager;

  /**
   * Initializes a new redirect response event subscriber.
   *
   * @param \Drupal\brightcove\Services\SessionManagerInterface $session_manager
   *   Module specific session manager.
   */
  public function __construct(SessionManagerInterface $session_manager) {
    $this->sessionManager = $session_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::RESPONSE => [
        // Using weight 1 as the destination has to be removed when set to
        // ignore before the
        // \Drupal\Core\EventSubscriber\RedirectResponseSubscriber::checkRedirectUrl()
        // event handler would change the redirect destination.
        ['checkIgnoreDestination', 1],
      ],
    ];
  }

  /**
   * Handles ignore destination event.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The response event.
   */
  public function checkIgnoreDestination(ResponseEvent $event): void {
    if ($event->getResponse() instanceof RedirectResponse) {
      $request = $event->getRequest();

      // If ignore destination is set remove the destination from the query
      // parameter to allow the original destination take priority.
      if ($request->query->get('destination') !== NULL && $this->sessionManager->isDestinationIgnored()) {
        $request->query->remove('destination');
        $this->sessionManager->setIgnoreDestination(FALSE);
      }
    }
  }

}
