<?php

declare(strict_types = 1);

namespace Drupal\brightcove\EventSubscriber;

use Symfony\Component\HttpKernel\Event\RequestEvent;
use Brightcove\API\Client;
use Drupal\Core\Extension\ModuleExtensionList;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to Drupal initialization event.
 */
class BrightcoveInitSubscriber implements EventSubscriberInterface {

  /**
   * Module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  private $moduleExtensionList;

  /**
   * Initializes an init subscriber.
   *
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   Module extension list.
   */
  public function __construct(ModuleExtensionList $module_extension_list) {
    $this->moduleExtensionList = $module_extension_list;
  }

  /**
   * Initialize Brightcove client proxy.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   GET response event.
   */
  public function initializeBrightcoveClient(RequestEvent $event): void {
    Client::$consumer = 'Drupal/' . \Drupal::VERSION . ' Brightcove/' . ($this->moduleExtensionList->getExtensionInfo('brightcove')['version'] ?: 'dev');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST][] = ['initializeBrightcoveClient'];
    return $events;
  }

}
