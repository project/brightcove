<?php

declare(strict_types = 1);

namespace Drupal\brightcove;

use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Provides a trait to identify changed entity fields.
 */
trait EntityChangedFieldsTrait {

  /**
   * Changed fields.
   *
   * @var bool[]
   */
  protected $changedFields = [];

  /**
   * {@inheritdoc}
   */
  public function isFieldChanged(string $name): bool {
    return !empty($this->changedFields[$name]);
  }

  /**
   * {@inheritdoc}
   */
  public function hasChangedField(): bool {
    return !empty($this->changedFields);
  }

  /**
   * {@inheritdoc}
   */
  public function clearChangedFields(): void {
    $this->changedFields = [];
  }

  /**
   * {@inheritdoc}
   */
  public function checkUpdatedFields(EntityStorageInterface $storage): void {
    // Collect object getters.
    $methods = [];
    foreach (get_class_methods($this) as $method) {
      // Create a matchable key for the get methods.
      if (preg_match('/^(?:get|is)[\w\d_]+$/i', $method)) {
        $methods[strtolower($method)] = $method;
      }
    }

    // Check fields if they were updated and mark them if changed.
    if (!empty($this->id())) {
      /** @var \Drupal\brightcove\Entity\BrightcoveVideo $original_entity */
      $original_entity = $storage->loadUnchanged($this->id());

      if ($original_entity->getChangedTime() !== $this->getChangedTime()) {
        /** @var \Drupal\Core\Field\FieldItemList $field */
        foreach ($this->getFields() as $name => $field) {
          $getter = $this->getGetterName($name, $methods);

          // If the getter is available for the field then compare the two
          // field and if changed mark it.
          if ($getter !== NULL && $this->$getter() !== $original_entity->$getter()) {
            $this->changedFields[$name] = TRUE;
          }
        }
      }
    }
    // If there is no original entity, mark all fields modified, because in
    // this case the entity is being created.
    else {
      foreach ($this->getFields() as $name => $field) {
        if ($this->getGetterName($name, $methods) !== NULL) {
          $this->changedFields[$name] = TRUE;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getGetterName(string $name, array $methods): ?string {
    $function_part_name = $name;

    // Get entity key's status field alias.
    $status = self::getEntityType()->getKey('status');

    // Use the correct function for the status field.
    if ($name === $status) {
      $function_part_name = 'published';
    }

    // Acquire getter method name.
    $getter_name = 'get' . str_replace('_', '', $function_part_name);
    $is_getter_name = 'is' . str_replace('_', '', $function_part_name);

    $getter = NULL;
    if (isset($methods[$getter_name])) {
      $getter = $methods[$getter_name];
    }
    elseif (isset($methods[$is_getter_name])) {
      $getter = $methods[$is_getter_name];
    }

    return $getter;
  }

}
