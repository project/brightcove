<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Plugin\QueueWorker;

use Drupal\brightcove\Entity\BrightcovePlayer;
use Drupal\brightcove\Entity\Storage\PlayerStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Entity Update Tasks for Players.
 *
 * @QueueWorker(
 *   id = "brightcove_player_queue_worker",
 *   title = @Translation("Brightcove Player queue worker"),
 *   cron = {
 *     "time" = 30,
 *   },
 * )
 */
class BrightcovePlayerQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The brightcove_player storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $playerStorage;

  /**
   * Constructs a new BrightcovePlayerQueueWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\brightcove\Entity\Storage\PlayerStorageInterface $player_storage
   *   Player storage.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, PlayerStorageInterface $player_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->playerStorage = $player_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('brightcove_player')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    /** @var \Brightcove\Item\Player\Player $player */
    $player = $data['player'];

    BrightcovePlayer::createOrUpdate($player, $this->playerStorage, $data['api_client_id']);
  }

}
