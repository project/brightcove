<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Plugin\QueueWorker;

use Drupal\brightcove\Entity\Storage\PlaylistStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Entity Local Delete Tasks for Playlist.
 *
 * @QueueWorker(
 *   id = "brightcove_playlist_local_delete_queue_worker",
 *   title = @Translation("Brightcove Playlist local delete queue worker"),
 *   cron = {
 *     "time" = 30,
 *   },
 * )
 */
class BrightcovePlaylistLocalDeleteQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Playlist storage.
   *
   * @var \Drupal\brightcove\Entity\Storage\PlaylistStorageInterface
   */
  protected $playlistStorage;

  /**
   * Constructs a new BrightcovePlaylistLocalDeleteQueueWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\brightcove\Entity\Storage\PlaylistStorageInterface $playlist_storage
   *   Brightcove Playlist Entity storage.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, PlaylistStorageInterface $playlist_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->playlistStorage = $playlist_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('brightcove_playlist')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    /** @var \Drupal\brightcove\Entity\BrightcovePlaylist $playlist */
    $playlist = $this->playlistStorage->load($data);

    if (!is_null($playlist)) {
      $this->playlistStorage->delete([$playlist], FALSE);
    }
  }

}
