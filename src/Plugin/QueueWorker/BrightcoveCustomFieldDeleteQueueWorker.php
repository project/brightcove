<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Plugin\QueueWorker;

use Drupal\brightcove\Entity\BrightcoveCustomField;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Entity Delete Tasks for Custom Fields.
 *
 * @QueueWorker(
 *   id = "brightcove_custom_field_delete_queue_worker",
 *   title = @Translation("Brightcove Custom Field delete queue worker"),
 *   cron = {
 *     "time" = 30,
 *   },
 * )
 */
class BrightcoveCustomFieldDeleteQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Custom field storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $customFieldStorage;

  /**
   * Initializes a custom field delete queue worker.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $custom_field_storage
   *   Custom field storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $custom_field_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->customFieldStorage = $custom_field_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('brightcove_custom_field')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    // Delete custom field.
    if ($data instanceof BrightcoveCustomField) {
      $this->customFieldStorage->delete([$data]);
    }
    else {
      /** @var \Drupal\brightcove\Entity\BrightcoveCustomField $custom_field_entity */
      $custom_field_entity = $this->customFieldStorage->load($data);

      if (!is_null($custom_field_entity)) {
        $this->customFieldStorage->delete([$custom_field_entity]);
      }
    }
  }

}
