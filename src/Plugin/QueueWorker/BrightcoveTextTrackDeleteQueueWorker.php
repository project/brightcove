<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Entity Delete Tasks for Text Track.
 *
 * @QueueWorker(
 *   id = "brightcove_text_track_delete_queue_worker",
 *   title = @Translation("Brightcove Text Track delete queue worker"),
 *   cron = {
 *     "time" = 30,
 *   },
 * )
 */
class BrightcoveTextTrackDeleteQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Text track storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $textTrackStorage;

  /**
   * Initializes a new Text Track deletion queue worker.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $text_track_storage
   *   Text Track storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $text_track_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->textTrackStorage = $text_track_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('brightcove_text_track')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $text_track_entities = $this->textTrackStorage->loadByProperties([
      'text_track_id' => $data,
    ]);

    if (!empty($text_track_entities)) {
      $this->textTrackStorage->delete($text_track_entities);
    }
  }

}
