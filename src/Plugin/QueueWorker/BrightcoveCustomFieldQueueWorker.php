<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Plugin\QueueWorker;

use Drupal\brightcove\Entity\BrightcoveCustomField;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Entity Update Tasks for Custom Fields.
 *
 * @QueueWorker(
 *   id = "brightcove_custom_field_queue_worker",
 *   title = @Translation("Brightcove Custom Field queue worker"),
 *   cron = {
 *     "time" = 30,
 *   },
 * )
 */
class BrightcoveCustomFieldQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The brightcove_custom_field storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $customFieldStorage;

  /**
   * Constructs a new BrightcoveCustomFieldQueueWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $custom_field_storage
   *   Custom field storage.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, EntityStorageInterface $custom_field_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->customFieldStorage = $custom_field_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('brightcove_custom_field')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    /** @var \Brightcove\Item\CustomField $custom_field */
    $custom_field = $data['custom_field'];

    BrightcoveCustomField::createOrUpdate($custom_field, $this->customFieldStorage, $data['api_client_id']);
  }

}
