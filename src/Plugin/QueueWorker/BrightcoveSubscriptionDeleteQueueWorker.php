<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Plugin\QueueWorker;

use Brightcove\API\Exception\APIException;
use Drupal\brightcove\BrightcoveUtil;
use Drupal\brightcove\Entity\BrightcoveSubscription;
use Drupal\brightcove\Services\LoggerInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Entity Sync Tasks for Subscription.
 *
 * @QueueWorker(
 *   id = "brightcove_subscription_delete_queue_worker",
 *   title = @Translation("Brightcove Subscription queue worker"),
 *   cron = {
 *     "time" = 30,
 *   },
 * )
 */
class BrightcoveSubscriptionDeleteQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Logger.
   *
   * @var \Drupal\brightcove\Services\LoggerInterface
   */
  private $logger;

  /**
   * Initializes a Subscription delete queue worker.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\brightcove\Services\LoggerInterface $logger
   *   Logger.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('brightcove.logger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    /** @var array $data */
    if (!empty($data['local_only'])) {
      $brightcove_subscription = BrightcoveSubscription::loadByBcSid($data['subscription_id']);
      if (!empty($brightcove_subscription)) {
        $brightcove_subscription->delete(TRUE);
      }
    }
    else {
      // Check the Subscription if it is available on Brightcove or not.
      try {
        $cms = BrightcoveUtil::getCmsApi($data['api_client_id']);
        $cms->getSubscription($data['subscription_id']);
      }
      catch (APIException $e) {
        // If we got a not found response, delete the local version of the
        // subscription.
        if ($e->getCode() === 404) {
          /** @var \Drupal\brightcove\Entity\BrightcoveSubscription $subscription */
          $brightcove_subscription = BrightcoveSubscription::loadByBcSid($data['subscription_id']);

          if (!empty($brightcove_subscription)) {
            // In case of a default subscription, unset the entity's
            // association with the Brightcove entity, but keep a local entity
            // in Drupal without the Brightcove ID and set its status to
            // disabled.
            if ($brightcove_subscription->isDefault()) {
              $brightcove_subscription->setBcSid(NULL);
              $brightcove_subscription->setStatus(FALSE);
              $brightcove_subscription->save();
            }
            else {
              $brightcove_subscription->delete(TRUE);
            }
          }
        }
        elseif ($e->getCode() === 401) {
          $this->logger->logException($e, 'Access denied for Notification', [], RfcLogLevel::WARNING);
        }
        // Otherwise, throw the same exception again.
        else {
          throw $e;
        }
      }
    }
  }

}
