<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Plugin\QueueWorker;

use Brightcove\API\Exception\APIException;
use Drupal\brightcove\BrightcoveUtil;
use Drupal\brightcove\Entity\Storage\VideoStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Entity Delete Tasks for Video.
 *
 * @QueueWorker(
 *   id = "brightcove_video_delete_queue_worker",
 *   title = @Translation("Brightcove Video delete queue worker"),
 *   cron = {
 *     "time" = 30,
 *   },
 * )
 */
class BrightcoveVideoDeleteQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Video storage.
   *
   * @var \Drupal\brightcove\Entity\Storage\VideoStorageInterface
   */
  protected $videoStorage;

  /**
   * Constructs a new BrightcoveVideoQueueWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\brightcove\Entity\Storage\VideoStorageInterface $video_storage
   *   Video storage.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, VideoStorageInterface $video_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->videoStorage = $video_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('brightcove_video')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    // Check the video if it is available on Brightcove or not.
    try {
      $cms = BrightcoveUtil::getCmsApi($data->api_client);
      $cms->getVideo($data->video_id);
    }
    catch (APIException $e) {
      // If we got a not found response, delete the local version of the video.
      if ($e->getCode() === 404) {
        /** @var \Drupal\brightcove\BrightcoveVideoInterface $video */
        $video = $this->videoStorage->load($data->bcvid);
        if (!empty($video)) {
          $this->videoStorage->delete([$video], FALSE);
        }
      }
      // Otherwise, throw again the same exception.
      else {
        throw new APIException($e->getMessage(), $e->getCode(), $e, $e->getResponseBody());
      }
    }
  }

}
