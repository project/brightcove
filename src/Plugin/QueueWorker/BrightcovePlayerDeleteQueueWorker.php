<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Plugin\QueueWorker;

use Drupal\brightcove\Entity\Storage\PlayerStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Entity Delete Tasks for Player.
 *
 * @QueueWorker(
 *   id = "brightcove_player_delete_queue_worker",
 *   title = @Translation("Brightcove Player delete queue worker"),
 *   cron = {
 *     "time" = 30,
 *   },
 * )
 */
class BrightcovePlayerDeleteQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The brightcove_player storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $playerStorage;

  /**
   * Constructs a new BrightcovePlayerDeleteQueueWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\brightcove\Entity\Storage\PlayerStorageInterface $player_storage
   *   The player storage.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, PlayerStorageInterface $player_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->playerStorage = $player_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('brightcove_player')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    if (isset($data['player_id'])) {
      /** @var \Drupal\brightcove\BrightcovePlayerInterface[] $players */
      $players = $this->playerStorage->loadByProperties([
        'player_id' => $data['player_id'],
      ]);
      $player = reset($players);

      if (!is_null($player)) {
        $this->playerStorage->delete([$player]);
      }
    }
    elseif (isset($data['player_entity_id'])) {
      /** @var \Drupal\brightcove\Entity\BrightcovePlayer $player */
      $player = $this->playerStorage->load($data['player_entity_id']);

      if (!is_null($player)) {
        $this->playerStorage->delete([$player]);
      }
    }
  }

}
