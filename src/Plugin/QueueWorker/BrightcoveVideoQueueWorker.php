<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Plugin\QueueWorker;

use Drupal\brightcove\Entity\Storage\VideoStorageInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\brightcove\Entity\BrightcoveVideo;

/**
 * Processes Entity Update Tasks for Video.
 *
 * @QueueWorker(
 *   id = "brightcove_video_queue_worker",
 *   title = @Translation("Brightcove Video queue worker"),
 *   cron = {
 *     "time" = 30,
 *   },
 * )
 */
class BrightcoveVideoQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Video entity storage.
   *
   * @var \Drupal\brightcove\Entity\Storage\VideoStorageInterface
   */
  protected $videoStorage;

  /**
   * Entity query factory.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The playlist page queue object.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $textTrackQueue;

  /**
   * The playlist page queue object.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $textTrackDeleteQueue;

  /**
   * Text Track entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $textTrackStorage;

  /**
   * Constructs a new BrightcoveVideoQueueWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\brightcove\Entity\Storage\VideoStorageInterface $video_storage
   *   Video entity storage.
   * @param \Drupal\Core\Entity\EntityStorageInterface $text_track_storage
   *   Text Track entity storage.
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection.
   * @param \Drupal\Core\Queue\QueueInterface $text_track_queue
   *   Text track queue object.
   * @param \Drupal\Core\Queue\QueueInterface $text_track_delete_queue
   *   Text track delete queue object.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, VideoStorageInterface $video_storage, EntityStorageInterface $text_track_storage, Connection $connection, QueueInterface $text_track_queue, QueueInterface $text_track_delete_queue) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->videoStorage = $video_storage;
    $this->textTrackStorage = $text_track_storage;
    $this->connection = $connection;
    $this->textTrackQueue = $text_track_queue;
    $this->textTrackDeleteQueue = $text_track_delete_queue;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    $entity_type_manager = $container->get('entity_type.manager');

    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager->getStorage('brightcove_video'),
      $entity_type_manager->getStorage('brightcove_text_track'),
      $container->get('database'),
      $container->get('queue')->get('brightcove_text_track_queue_worker'),
      $container->get('queue')->get('brightcove_text_track_delete_queue_worker')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    /** @var \Brightcove\Item\Video\Video $video */
    $video = $data['video'];

    /** @var \Drupal\brightcove\Entity\BrightcoveVideo $video_entity */
    $video_entity = BrightcoveVideo::createOrUpdate($video, $this->videoStorage, $data['api_client_id']);

    if (!empty($video_entity)) {
      // Get existing text tracks.
      $existing_text_tracks = [];
      foreach ($video_entity->getTextTracks() as $text_track) {
        /** @var \Drupal\brightcove\Entity\BrightcoveTextTrack $text_track_entity */
        $text_track_entity = $this->textTrackStorage->load($text_track['target_id']);

        if (!is_null($text_track_entity)) {
          $existing_text_tracks[$text_track_entity->getTextTrackId()] = TRUE;
        }
      }

      // Save Video text tracks.
      $text_tracks = $video->getTextTracks();
      foreach ($text_tracks as $text_track) {
        // Remove existing text tracks from the list which are still existing on
        // Brightcove.
        if (isset($existing_text_tracks[$text_track->getId()])) {
          unset($existing_text_tracks[$text_track->getId()]);
        }

        // Create new queue item for text track.
        $this->textTrackQueue->createItem([
          'text_track' => $text_track,
          'video_entity_id' => $video_entity->id(),
        ]);
      }

      // Remove existing text tracks which are no longer available on
      // Brightcove.
      foreach (array_keys($existing_text_tracks) as $text_track_id) {
        // Create new delete queue item for text track.
        $this->textTrackDeleteQueue->createItem($text_track_id);
      }
    }
  }

}
