<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Plugin\QueueWorker;

use Brightcove\API\Exception\APIException;
use Drupal\brightcove\BrightcoveUtil;
use Drupal\brightcove\Entity\Storage\PlaylistStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Entity Delete Tasks for Playlist.
 *
 * @QueueWorker(
 *   id = "brightcove_playlist_delete_queue_worker",
 *   title = @Translation("Brightcove Playlist delete queue worker"),
 *   cron = {
 *     "time" = 30,
 *   },
 * )
 */
class BrightcovePlaylistDeleteQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Playlist storage.
   *
   * @var \Drupal\brightcove\Entity\Storage\PlaylistStorageInterface
   */
  protected $playlistStorage;

  /**
   * Initializes a Playlist delete queue worker.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\brightcove\Entity\Storage\PlaylistStorageInterface $playlist_storage
   *   Playlist storage.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, PlaylistStorageInterface $playlist_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->playlistStorage = $playlist_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('brightcove_playlist')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    // Check the playlist if it is available on Brightcove or not.
    try {
      $cms = BrightcoveUtil::getCmsApi($data->api_client);
      $cms->getPlaylist($data->playlist_id);
    }
    catch (APIException $e) {
      // If we got a not found response, delete the local version of the
      // playlist.
      if ($e->getCode() === 404) {
        /** @var \Drupal\brightcove\Entity\BrightcovePlaylist $playlist */
        $playlist = $this->playlistStorage->load($data->bcplid);
        $this->playlistStorage->delete([$playlist], FALSE);
      }
      // Otherwise, throw again the same exception.
      else {
        throw new APIException($e->getMessage(), $e->getCode(), $e, $e->getResponseBody());
      }
    }
  }

}
