<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Plugin\QueueWorker;

use Drupal\brightcove\Entity\Storage\VideoStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Entity Local Delete Tasks for Video.
 *
 * @QueueWorker(
 *   id = "brightcove_video_local_delete_queue_worker",
 *   title = @Translation("Brightcove Video local delete queue worker"),
 *   cron = {
 *     "time" = 30,
 *   },
 * )
 */
class BrightcoveVideoLocalDeleteQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Video storage.
   *
   * @var \Drupal\brightcove\Entity\Storage\VideoStorageInterface
   */
  protected $videoStorage;

  /**
   * Constructs a new BrightcoveVideoLocalDeleteQueueWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\brightcove\Entity\Storage\VideoStorageInterface $video_storage
   *   Video storage.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, VideoStorageInterface $video_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->videoStorage = $video_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('brightcove_video')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    /** @var \Drupal\brightcove\Entity\BrightcoveVideo $video */
    $video = $this->videoStorage->load($data);

    if (!is_null($video)) {
      $this->videoStorage->delete([$video], FALSE);
    }
  }

}
