<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Plugin\QueueWorker;

use Drupal\brightcove\Entity\BrightcoveTextTrack;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Entity Update Tasks for Text Track.
 *
 * @QueueWorker(
 *   id = "brightcove_text_track_queue_worker",
 *   title = @Translation("Brightcove Text Track queue worker"),
 *   cron = {
 *     "time" = 30,
 *   },
 * )
 */
class BrightcoveTextTrackQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Text Track storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $textTrackStorage;

  /**
   * Constructs a new BrightcoveTextTrackQueueWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $text_track_storage
   *   Text Track storage.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, EntityStorageInterface $text_track_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->textTrackStorage = $text_track_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('brightcove_text_track')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    /** @var \Brightcove\Item\Video\TextTrack $text_track */
    $text_track = $data['text_track'];

    BrightcoveTextTrack::createOrUpdate($text_track, $this->textTrackStorage, (int) $data['video_entity_id']);
  }

}
