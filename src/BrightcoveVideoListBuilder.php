<?php

declare(strict_types = 1);

namespace Drupal\brightcove;

use Drupal\Core\Link;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\file\FileStorageInterface;
use Drupal\image\ImageStyleStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Brightcove Videos.
 *
 * @ingroup brightcove
 */
class BrightcoveVideoListBuilder extends EntityListBuilder {

  /**
   * Account proxy.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $accountProxy;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * File storage.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  /**
   * Image style storage.
   *
   * @var \Drupal\image\ImageStyleStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Entity type.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   Entity storage.
   * @param \Drupal\Core\Session\AccountProxy $account_proxy
   *   Account proxy.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   Date formatter.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String translation.
   * @param \Drupal\file\FileStorageInterface $file_storage
   *   File storage.
   * @param \Drupal\image\ImageStyleStorageInterface $image_style_storage
   *   Image style storage.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, AccountProxy $account_proxy, DateFormatter $date_formatter, TranslationInterface $string_translation, FileStorageInterface $file_storage, ImageStyleStorageInterface $image_style_storage) {
    parent::__construct($entity_type, $storage);
    $this->accountProxy = $account_proxy;
    $this->dateFormatter = $date_formatter;
    $this->stringTranslation = $string_translation;
    $this->fileStorage = $file_storage;
    $this->imageStyleStorage = $image_style_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type): self {
    $entity_type_manager = $container->get('entity_type.manager');

    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('current_user'),
      $container->get('date.formatter'),
      $container->get('string_translation'),
      $entity_type_manager->getStorage('file'),
      $entity_type_manager->getStorage('image_style')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->accessCheck()
      ->sort('changed', 'DESC');

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    // Assemble header columns.
    $header = [
      'video' => $this->t('Video'),
      'name' => $this->t('Name'),
      'status' => $this->t('Status'),
      'updated' => $this->t('Updated'),
      'reference_id' => $this->t('Reference ID'),
      'created' => $this->t('Created'),
    ];

    // Add operations header column only if the user has access.
    if ($this->accountProxy->hasPermission('edit brightcove videos') || $this->accountProxy->hasPermission('delete brightcove videos')) {
      $header += parent::buildHeader();
    }

    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\brightcove\Entity\BrightcoveVideo $entity */
    if (($entity->isPublished() && $this->accountProxy->hasPermission('view published brightcove videos')) || (!$entity->isPublished() && $this->accountProxy->hasPermission('view unpublished brightcove videos'))) {
      $name = Link::fromTextAndUrl($entity->label(), new Url(
        'entity.brightcove_video.canonical', [
          'brightcove_video' => $entity->id(),
        ]
      ));
    }
    else {
      $name = $entity->label();
    }

    // Get thumbnail image style and render it.
    $thumbnail = $entity->getThumbnail();
    $thumbnail_image = '';
    if (!empty($thumbnail['target_id'])) {
      /** @var \Drupal\file\Entity\File $thumbnail_file */
      $thumbnail_file = $this->fileStorage->load($thumbnail['target_id']);
      /** @var \Drupal\image\Entity\ImageStyle $image_style */
      $image_style = $this->imageStyleStorage->load('brightcove_videos_list_thumbnail');
      if (!empty($thumbnail_file) && !is_null($image_style)) {
        $image_uri = $image_style->buildUrl($thumbnail_file->getFileUri());
        $thumbnail_image = "<img src='{$image_uri}' alt='{$entity->getName()}'>";
      }
    }

    // Assemble row.
    $row = [
      'video' => [
        'data' => [
          '#markup' => $thumbnail_image,
        ],
      ],
      'name' => $name,
      'status' => $entity->isPublished() ? $this->t('Active') : $this->t('Inactive'),
      'updated' => $this->dateFormatter->format($entity->getChangedTime(), 'short'),
      'reference_id' => $entity->getReferenceId(),
      'created' => $this->dateFormatter->format($entity->getCreatedTime(), 'short'),
    ];

    // Add operations column only if the user has access.
    if ($this->accountProxy->hasPermission('edit brightcove videos') || $this->accountProxy->hasPermission('delete brightcove videos')) {
      $row += parent::buildRow($entity);
    }

    return $row;
  }

}
