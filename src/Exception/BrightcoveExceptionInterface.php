<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Exception;

/**
 * Module specific exception interface.
 */
interface BrightcoveExceptionInterface extends \Throwable {}
