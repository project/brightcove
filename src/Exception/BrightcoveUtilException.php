<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Exception;

/**
 * Brightcove util exception.
 */
class BrightcoveUtilException extends \Exception implements BrightcoveExceptionInterface {}
