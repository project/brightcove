<?php

declare(strict_types = 1);

namespace Drupal\brightcove;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides common interface for CMS entities.
 */
interface BrightcoveCMSEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityChangedFieldsInterface, EntityOwnerInterface {

  /**
   * Gets the Brightcove CMS entity name.
   *
   * @return string
   *   Name of the Brightcove CMS entity.
   */
  public function getName(): ?string;

  /**
   * Sets the Brightcove CMS entity name.
   *
   * @param string $name
   *   The Brightcove CMS entity name.
   *
   * @return \Drupal\brightcove\BrightcoveCMSEntityInterface
   *   The called Brightcove CMS entity.
   */
  public function setName(string $name): BrightcoveCMSEntityInterface;

  /**
   * Returns the Brightcove Client API target ID.
   *
   * @return string|null
   *   Target ID of the Brightcove Client API.
   */
  public function getApiClient(): ?string;

  /**
   * Sets the Brightcove Client API target ID.
   *
   * @param string $api_client
   *   Target ID of the Brightcove Client API.
   *
   * @return \Drupal\brightcove\BrightcoveCMSEntityInterface
   *   The called Brightcove CMS entity.
   */
  public function setApiClient(string $api_client): BrightcoveCMSEntityInterface;

  /**
   * Returns the description.
   *
   * @return string|null
   *   The description of the CMS entity.
   */
  public function getDescription(): ?string;

  /**
   * Sets the CMS entity's description.
   *
   * @param string $description
   *   The description of the CMS entity.
   *
   * @return \Drupal\brightcove\BrightcoveCMSEntityInterface
   *   The called Brightcove CMS entity.
   */
  public function setDescription(string $description): BrightcoveCMSEntityInterface;

  /**
   * Gets the Brightcove CMS entity creation timestamp.
   *
   * @return int|null
   *   Creation timestamp of the Brightcove CMS entity.
   */
  public function getCreatedTime(): ?int;

  /**
   * Sets the Brightcove CMS entity creation timestamp.
   *
   * @param int $timestamp
   *   The Brightcove CMS entity creation timestamp.
   *
   * @return \Drupal\brightcove\BrightcoveCMSEntityInterface
   *   The called Brightcove CMS entity.
   */
  public function setCreatedTime(int $timestamp): BrightcoveCMSEntityInterface;

}
