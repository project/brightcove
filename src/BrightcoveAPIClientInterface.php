<?php

declare(strict_types = 1);

namespace Drupal\brightcove;

use Brightcove\API\Client;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Brightcove API Client entities.
 */
interface BrightcoveAPIClientInterface extends ConfigEntityInterface {

  /**
   * Indicates the default player for any API Client.
   */
  const DEFAULT_PLAYER = 'default';

  /**
   * Indicates that the connection to the API was not successful.
   */
  const CLIENT_ERROR = 0;

  /**
   * Indicates that the connection to the API was successful.
   */
  const CLIENT_OK = 1;

  /**
   * Indicates that the connection is being initialized.
   */
  const CLIENT_INITIALIZING = 2;

  /**
   * Returns the API Client label.
   *
   * @return string|null
   *   The label for this API Client.
   */
  public function getLabel(): ?string;

  /**
   * Returns the API Client account ID.
   *
   * @return string|null
   *   The account ID for this API Client.
   */
  public function getAccountId(): ?string;

  /**
   * Returns the API Client ID.
   *
   * @return string|null
   *   The client ID for this API Client.
   */
  public function getClientId(): ?string;

  /**
   * Returns the API Client default player.
   *
   * @return string|null
   *   The default player for this API Client.
   */
  public function getDefaultPlayer(): ?string;

  /**
   * Returns the API Client secret key.
   *
   * @return string|null
   *   The secret key for this API Client.
   */
  public function getSecretKey(): ?string;

  /**
   * Returns the loaded API client.
   *
   * @return \Brightcove\API\Client
   *   Loaded API client.
   */
  public function getClient(): Client;

  /**
   * Returns the connection status.
   *
   * @return int
   *   Possible values:
   *     - CLIENT_OK
   *     - CLIENT_ERROR
   *     - CLIENT_INITIALIZING
   */
  public function getClientStatus(): int;

  /**
   * Returns the connection status message.
   *
   * @return string
   *   The connection status message.
   */
  public function getClientStatusMessage(): string;

  /**
   * Returns access token.
   *
   * @return string|null
   *   The access token.
   */
  public function getAccessToken(): ?string;

  /**
   * Returns the maximum number of addable custom fields.
   *
   * @return int
   *   The maximum number of addable custom fields.
   */
  public function getMaxCustomFields(): int;

  /**
   * Sets the API Client label.
   *
   * @param string $label
   *   The desired label.
   *
   * @return $this
   */
  public function setLabel(string $label): BrightcoveAPIClientInterface;

  /**
   * Sets the API Client account ID.
   *
   * @param string $account_id
   *   The desired account ID.
   *
   * @return $this
   */
  public function setAccountId(string $account_id): BrightcoveAPIClientInterface;

  /**
   * Sets the API Client ID.
   *
   * @param string $client_id
   *   The desired client ID.
   *
   * @return $this
   */
  public function setClientId(string $client_id): BrightcoveAPIClientInterface;

  /**
   * Sets the API Client default player.
   *
   * @param string $default_player
   *   The desired default player.
   *
   * @return $this
   */
  public function setDefaultPlayer(string $default_player): BrightcoveAPIClientInterface;

  /**
   * Sets the API Client secret key.
   *
   * @param string $secret_key
   *   The desired secret key.
   *
   * @return $this
   */
  public function setSecretKey(string $secret_key): BrightcoveAPIClientInterface;

  /**
   * Sets access token.
   *
   * @param string $access_token
   *   The access token.
   * @param int $expire
   *   The time for which the token is valid in seconds.
   *
   * @return $this
   */
  public function setAccessToken(string $access_token, int $expire): BrightcoveAPIClientInterface;

  /**
   * Sets the maximum addable custom fields number.
   *
   * @param int $max_custom_fields
   *   The maximum custom fields number.
   *
   * @return $this
   */
  public function setMaxCustomFields(int $max_custom_fields): BrightcoveAPIClientInterface;

}
