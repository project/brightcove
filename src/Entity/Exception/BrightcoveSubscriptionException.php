<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Entity\Exception;

use Drupal\brightcove\Exception\BrightcoveExceptionInterface;

/**
 * Brightcove Subscription exception.
 */
class BrightcoveSubscriptionException extends \Exception implements BrightcoveExceptionInterface {}
