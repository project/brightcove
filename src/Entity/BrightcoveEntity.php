<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Entity;

use Drupal\Core\Entity\EntityBase;
use Drupal\Core\Entity\EntityConstraintViolationList;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Base class for Brightcove entities.
 */
abstract class BrightcoveEntity extends EntityBase implements BrightcoveEntityInterface {

  /**
   * Field item list.
   *
   * @var \Drupal\Core\Field\FieldItemListInterface[]
   */
  private $fields;

  /**
   * Validation required flag.
   *
   * @var bool
   */
  private $validationRequired;

  /**
   * Base initializer for Brightcove entities.
   *
   * @param string $entity_type
   *   Entity type ID.
   *
   * @noinspection PhpMissingParentConstructorInspection
   *   Overrides parent constructor.
   */
  public function __construct(string $entity_type) {
    $this->entityTypeId = $entity_type;
    $this->fields = [];
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function hasField($field_name): bool {
    return !empty($this->getFieldDefinition($field_name));
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldDefinition($name) {
    if (!isset($this->fieldDefinitions)) {
      $this->getFieldDefinitions();
    }
    if (isset($this->fieldDefinitions[$name])) {
      return $this->fieldDefinitions[$name];
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldDefinitions(): array {
    if (!isset($this->fieldDefinitions)) {
      $this->fieldDefinitions = \Drupal::service('entity_field.manager')
        ->getFieldDefinitions($this->entityTypeId, $this->bundle());
    }
    return $this->fieldDefinitions;
  }

  /**
   * {@inheritdoc}
   */
  public function get($field_name): FieldItemListInterface {
    if (!isset($this->fields[$field_name])) {
      $definition = $this->getFieldDefinition($field_name);
      if (empty($definition) || !property_exists($this, $field_name)) {
        throw new \InvalidArgumentException(strtr('Field :name is unknown.', [
          ':name' => $field_name,
        ]));
      }

      $value = $this->{$field_name} ?? NULL;
      $this->fields[$field_name] = \Drupal::service('plugin.manager.field.field_type')
        ->createFieldItemList($this, $field_name, $value);
    }
    return $this->fields[$field_name];
  }

  /**
   * {@inheritdoc}
   */
  public function set($field_name, $value, $notify = TRUE): self {
    $value = $this->get($field_name);
    $value->setValue($value, $notify);
    $value->validate();
    $this->{$field_name} = $value;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFields($include_computed = TRUE, bool $translate = FALSE): array {
    $fields = [];
    foreach ($this->getFieldDefinitions() as $name => $definition) {
      if (($include_computed || !$definition->isComputed()) && (!$translate || $definition->isTranslatable())) {
        $fields[$name] = $this->get($name);
      }
    }
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslatableFields($include_computed = TRUE): array {
    return $this->getFields($include_computed, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function onChange($field_name): void {
    // Nothing to do here for now.
  }

  /**
   * {@inheritdoc}
   */
  public function validate(): EntityConstraintViolationListInterface {
    $violations = $this->getTypedData()->validate();
    return new EntityConstraintViolationList($this, iterator_to_array($violations));
  }

  /**
   * {@inheritdoc}
   */
  public function isValidationRequired(): bool {
    return $this->validationRequired;
  }

  /**
   * {@inheritdoc}
   */
  public function setValidationRequired($required): self {
    $this->validationRequired = (bool) $required;
    return $this;
  }

  /**
   * Checks if this entity is the default revision.
   *
   * Added for compatibility reasons as
   * Drupal\Core\Entity\EntityViewBuilder::getBuildDefaults() method doesn't
   * properly check if the entity is revisionable or not.
   * Remove if https://www.drupal.org/project/drupal/issues/2951487 resolved.
   *
   * @param bool|null $new_value
   *   (Re)sets the isDefaultRevision flag.
   *
   * @return bool
   *   Always TRUE as by default revisions are not supported by this type of
   *   entity.
   */
  public function isDefaultRevision(?bool $new_value = NULL): bool {
    return TRUE;
  }

}
