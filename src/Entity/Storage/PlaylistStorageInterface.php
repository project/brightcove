<?php

namespace Drupal\brightcove\Entity\Storage;

use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines required methods for a player storage.
 */
interface PlaylistStorageInterface extends EntityStorageInterface {

  /**
   * Deletes permanently saved entities.
   *
   * @param \Drupal\brightcove\BrightcovePlaylistInterface[] $entities
   *   An array of playlists to delete.
   * @param bool $remote
   *   TRUE to delete the remote and local playlists, FALSE to only local
   *   versions.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of storage failure.
   * @throws \Brightcove\API\Exception\APIException
   *   If the remote entity cannot be deleted.
   */
  public function delete(array $entities, bool $remote = TRUE): void;

}
