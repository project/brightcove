<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Entity\Storage;

use Brightcove\API\Exception\APIException;
use Brightcove\Item\Video\Link;
use Brightcove\Item\Video\Schedule;
use Brightcove\Item\Video\TextTrack;
use Brightcove\Item\Video\TextTrackSource;
use Brightcove\Item\Video\Video;
use Drupal\brightcove\BrightcoveUtil;
use Drupal\brightcove\BrightcoveVideoInterface;
use Drupal\brightcove\Entity\BrightcoveVideo;
use Drupal\brightcove\Services\IngestionInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Drupal\taxonomy\TermStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Playlist entity storage.
 */
final class VideoStorage extends SqlContentEntityStorage implements VideoStorageInterface {

  /**
   * Custom field storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $customFieldStorage;

  /**
   * Ingestion service.
   *
   * @var \Drupal\brightcove\Services\Ingestion
   */
  private $ingestion;

  /**
   * Taxonomy term storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $termStorage;

  /**
   * Text track storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $textTrackStorage;

  /**
   * Initializes a new Video storage.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend to be used.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $memory_cache
   *   The memory cache backend to be used.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\brightcove\Services\IngestionInterface $ingestion
   *   Ingestion.
   * @param \Drupal\Core\Entity\EntityStorageInterface $custom_field_storage
   *   Custom Field storage.
   * @param \Drupal\taxonomy\TermStorageInterface $term_storage
   *   Term storage.
   * @param \Drupal\Core\Entity\EntityStorageInterface $text_track_storage
   *   Text Track storage.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    Connection $database,
    EntityFieldManagerInterface $entity_field_manager,
    CacheBackendInterface $cache,
    LanguageManagerInterface $language_manager,
    MemoryCacheInterface $memory_cache,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    EntityTypeManagerInterface $entity_type_manager,
    IngestionInterface $ingestion,
    EntityStorageInterface $custom_field_storage,
    TermStorageInterface $term_storage,
    EntityStorageInterface $text_track_storage,
  ) {
    parent::__construct($entity_type, $database, $entity_field_manager, $cache, $language_manager, $memory_cache, $entity_type_bundle_info, $entity_type_manager);

    $this->customFieldStorage = $custom_field_storage;
    $this->ingestion = $ingestion;
    $this->termStorage = $term_storage;
    $this->textTrackStorage = $text_track_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type): self {
    $entity_type_manager = $container->get('entity_type.manager');

    return new static(
      $entity_type,
      $container->get('database'),
      $container->get('entity_field.manager'),
      $container->get('cache.entity'),
      $container->get('language_manager'),
      $container->get('entity.memory_cache'),
      $container->get('entity_type.bundle.info'),
      $entity_type_manager,
      $container->get('brightcove.ingestion'),
      $entity_type_manager->getStorage('brightcove_custom_field'),
      $entity_type_manager->getStorage('taxonomy_term'),
      $entity_type_manager->getStorage('brightcove_text_track')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function save(EntityInterface $entity, bool $upload = FALSE) {
    /** @var \Drupal\brightcove\BrightcoveVideoInterface $entity */

    /* @noinspection PhpUndefinedFieldInspection */
    $entity->_upload = $upload;
    $status = parent::save($entity);

    try {
      if ($upload) {
        $this->ingestion->sendIngestVideo($entity);
      }
    }
    finally {
      // Reset changed fields.
      $entity->clearChangedFields();
    }

    return $status;
  }

  /**
   * {@inheritdoc}
   */
  protected function doPreSave(EntityInterface $entity) {
    /** @var \Drupal\brightcove\BrightcoveVideoInterface $entity */
    $id = parent::doPreSave($entity);

    // Upload data for Brightcove only if we saved the video from form.
    if ($entity->_upload ?? FALSE) {
      $cms = BrightcoveUtil::getCmsApi($entity->getApiClient());

      // Setup video object and set minimum required values.
      $video = new Video();
      $video->setName($entity->getName());

      // Mark Video's asset fields for ingestion if any of the ingestable assets
      // are set or changed.
      if ($entity->isFieldChanged(BrightcoveVideoInterface::IMAGE_TYPE_POSTER) && !empty($entity->getPoster()['target_id'])) {
        $this->ingestion->setFieldMarkedForIngestion($entity, BrightcoveVideoInterface::IMAGE_TYPE_POSTER, TRUE);
      }
      if ($entity->isFieldChanged(BrightcoveVideoInterface::IMAGE_TYPE_THUMBNAIL) && !empty($entity->getThumbnail()['target_id'])) {
        $this->ingestion->setFieldMarkedForIngestion($entity, BrightcoveVideoInterface::IMAGE_TYPE_THUMBNAIL, TRUE);
      }
      if ($entity->isFieldChanged('text_tracks') && !empty($entity->getTextTracks())) {
        $this->ingestion->setFieldMarkedForIngestion($entity, 'text_tracks', TRUE);
      }

      // Save or update description if needed.
      if ($entity->isFieldChanged('description')) {
        $video->setDescription($entity->getDescription());
      }

      // Save or update duration if needed.
      if ($entity->isFieldChanged('duration')) {
        $video->setDuration($entity->getDuration());
      }

      // Save or update economics if needed.
      if ($entity->isFieldChanged('economics')) {
        $video->setEconomics($entity->getEconomics());
      }

      // Save or update tags if needed.
      if ($entity->isFieldChanged('tags')) {
        // Get term IDs.
        $term_ids = [];
        foreach ($entity->getTags() as $tag) {
          $term_ids[] = $tag['target_id'];
        }

        // Load terms.
        /** @var \Drupal\taxonomy\Entity\Term[] $terms */
        $terms = $this->termStorage->loadMultiple($term_ids);
        $tags = [];
        foreach ($terms as $term) {
          $tags[] = $term->getName();
        }

        $video->setTags($tags);
      }

      // Save or update link if needed.
      if ($entity->isFieldChanged('related_link')) {
        $link = new Link();
        $related_link = $entity->getRelatedLink();
        $is_link_set = FALSE;

        if (!empty($related_link['uri'])) {
          $link->setUrl(Url::fromUri($related_link['uri'], ['absolute' => TRUE])->toString());
          $is_link_set = TRUE;
        }

        if (!empty($related_link['title'])) {
          $link->setText($related_link['title']);
          $is_link_set = TRUE;
        }

        if ($is_link_set) {
          $video->setLink($link);
        }
        else {
          $video->setLink();
        }
      }

      // Save or update long description if needed.
      if ($entity->isFieldChanged('long_description')) {
        $video->setLongDescription($entity->getLongDescription());
      }

      // Save or update reference ID if needed.
      if ($entity->isFieldChanged('reference_id')) {
        $video->setReferenceId($entity->getReferenceId());
      }

      // Save or update custom field values.
      $custom_field_num = $this->customFieldStorage->getQuery()
        ->condition('api_client', $entity->getApiClient())
        ->accessCheck()
        ->count()
        ->execute();
      if ($entity->isFieldChanged('custom_field_values') && $custom_field_num > 0) {
        $video->setCustomFields($entity->getCustomFieldValues());
      }

      // Save or update schedule start at date if needed.
      if ($entity->isFieldChanged('schedule_starts_at') || $entity->isFieldChanged('schedule_ends_at')) {
        $starts_at = $entity->getScheduleStartsAt();
        $ends_at = $entity->getScheduleEndsAt();
        $schedule = new Schedule();
        $is_schedule_set = FALSE;

        if ($starts_at !== NULL) {
          $schedule->setStartsAt($starts_at . '.000Z');
          $is_schedule_set = TRUE;
        }

        if ($ends_at !== NULL) {
          $schedule->setEndsAt($ends_at . '.000Z');
          $is_schedule_set = TRUE;
        }

        if ($is_schedule_set) {
          $video->setSchedule($schedule);
        }
        else {
          $video->setSchedule();
        }
      }

      // Upload text tracks.
      if ($entity->isFieldChanged('text_tracks')) {
        $video_text_tracks = [];
        foreach ($entity->getTextTracks() as $text_track) {
          if (!empty($text_track['target_id'])) {
            /** @var \Drupal\brightcove\Entity\BrightcoveTextTrack $text_track_entity */
            $text_track_entity = $this->textTrackStorage->load($text_track['target_id']);

            if ($text_track_entity !== NULL) {
              // Setup ingestion request if there was a text track uploaded.
              $webvtt_file = $text_track_entity->getWebVttFile();
              // Build the whole text track for Brightcove.
              if (empty($webvtt_file[0]['target_id'])) {
                $video_text_track = (new TextTrack())
                  ->setId($text_track_entity->getTextTrackId())
                  ->setSrclang($text_track_entity->getSourceLanguage())
                  ->setLabel($text_track_entity->getLabel())
                  ->setKind($text_track_entity->getKind())
                  ->setMimeType($text_track_entity->getMimeType())
                  ->setAssetId($text_track_entity->getAssetId());

                // If asset ID is set the src will be ignored, so in this case
                // we don't set the src.
                if (!empty($text_track_entity->getAssetId())) {
                  $video_text_track->setAssetId($text_track_entity->getAssetId());
                }
                // Otherwise, set the src.
                else {
                  $source = $text_track_entity->getSource();
                  $video_text_track->setSrc(!empty($source) ? $source['uri'] : '');
                }

                // Get text track sources.
                $video_text_track_sources = [];
                foreach ($text_track_entity->getSources() as $source) {
                  $text_track_source = new TextTrackSource();
                  $text_track_source->setSrc($source['uri']);
                  $video_text_track_sources[] = $text_track_source;
                }
                $video_text_track->setSources($video_text_track_sources);

                $video_text_tracks[] = $video_text_track;
              }
            }
          }
        }

        // Set existing text tracks.
        $video->setTextTracks($video_text_tracks);
      }

      // Update status field based on Brightcove's Video state.
      if ($entity->isFieldChanged('status')) {
        $video->setState($entity->isPublished() ? BrightcoveVideoInterface::STATE_ACTIVE : BrightcoveVideoInterface::STATE_INACTIVE);
      }

      // Create or update a video.
      if ($entity->isNew()) {
        // Create new video on Brightcove.
        $saved_video = $cms->createVideo($video);

        // Set the rest of the fields on BrightcoveVideo entity.
        $entity->setBrightcoveId($saved_video->getId());
        $entity->setCreatedTime(strtotime($saved_video->getCreatedAt()));
      }
      else {
        // Set Video ID.
        $video->setId($entity->getBrightcoveId());

        // Update video.
        $saved_video = $cms->updateVideo($video);
      }

      // Update changed time and video entity with the video ID.
      if (isset($saved_video)) {
        $entity->setChangedTime(strtotime($saved_video->getUpdatedAt()));
      }
    }

    return $id;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(array $entities, bool $remote = TRUE): void {
    if ($remote) {
      $this->deleteRemoteMultiple($entities);
    }
    parent::delete($entities);
  }

  /**
   * Deletes multiple remote videos.
   *
   * @param \Drupal\brightcove\Entity\BrightcoveVideo[] $videos
   *   The list of remote videos to delete.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of storage failure.
   * @throws \Brightcove\API\Exception\APIException
   *   If the remote entity cannot be deleted.
   */
  private function deleteRemoteMultiple(array $videos): void {
    foreach ($videos as $video) {
      $this->deleteRemote($video);
    }
  }

  /**
   * Deletes a remote video.
   *
   * @param \Drupal\brightcove\Entity\BrightcoveVideo $video
   *   The remote video to delete.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of storage failure.
   * @throws \Brightcove\API\Exception\APIException
   *   If the remote entity cannot be deleted.
   */
  private function deleteRemote(BrightcoveVideo $video): void {
    if (!$video->isNew()) {
      $api_client = BrightcoveUtil::getApiClient($video->getApiClient());
      $client = $api_client->getClient();

      // Delete video references.
      try {
        $client->request('DELETE', 1, 'cms', $api_client->getAccountId(), "/videos/{$video->getBrightcoveId()}/references", NULL);

        // Delete video.
        $cms = BrightcoveUtil::getCmsApi($video->getApiClient());
        $cms->deleteVideo($video->getBrightcoveId());
        parent::delete([$video]);
      }
      catch (APIException $e) {
        if ($e->getCode() === 404) {
          // Video not found remotely, remove the local version.
          parent::delete([$video]);
        }
        throw $e;
      }
    }
  }

}
