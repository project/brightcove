<?php

namespace Drupal\brightcove\Entity\Storage;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines required methods for a player storage.
 */
interface VideoStorageInterface extends EntityStorageInterface {

  /**
   * Saves the Video entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Video entity.
   * @param bool $upload
   *   Whether to upload the video to Brightcove or not.
   *
   * @return int|bool
   *   Either SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   *   FALSE returned if there was an error.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of a failure an exception is thrown.
   */
  public function save(EntityInterface $entity, bool $upload = FALSE);

  /**
   * Deletes permanently saved Videos.
   *
   * @param \Drupal\brightcove\BrightcoveVideoInterface[] $entities
   *   An array of Video to delete.
   * @param bool $remote
   *   TRUE to delete the remote and local Video, FALSE to only local
   *   versions.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of storage failure.
   * @throws \Brightcove\API\Exception\APIException
   *   If the remote entity cannot be deleted.
   */
  public function delete(array $entities, bool $remote = TRUE): void;

}
