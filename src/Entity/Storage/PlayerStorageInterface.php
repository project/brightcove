<?php

namespace Drupal\brightcove\Entity\Storage;

use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Required methods for a player entity storage.
 */
interface PlayerStorageInterface extends EntityStorageInterface {

  /**
   * Returns a list of players.
   *
   * @param array|null $api_client
   *   The API Client for which the players should be returned. If it's NULL,
   *   then only the default player will be returned.
   * @param bool $use_entity_id
   *   TRUE to use the Player entity's IDs or FALSE to use Brightcove's IDs for
   *   the player's key.
   *   In case of TRUE and default player won't be included because that doesn't
   *   have a real entity representation.
   *
   * @return array
   *   A list of player names keyed by their Brightcove ID or by the Entity ID
   *   if $use_entity_id is set.
   */
  public function getList(?array $api_client, bool $use_entity_id = FALSE): array;

}
