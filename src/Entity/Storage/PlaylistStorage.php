<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Entity\Storage;

use Brightcove\API\Exception\APIException;
use Drupal\brightcove\BrightcovePlaylistInterface;
use Drupal\brightcove\BrightcoveUtil;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Playlist entity storage.
 */
final class PlaylistStorage extends SqlContentEntityStorage implements PlaylistStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function delete(array $entities, bool $remote = TRUE): void {
    if ($remote) {
      $this->deleteRemoteMultiple($entities);
    }
    parent::delete($entities);
  }

  /**
   * Deletes remote playlists.
   *
   * @param \Drupal\brightcove\BrightcovePlaylistInterface[] $playlists
   *   The list of playlists to delete.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of storage failure.
   * @throws \Brightcove\API\Exception\APIException
   *   If the remote entity cannot be deleted.
   */
  private function deleteRemoteMultiple(array $playlists): void {
    foreach ($playlists as $playlist) {
      $this->deleteRemote($playlist);
    }
  }

  /**
   * Deletes a remote playlist.
   *
   * @param \Drupal\brightcove\BrightcovePlaylistInterface $playlist
   *   The playlist to delete.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of storage failure.
   * @throws \Brightcove\API\Exception\APIException
   *   If the remote entity cannot be deleted.
   */
  private function deleteRemote(BrightcovePlaylistInterface $playlist): void {
    if (!$playlist->isNew()) {
      try {
        $cms = BrightcoveUtil::getCmsApi($playlist->getApiClient());
        $cms->deletePlaylist($playlist->getBrightcoveId());
      }
      catch (APIException $e) {
        if ($e->getCode() === 404) {
          // Playlist not found remotely, remove the local version.
          parent::delete([$playlist]);
        }
        throw $e;
      }
    }
  }

}
