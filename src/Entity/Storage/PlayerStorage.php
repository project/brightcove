<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Entity\Storage;

use Drupal\brightcove\BrightcoveAPIClientInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Player entity storage.
 */
final class PlayerStorage extends SqlContentEntityStorage implements PlayerStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function getList(?array $api_client, bool $use_entity_id = FALSE): array {
    // If the use_entity_id set to TRUE then don't add the default player
    // to the list, because it would be a non-existing entity.
    if ($use_entity_id) {
      $players = [];
    }
    else {
      // Provide default player.
      $players = [
        BrightcoveAPIClientInterface::DEFAULT_PLAYER => $this->t('Brightcove Default Player'),
      ];
    }

    // If no API Client provided return the default player.
    if (empty($api_client)) {
      return $players;
    }

    // Collect Players referencing a given API Client.
    /** @var \Drupal\brightcove\Entity\BrightcovePlayer[] $player_entities */
    $player_entities = $this->loadByProperties([
      'api_client' => $api_client,
    ]);

    foreach ($player_entities as $player_entity) {
      $players[$use_entity_id ? $player_entity->id() : $player_entity->getPlayerId()] = $player_entity->getName();
    }

    return $players;
  }

}
