<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Entity;

use Drupal\brightcove\BrightcoveVideoPlaylistCMSEntityInterface;
use Drupal\node\NodeInterface;

/**
 * Common base class for CMS entities like Video and Playlist.
 */
abstract class BrightcoveVideoPlaylistCmsEntity extends BrightcoveCmsEntity implements BrightcoveVideoPlaylistCMSEntityInterface {

  /**
   * {@inheritdoc}
   */
  public function getPlayer(): ?int {
    $player = $this->get('player');
    return !$player->isEmpty() ? (int) $player->target_id : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setPlayer(int $player): BrightcoveVideoPlaylistCMSEntityInterface {
    return $this->set('player', $player);
  }

  /**
   * {@inheritdoc}
   */
  public function getReferenceId(): ?string {
    return $this->get('reference_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setReferenceId(?string $reference_id): BrightcoveVideoPlaylistCMSEntityInterface {
    return $this->set('reference_id', $reference_id);
  }

  /**
   * Default value callback for 'reference_id' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getDefaultReferenceId(): array {
    return [
      'drupal:' . \Drupal::CORE_COMPATIBILITY . ":" . \Drupal::currentUser()->id() . ":" . md5(microtime()),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTags(): array {
    return $this->get('tags')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setTags(array $tags): BrightcoveVideoPlaylistCMSEntityInterface {
    $this->set('tags', $tags);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished(): bool {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished(bool $published) {
    return $this->set('status', $published ? NodeInterface::PUBLISHED : NodeInterface::NOT_PUBLISHED);
  }

}
