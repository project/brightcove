<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Entity;

use Drupal\brightcove\BrightcoveAPIClientInterface;

/**
 * Provides an interface for defining Brightcove Subscription entities.
 */
interface BrightcoveSubscriptionInterface {

  /**
   * The status of the Subscription.
   *
   * @return bool
   *   It always returns TRUE except for default, which can be FALSE.
   */
  public function isActive(): bool;

  /**
   * Whether the Subscription is default or not.
   *
   * @return bool
   *   If the current Subscription is default return TRUE, otherwise FALSE.
   */
  public function isDefault(): bool;

  /**
   * Determines whether an entity is new or not.
   *
   * @return bool
   *   If the entity doesn't have an ID then it will be treated as new, so TRUE
   *   will be return, otherwise FALSE will be return if it's an already
   *   existing entity.
   */
  public function isNew(): bool;

  /**
   * Returns the API Client ID.
   *
   * @return \Drupal\brightcove\BrightcoveAPIClientInterface|null
   *   The API Client for this Subscription.
   */
  public function getApiClient(): ?BrightcoveAPIClientInterface;

  /**
   * Returns the Brightcove Subscription ID.
   *
   * @return string|null
   *   Brightcove Subscription ID if exist, NULL otherwise.
   */
  public function getBcSid(): ?string;

  /**
   * Returns the Subscription endpoint.
   *
   * @return string|null
   *   The endpoint for the Subscription.
   */
  public function getEndpoint(): ?string;

  /**
   * Returns subscribed events.
   *
   * @return string[]
   *   Array of events subscribed to.
   */
  public function getEvents(): array;

  /**
   * Gets the Subscription's Drupal ID.
   *
   * @return int|null
   *   ID of the subscription, or NULL if it's a new entity.
   */
  public function getId(): ?int;

  /**
   * Sets the API Client ID.
   *
   * @param \Drupal\brightcove\Entity\BrightcoveAPIClient|null $api_client
   *   The API Client.
   *
   * @return $this
   *   The current object.
   */
  public function setApiClient(?BrightcoveAPIClientInterface $api_client): BrightcoveSubscriptionInterface;

  /**
   * Sets the Brightcove Subscription ID.
   *
   * @param string|null $bcsid
   *   Brightcove Subscription ID.
   *
   * @return $this
   *   The current object.
   */
  public function setBcSid(?string $bcsid): BrightcoveSubscriptionInterface;

  /**
   * Set the endpoint for the subscription.
   *
   * @param string $endpoint
   *   The Subscription's endpoint.
   *
   * @return $this
   *   The current object.
   */
  public function setEndpoint(string $endpoint): BrightcoveSubscriptionInterface;

  /**
   * Sets the events for which we want to subscribe.
   *
   * @param string[] $events
   *   Array of events to subscribe to.
   *
   * @return $this
   *   The current object.
   */
  public function setEvents(array $events): BrightcoveSubscriptionInterface;

  /**
   * Set the entity's status.
   *
   * @param bool $status
   *   TRUE if enabled, FALSE if disabled.
   *
   * @return $this
   *   The current object.
   *
   * @throws \Exception
   *   In case of an error.
   */
  public function setStatus(bool $status): BrightcoveSubscriptionInterface;

  /**
   * Saves the subscription entity.
   *
   * @param bool $upload
   *   Whether to upload the new subscription to Brightcove or not.
   *
   * @throws \Drupal\brightcove\Entity\Exception\BrightcoveSubscriptionException
   *   If an error happens during saving the subscription.
   */
  public function save(bool $upload = FALSE): void;

  /**
   * Saves the subscription entity to Brightcove.
   *
   * @throws \Drupal\brightcove\Entity\Exception\BrightcoveSubscriptionException
   *   If the Subscription wasn't saved to Brightcove successfully.
   */
  public function saveToBrightcove(): void;

  /**
   * Deletes the Brightcove Subscription.
   *
   * @param bool $local_only
   *   If TRUE delete the local Subscription entity only, otherwise delete the
   *   subscription from Brightcove as well.
   *
   * @throws \Drupal\brightcove\Entity\Exception\BrightcoveSubscriptionException
   *   If the subscription failed to be deleted.
   */
  public function delete(bool $local_only = TRUE): void;

  /**
   * Delete the Subscription from Brightcove only.
   *
   * @throws \Drupal\brightcove\Entity\Exception\BrightcoveSubscriptionException
   * @throws \Exception
   */
  public function deleteFromBrightcove(): void;

}
