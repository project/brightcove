<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Entity;

use Brightcove\Item\Video\Image;
use Drupal\brightcove\Entity\Storage\VideoStorageInterface;
use Drupal\Core\File\FileSystemInterface;
use Brightcove\API\Exception\APIException;
use Brightcove\Item\Video\Video;
use Drupal\brightcove\BrightcoveUtil;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\brightcove\BrightcoveVideoInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\link\LinkItemInterface;
use Drupal\time_formatter\Plugin\Field\FieldFormatter\TimeFieldFormatter;

/**
 * Defines the Brightcove Video entity.
 *
 * @ingroup brightcove
 *
 * @ContentEntityType(
 *   id = "brightcove_video",
 *   label = @Translation("Brightcove Video"),
 *   handlers = {
 *     "storage" = "Drupal\brightcove\Entity\Storage\VideoStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\brightcove\BrightcoveVideoListBuilder",
 *     "views_data" = "Drupal\brightcove\Entity\BrightcoveVideoViewsData",
 *     "form" = {
 *       "default" = "Drupal\brightcove\Form\BrightcoveVideoForm",
 *       "add" = "Drupal\brightcove\Form\BrightcoveVideoForm",
 *       "edit" = "Drupal\brightcove\Form\BrightcoveVideoForm",
 *       "delete" = "Drupal\brightcove\Form\BrightcoveEntityDeleteForm",
 *     },
 *     "access" = "Drupal\brightcove\Access\BrightcoveVideoAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\brightcove\BrightcoveVideoHtmlRouteProvider",
 *     },
 *     "inline_form" = "Drupal\brightcove\Form\BrightcoveInlineForm",
 *   },
 *   base_table = "brightcove_video",
 *   admin_permission = "administer brightcove videos",
 *   entity_keys = {
 *     "id" = "bcvid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/brightcove_video/{brightcove_video}",
 *     "add-form" = "/brightcove_video/add",
 *     "edit-form" = "/brightcove_video/{brightcove_video}/edit",
 *     "delete-form" = "/brightcove_video/{brightcove_video}/delete",
 *     "collection" = "/admin/content/brightcove_video",
 *   },
 *   field_ui_base_route = "brightcove_video.settings"
 * )
 *
 * phpcs:disable Drupal.Commenting.Deprecated.DeprecatedWrongSeeUrlFormat
 */
class BrightcoveVideo extends BrightcoveVideoPlaylistCmsEntity implements BrightcoveVideoInterface {

  /**
   * Ingestion request object.
   *
   * @var \Brightcove\API\Request\IngestRequest
   *
   * @deprecated in brightcove:3.0.0 and is removed from brightcove:4.0.0.
   *   No longer used as the logic is changed, please use the Ingestion service.
   * @see \Drupal\brightcove\Services\Ingestion::createIngestRequest()
   */
  protected $ingestRequest = NULL;

  /**
   * Create or get an existing ingestion request object.
   *
   * @return \Brightcove\API\Request\IngestRequest
   *   Returns an ingestion request object.
   *
   * @deprecated in brightcove:3.0.0 and is removed from brightcove:4.0.0.
   *   Please use ingestion service to create a new ingestion request object.
   * @see \Drupal\brightcove\Services\Ingestion::createIngestRequest()
   */
  protected function getIngestRequest() {
    if (is_null($this->ingestRequest)) {
      $this->ingestRequest = \Drupal::getContainer()
        ->get('brightcove.ingestion')
        ->createIngestRequest();
    }
    return $this->ingestRequest;
  }

  /**
   * Helper function to provide default values for image fields.
   *
   * The original and the save entity's field arrays is a bit different from
   * each other, so provide the missing values.
   *
   * @param array $values
   *   The field's values array.
   *
   * @return array
   *   Image data.
   */
  protected function provideDefaultValuesForImageField(array $values): array {
    $container = \Drupal::getContainer();

    // Add missing height and/or width if it wouldn't be set.
    if (!empty($values[0]['target_id']) && (empty($values[0]['height']) || empty($values[0]['width']))) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $container->get('entity_type.manager')->getStorage('file')->load($values[0]['target_id']);
      if ($file !== NULL) {
        $image = $container->get('image.factory')->get($file->getFileUri());
        if (!empty($image)) {
          if (empty($values[0]['height'])) {
            $values[0]['height'] = (string) $image->getHeight();
          }
          if (empty($values[0]['width'])) {
            $values[0]['width'] = (string) $image->getWidth();
          }
        }
      }
    }

    // The form builder may set other fields that are not stored on the entity
    // itself, so let's define and return only what is actually stored on the
    // entity's base field definition for an image field.
    return [
      'alt' => $values[0]['alt'] ?? '',
      'height' => $values[0]['height'] ?? '',
      'target_id' => $values[0]['target_id'] ?? '',
      'title' => $values[0]['title'] ?? '',
      'width' => $values[0]['width'] ?? '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function saveImage(string $type, Image $image): BrightcoveVideoInterface {
    // Prepare function name and throw an exception if it doesn't match for an
    // existing function.
    $image_dir = 'public://';
    switch ($type) {
      case self::IMAGE_TYPE_THUMBNAIL:
        $image_dir .= self::VIDEOS_IMAGES_THUMBNAILS_DIR;
        break;

      case self::IMAGE_TYPE_POSTER:
        $image_dir .= self::VIDEOS_IMAGES_POSTERS_DIR;
        break;

      default:
        throw new \Exception(strtr('Invalid type given: :type, the type argument must be either ":thumbnail" or ":poster".', [
          ':type' => $type,
          ':thumbnail' => self::IMAGE_TYPE_THUMBNAIL,
          ':poster' => self::IMAGE_TYPE_POSTER,
        ]));
    }

    // Make type's first character uppercase for correct function name.
    $function = ucfirst($type);

    $container = \Drupal::getContainer();
    $needs_save = TRUE;

    // Try to get the image's filename from Brightcove.
    // @todo Find a drupal friendly solution for file handling.
    $image_source = $image->getSrc();
    preg_match('/\/(?!.*\/)([\w\.-]+)/i', $image_source, $matches);
    if (isset($matches[1])) {
      $file_system = $container->get('file_system');

      // Get entity's image file.
      $entity_image = $this->{"get{$function}"}();
      // If the entity already has an image then load and delete it.
      if (!empty($entity_image['target_id'])) {
        /** @var \Drupal\file\FileInterface $file */
        $file = $container->get('entity_type.manager')
          ->getStorage('file')
          ->load($entity_image['target_id']);

        if ($file !== NULL && $file->getFileName() !== $matches[1]) {
          $this->{"set{$function}"}(NULL);
        }
        elseif ($file !== NULL) {
          $file_path = $file_system->realpath($file->getFileUri());
          $file_md5 = md5_file($file_path);
          $image_md5 = md5_file($image_source);
          if ($file_md5 !== FALSE && $file_md5 === $image_md5) {
            $needs_save = FALSE;
          }
        }
      }

      // Save the new image from Brightcove to the entity.
      if ($needs_save) {
        $image_content = file_get_contents($image_source);
        // Prepare directory and if it was a success try to save the image.
        if (!empty($image_content) && $file_system->prepareDirectory($image_dir, FileSystemInterface::MODIFY_PERMISSIONS | FileSystemInterface::CREATE_DIRECTORY)) {
          $image_name = $matches[1];
          $file = $container->get('file.repository')->writeData($image_content, "{$image_dir}/{$image_name}");

          // Get image information for height and width.
          $image_data = $container->get('image.factory')->get($file->getFileUri());
          $height = NULL;
          $width = NULL;
          if (!empty($image_data)) {
            $height = $image_data->getHeight();
            $width = $image_data->getWidth();
          }

          // Set image.
          $this->{"set{$function}"}((int) $file->id(), $height, $width);
        }
      }
    }

    return $this;
  }

  /**
   * Helper function to delete a file from the entity.
   *
   * @param int $target_id
   *   The target ID of the saved file.
   *
   * @return \Drupal\brightcove\BrightcoveVideoInterface
   *   The called Brightcove Video.
   *
   * @throws \Exception
   *   If the $target_id is not a positive number or zero.
   */
  private function deleteFile(int $target_id): BrightcoveVideoInterface {
    if ($target_id <= 0) {
      throw new \Exception('Target ID must be non-zero number.');
    }

    $file_storage = \Drupal::getContainer()
      ->get('entity_type.manager')
      ->getStorage('file');

    /** @var \Drupal\file\FileInterface $file */
    $file = $file_storage->load($target_id);

    // Delete image.
    // @todo Check for multiple file references when it will be needed.
    // So far as I find it, it is not possible to create multiple file
    // references on the Drupal's UI, so it should be OK now.
    if ($file !== NULL) {
      $file_storage->delete([$file]);
    }

    return $this;
  }

  /**
   * Create an ingestion request for image.
   *
   * @param string $type
   *   The type of the image, possible values are:
   *     - IMAGE_TYPE_POSTER
   *     - IMAGE_TYPE_THUMBNAIL
   *   .
   *
   * @throws \Exception
   *   If the $type is not matched with the possible types.
   *
   * @deprecated in brightcove:3.0.0 and is removed from brightcove:4.0.0.
   *   Please use the entity storage to load videos by account and video ID.
   * @see \Drupal\brightcove\Services\Ingestion::getImageIngestion()
   */
  protected function createIngestImage(string $type): void {
    if (!in_array($type, [self::IMAGE_TYPE_POSTER, self::IMAGE_TYPE_THUMBNAIL])) {
      throw new \Exception(strtr("Invalid type given: @type, the type argument must be either '@thumbnail' or '@poster'.", [
        '@type' => $type,
        '@thumbnail' => self::IMAGE_TYPE_THUMBNAIL,
        '@poster' => self::IMAGE_TYPE_POSTER,
      ]));
    }

    $function = ucfirst($type);
    $image = \Drupal::getContainer()->get('brightcove.ingestion')->getImageIngestion($this->{"get{$function}"}());
    if ($image !== NULL) {
      $this->getIngestRequest()->{"set{$function}"}($image);
    }
  }

  /**
   * Get a random hash token for ingestion request callback.
   *
   * @return string|null
   *   The generated random token or NULL if an error happened.
   *
   * @deprecated in brightcove:3.0.0 and is removed from brightcove:4.0.0.
   *   Please use the ingestion service to get a new token.
   * @see \Drupal\brightcove\Services\Ingestion::getIngestionToken()
   */
  protected function getIngestionToken(): ?string {
    return \Drupal::getContainer()
      ->get('brightcove.ingestion')
      ->getIngestionToken($this);
  }

  /**
   * {@inheritdoc}
   */
  public function id(): ?int {
    $id = $this->getEntityKey('id');
    return $id !== NULL ? (int) $id : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getBrightcoveId(): ?string {
    return $this->get('video_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setBrightcoveId(string $id): BrightcoveVideoInterface {
    $this->set('video_id', $id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDuration(): ?int {
    $duration = $this->get('duration');
    return $duration->value !== NULL ? (int) $duration->value : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setDuration(int $duration): BrightcoveVideoInterface {
    $this->set('duration', $duration);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRelatedLink(): array {
    $value = $this->get('related_link')->getValue() ?? [];

    $link = ($value[0] ?? []) + [
      'uri' => NULL,
      'title' => NULL,
      'options' => NULL,
      'attributes' => [],
    ];
    ksort($link);

    return $link;
  }

  /**
   * {@inheritdoc}
   */
  public function setRelatedLink(?array $related_link): BrightcoveVideoInterface {
    // If the protocol is missing from the link add default http protocol to
    // the link.
    if (!empty($related_link['uri']) && !preg_match('/[\w-]+:\/\//i', $related_link['uri'])) {
      $related_link['uri'] = "http://{$related_link['uri']}";
    }

    $this->set('related_link', $related_link);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLongDescription(): ?string {
    return $this->get('long_description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLongDescription(string $long_description): BrightcoveVideoInterface {
    $this->set('long_description', $long_description);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEconomics(): ?string {
    return $this->get('economics')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEconomics(string $is_economics): BrightcoveVideoInterface {
    $this->set('economics', $is_economics);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getVideoFile(): ?array {
    return $this->cleanEntityReferenceValues('video_file')[0] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setVideoFile(?array $video_file): BrightcoveVideoInterface {
    $video_to_delete = $this->getVideoFile();
    if ($video_file === NULL && !empty($video_to_delete['target_id'])) {
      $this->deleteFile((int) $video_to_delete['target_id']);
    }
    $this->set('video_file', $video_file);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getVideoUrl(): ?string {
    return $this->get('video_url')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setVideoUrl(?string $video_url): BrightcoveVideoInterface {
    $this->set('video_url', $video_url);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProfile(): ?string {
    return $this->get('profile')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setProfile(string $profile): BrightcoveVideoInterface {
    $this->set('profile', $profile);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPoster(): array {
    return $this->provideDefaultValuesForImageField($this->get('poster')->getValue() ?? []);
  }

  /**
   * {@inheritdoc}
   */
  public function setPoster(?int $poster_id, ?int $height = NULL, ?int $width = NULL): BrightcoveVideoInterface {
    // Handle image deletion here as well.
    $poster_to_delete = $this->getPoster();
    if ($poster_id === NULL && !empty($poster_to_delete['target_id'])) {
      $this->deleteFile((int) $poster_to_delete['target_id']);
    }

    $this->set('poster', [
      'target_id' => $poster_id,
      'height' => $height,
      'width' => $width,
    ]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getThumbnail(): array {
    return $this->provideDefaultValuesForImageField($this->get('thumbnail')->getValue() ?? []);
  }

  /**
   * {@inheritdoc}
   */
  public function setThumbnail(?int $thumbnail_id, ?int $height = NULL, ?int $width = NULL): BrightcoveVideoInterface {
    // Handle image deletion here as well.
    $thumbnail_to_delete = $this->getThumbnail();
    if ($thumbnail_id === NULL && !empty($thumbnail_to_delete['target_id'])) {
      $this->deleteFile((int) $thumbnail_to_delete['target_id']);
    }

    $this->set('thumbnail', [
      'target_id' => $thumbnail_id,
      'height' => $height,
      'width' => $width,
    ]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomFieldValues(): array {
    $value = $this->get('custom_field_values')->getValue() ?? [];

    // If the custom field values are set, return the first element as it is a
    // "single value field" with dynamically set properties defined in the
    // Brightcove Studio.
    if (!empty($value[0])) {
      ksort($value[0]);
      return $value[0];
    }

    // If there is no custom field set, the value would be an empty array, so
    // let's return that in this case.
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCustomFieldValues(array $values): BrightcoveVideoInterface {
    return $this->set('custom_field_values', $values);
  }

  /**
   * {@inheritdoc}
   */
  public function getScheduleStartsAt(): ?string {
    $value = $this->get('schedule_starts_at')->getValue();
    if (empty($value)) {
      return NULL;
    }

    return $value[0]['value'];
  }

  /**
   * {@inheritdoc}
   */
  public function setScheduleStartsAt(?string $schedule_starts_at): BrightcoveVideoInterface {
    return $this->set('schedule_starts_at', $schedule_starts_at);
  }

  /**
   * {@inheritdoc}
   */
  public function getScheduleEndsAt(): ?string {
    $value = $this->get('schedule_ends_at')->getValue();
    if (empty($value)) {
      return NULL;
    }

    return $value[0]['value'];
  }

  /**
   * {@inheritdoc}
   */
  public function setScheduleEndsAt(?string $schedule_ends_at): BrightcoveVideoInterface {
    $this->set('schedule_ends_at', $schedule_ends_at);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTextTracks(): array {
    return $this->cleanEntityReferenceValues('text_tracks');
  }

  /**
   * {@inheritdoc}
   */
  public function setTextTracks($text_tracks): BrightcoveVideoInterface {
    return $this->set('text_tracks', $text_tracks);
  }

  /**
   * {@inheritdoc}
   */
  public function getMarkedForIngestion(): array {
    $value = $this->get('marked_for_ingestion')->getValue() ?? [];

    if (!empty($value[0])) {
      ksort($value[0]);
      return $value[0];
    }

    return $value;
  }

  /**
   * Helper function to clean entity reference fields from the form data.
   *
   * The form API may push values for the fields which would break the
   * comparison between the original and the modified entity.
   *
   * @param string $field
   *   The name of the field.
   *
   * @return array
   *   Field value list.
   */
  protected function cleanEntityReferenceValues(string $field): array {
    $values = [];
    foreach ($this->get($field)->getValue() as $key => $value) {
      $values[$key] = array_filter($value, static function ($key): bool {
        return $key === 'target_id';
      }, ARRAY_FILTER_USE_KEY);
    }
    return $values;
  }

  /**
   * Loads a Video based on the Brightcove Video ID and Account ID.
   *
   * @param string $account_id
   *   The ID of the account.
   * @param string $brightcove_video_id
   *   The External ID of the Video.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The matching Brightcove Video or NULL if the video cannot be found.
   *
   * @deprecated in brightcove:3.0.0 and is removed from brightcove:4.0.0.
   *   Please use the entity storage to load videos by account and video ID.
   * @see \Drupal\Core\Entity\EntityStorageInterface::loadByProperties()
   */
  public static function loadByBrightcoveVideoId($account_id, $brightcove_video_id) {
    // Get API Client by Account ID.
    $api_client_ids = \Drupal::entityQuery('brightcove_api_client')
      ->condition('account_id', $account_id)
      ->execute();

    $entity_ids = \Drupal::entityQuery('brightcove_video')
      ->condition('api_client', reset($api_client_ids))
      ->condition('video_id', $brightcove_video_id)
      ->condition('status', self::PUBLISHED)
      ->execute();

    if (!empty($entity_ids)) {
      return self::load(reset($entity_ids));
    }
    return NULL;
  }

  /**
   * Saves the Video entity.
   *
   * @param bool $upload
   *   Whether to upload the video to Brightcove or not.
   *
   * @return int|bool
   *   Either SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   *   FALSE returned if there was an error.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of a failure an exception is thrown.
   */
  public function save(bool $upload = FALSE) {
    /** @var \Drupal\brightcove\Entity\Storage\VideoStorageInterface $storage */
    $storage = $this->entityTypeManager()->getStorage($this->entityTypeId);
    return $storage->save($this, $upload);
  }

  /**
   * Deletes the Video entity.
   *
   * @param bool $local_only
   *   Whether to delete the local version only or both local and Brightcove
   *   versions.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of failures an exception is thrown.
   *
   * @deprecated in brightcove:3.0.0 and is removed from brightcove:4.0.0.
   *   Please use the storage delete to be able to remove the remote entities
   *   as well.
   * @see \Drupal\brightcove\Entity\Storage\VideoStorageInterface::delete()
   */
  public function delete(bool $local_only = FALSE): void {
    $this->entityTypeManager()
      ->getStorage($this->entityTypeId)
      ->delete([$this], !$local_only);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    // Set weights based on the real order of the fields.
    $weight = -30;

    /*
     * Drupal-specific fields first.
     *
     * bcvid - Brightcove Video ID (Drupal-internal).
     * uuid - UUID.
     * - Title comes here, but that's the "Name" field from Brightcove.
     * langcode - Language.
     * api_client - Entityreference to BrightcoveAPIClient.
     * - Brightcove fields come here.
     * uid - Author.
     * created - Posted.
     * changed - Last modified.
     */
    $fields['bcvid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The Drupal entity ID of the Brightcove Video.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The Brightcove Video UUID.'))
      ->setReadOnly(TRUE);

    $fields['api_client'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('API Client'))
      ->setDescription(t('Brightcove API credentials (account) to use.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'brightcove_api_client')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'hidden',
        'region' => 'hidden',
        'label' => 'inline',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['player'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Player'))
      ->setDescription(t('Brightcove Player to be used for playback.'))
      ->setSetting('target_type', 'brightcove_player')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'hidden',
        'region' => 'hidden',
        'label' => 'inline',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Status field, tied together with the status of the entity.
    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enabled'))
      ->setDescription(t('Determines whether the video is playable.'))
      ->setDefaultValue(TRUE)
      ->setSettings([
        'on_label' => t('Active'),
        'off_label' => t('Inactive'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'label' => 'above',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'inline',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('Title of the video.'))
      ->setRequired(TRUE)
      ->setSettings([
        // Not applying the max_length setting any longer. Without an explicit
        // max_length setting Drupal will use a varchar(255) field, at least on
        // my MySQL backend. BC docs currently say the length of the 'name'
        // field is 1..255, but let's just not apply any explicit limit any
        // longer on the Drupal end.
        'text_processing' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'hidden',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code for the Brightcove Video.'))
      ->setDisplayOptions('form', [
        'type' => 'language_select',
        'weight' => ++$weight,
      ])
      ->setDisplayConfigurable('form', TRUE);

    /*
     * Additional Brightcove fields, based on
     * @see https://videocloud.brightcove.com/admin/fields
     * superseded by
     * @see http://docs.brightcove.com/en/video-cloud/cms-api/references/cms-api/versions/v1/index.html#api-videoGroup-Get_Videos
     * superseded by
     * @see http://docs.brightcove.com/en/video-cloud/cms-api/references/cms-api/versions/v1/index.html#api-videoGroup-Create_Video
     *
     * Brightcove ID - string (Not editable. Unique Video ID assigned by
     *   Brightcove)
     * Economics - list (ECONOMICS_TYPE_FREE, ECONOMICS_TYPE_AD_SUPPORTED)
     * Force Ads - boolean
     * Geo-filtering Country List - list (ISO-3166 country code list)
     * Geo-filtering On - boolean
     * Geo-filtering Options - list (Include countries, Exclude Countries)
     * Logo Overlay Alignment - list (Top Left, Top Right, Bottom Right,
     *   Bottom Left)
     * Logo Overlay Image - image file (Transparent PNG or GIF)
     * Logo Overlay Tooltip - text(128)
     * Logo Overlay URL - URL(128)
     * Long Description - string(0..5000)
     * Bumper Video - video file (FLV or H264 video file to playback before the
     *   Video content)
     * Reference ID - string(..150) (Value specified must be unique)
     * Related Link Text - text(40)
     * Scheduling End Date - date (Day/Time for the video to be hidden in the
     *   player)
     * Scheduling Start Date - date (Day/Time for the video be displayed in the
     *   player)
     * Short Description - string(0..250)
     * Tags - text (Separate tags with a comma; no tag > 128 characters. Max
     *   1200 tags per video)
     * Thumbnail - image file (Suggested size: 120 x 90 pixels, JPG)
     * Video Duration - number (Not editable. Stores the length of the video
     *   file.)
     * Video Files - video file (One or more FLV or H264 video files)
     * Video Name - string(1..255)
     * Video Still - image file (Suggested size: 480 x 360 pixels, JPG)
     * Viral Distribution - boolean (Enables the get code and blogging menu
     *   options for the video)
     */
    $fields['video_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Video ID'))
      ->setDescription(t('Unique Video ID assigned by Brightcove.'))
      ->setReadOnly(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'inline',
        'weight' => ++$weight,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['duration'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Video Duration'))
      ->setReadOnly(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'number_time',
        'label' => 'inline',
        'weight' => ++$weight,
        'settings' => [
          'storage' => TimeFieldFormatter::STORAGE_MILLISECONDS,
          'display' => TimeFieldFormatter::DISPLAY_NUMBERSMS,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Short description'))
      ->setDescription(t('Max 250 characters.'))
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'basic_string',
        'label' => 'above',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->addPropertyConstraints('value', [
        'Length' => [
          'max' => 250,
        ],
      ]);

    $fields['tags'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Tags'))
      ->setDescription(t('Max 1200 tags per video'))
      // We can't really say 1200 here as it'd yield 1200 textfields on the UI.
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSettings([
        'target_type' => 'taxonomy_term',
        'handler_settings' => [
          'target_bundles' => [self::TAGS_VID => self::TAGS_VID],
          'auto_create' => TRUE,
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => ++$weight,
        'settings' => [
          'autocomplete_type' => 'tags',
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
        'label' => 'above',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['related_link'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Related Link'))
      ->setSettings([
        'max_length' => 150,
        'link_type' => LinkItemInterface::LINK_GENERIC,
        'title' => DRUPAL_OPTIONAL,
      ])
      ->setDisplayOptions('form', [
        'type' => 'link_default',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'link',
        'label' => 'inline',
        'weight' => $weight,
        'settings' => [
          'trim_length' => 150,
          'target' => '_blank',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['reference_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Reference ID'))
      ->addConstraint('UniqueField')
      ->setDescription(t('Value specified must be unique'))
      ->setSettings([
        'max_length' => 150,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'inline',
        'weight' => $weight,
      ])
      ->setDefaultValueCallback(static::class . '::getDefaultReferenceId')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['long_description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Long description'))
      ->setDescription(t('Max 5000 characters'))
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'basic_string',
        'label' => 'above',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->addPropertyConstraints('value', [
        'Length' => [
          'max' => 5000,
        ],
      ]);

    // Advertising field, but Brightcove calls it 'economics' in the API.
    $fields['economics'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Advertising'))
      ->setRequired(TRUE)
      ->setDefaultValue(self::ECONOMICS_TYPE_FREE)
      ->setSetting('allowed_values', [
        self::ECONOMICS_TYPE_FREE => 'Free',
        self::ECONOMICS_TYPE_AD_SUPPORTED => 'Ad Supported',
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'inline',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // @todo Folder
    $fields['video_file'] = BaseFieldDefinition::create('file')
      ->setLabel(t('Video source'))
      ->setSettings([
        'file_extensions' => '3gp 3g2 aac ac3 asf avchd avi avs bdav dv dxa ea eac3 f4v flac flv h261 h263 h264 m2p m2ts m4a m4v mjpeg mka mks mkv mov mp3 mp4 mpeg mpegts mpg mt2s mts ogg ps qt rtsp thd ts vc1 wav webm wma wmv',
        'file_directory' => '[random:hash:md5]',
      ])
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'file_url_plain',
        'label' => 'inline',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Provide an external URL.
    $fields['video_url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Video source URL'))
      ->setDisplayOptions('form', [
        'type' => 'uri',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'uri_link',
        'label' => 'inline',
        'weight' => $weight,
        'settings' => [
          'trim_length' => 150,
          'target' => '_blank',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['profile'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Encoding profile'))
      ->setRequired(TRUE)
      ->setSetting('allowed_values_function', [
        self::class,
        'profileAllowedValues',
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => ++$weight,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['poster'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Video Still'))
      ->setSettings([
        'file_extensions' => 'jpg jpeg png',
        'file_directory' => self::VIDEOS_IMAGES_POSTERS_DIR,
        'alt_field' => FALSE,
        'alt_field_required' => FALSE,
      ])
      ->setDisplayOptions('form', [
        'type' => 'image_image',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'image',
        'label' => 'above',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['thumbnail'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Thumbnail'))
      ->setSettings([
        'file_extensions' => 'jpg jpeg png',
        'file_directory' => self::VIDEOS_IMAGES_THUMBNAILS_DIR,
        'alt_field' => FALSE,
        'alt_field_required' => FALSE,
      ])
      ->setDisplayOptions('form', [
        'type' => 'image_image',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'image',
        'label' => 'above',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['custom_field_values'] = BaseFieldDefinition::create('map');

    $fields['schedule_starts_at'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Scheduled Start Date'))
      ->setDescription(t('If not specified, the video will be Available Immediately.'))
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'datetime_default',
        'label' => 'inline',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['schedule_ends_at'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Scheduled End Date'))
      ->setDescription(t('If not specified, the video will have No End Date.'))
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'datetime_default',
        'label' => 'inline',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['text_tracks'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Text Tracks'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('Referenced text tracks which belong to the video.'))
      ->setSetting('target_type', 'brightcove_text_track')
      ->setDisplayOptions('form', [
        'type' => 'brightcove_inline_entity_form_complex',
        'settings' => [
          'allow_new' => TRUE,
          'allow_existing' => FALSE,
        ],
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The username of the Brightcove Video author.'))
      ->setTranslatable(TRUE)
      ->setDefaultValueCallback('Drupal\brightcove\Entity\BrightcoveVideo::getCurrentUserId')
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => ++$weight,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'author',
        'label' => 'inline',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the Brightcove Video was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => ++$weight,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the Brightcove Video was last edited.'))
      ->setTranslatable(TRUE);

    // FIXME: Couldn't find this on the Brightcove UI: https://studio.brightcove.com/products/videocloud/media/videos/4585854207001
    $fields['force_ads'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Force Ads'))
      ->setDefaultValue(FALSE);

    // FIXME: Couldn't find this on the Brightcove UI: https://studio.brightcove.com/products/videocloud/media/videos/4585854207001
    $fields['geo_countries'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Geo-filtering Country List'))
      ->setDescription(t('ISO-3166 country code list.'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSettings([
        'max_length' => 5,
        'text_processing' => 0,
      ])
      // FIXME: Use a dropdown to select the country code/country name instead.
      ->setDisplayOptions('form', [
        // Usable default type: string_textfield.
        'type' => 'hidden',
        'region' => 'hidden',
        'weight' => ++$weight,
      ])
      // FIXME: Display the country name instead.
      ->setDisplayOptions('view', [
        // Usable default type: string.
        'type' => 'hidden',
        'region' => 'hidden',
        'label' => 'above',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // FIXME: Couldn't find this on the Brightcove UI: https://studio.brightcove.com/products/videocloud/media/videos/4585854207001
    $fields['geo_restricted'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Geo-filtering On'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        // Usable default type: boolean_checkbox.
        'type' => 'hidden',
        'region' => 'hidden',
        'weight' => ++$weight,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayOptions('view', [
        // Usable default type: string.
        'type' => 'hidden',
        'region' => 'hidden',
        'label' => 'inline',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // FIXME: Couldn't find this on the Brightcove UI: https://studio.brightcove.com/products/videocloud/media/videos/4585854207001
    $fields['geo_exclude_countries'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Geo-filtering Options: Exclude countries'))
      ->setDescription(t('If enabled, country list is treated as a list of countries excluded from viewing.'))
      ->setDisplayOptions('form', [
        // Usable default type: boolean_checkbox.
        'type' => 'hidden',
        'region' => 'hidden',
        'weight' => ++$weight,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayOptions('view', [
        // Usable default type: string.
        'type' => 'hidden',
        'region' => 'hidden',
        'label' => 'inline',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // FIXME: Couldn't find this on the Brightcove UI: https://studio.brightcove.com/products/videocloud/media/videos/4585854207001
    $fields['logo_alignment'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Logo Overlay Alignment'))
      ->setCardinality(4)
      ->setSetting('allowed_values', [
        'top_left' => 'Top Left',
        'top_right' => 'Top Right',
        'bottom_left' => 'Bottom Left',
        'bottom_right' => 'Bottom Right',
      ])
      ->setDisplayOptions('form', [
        // Usable default type: options_select.
        'type' => 'hidden',
        'region' => 'hidden',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        // Usable default type: string.
        'type' => 'hidden',
        'region' => 'hidden',
        'label' => 'above',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // FIXME: Couldn't find this on the Brightcove UI: https://studio.brightcove.com/products/videocloud/media/videos/4585854207001
    $fields['logo_image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Logo Overlay Image'))
      ->setSettings([
        'file_extensions' => 'png gif',
        'file_directory' => '[random:hash:md5]',
        'alt_field' => FALSE,
        'alt_field_required' => FALSE,
      ])
      ->setDisplayOptions('form', [
        // Usable default type: image_image.
        'type' => 'hidden',
        'region' => 'hidden',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        // Usable default type: image.
        'type' => 'hidden',
        'region' => 'hidden',
        'label' => 'above',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // FIXME: Couldn't find this on the Brightcove UI: https://studio.brightcove.com/products/videocloud/media/videos/4585854207001
    $fields['logo_tooltip'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Logo Overlay Tooltip'))
      ->setSettings([
        'max_length' => 128,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('form', [
        // Usable default type: string_textfield.
        'type' => 'hidden',
        'region' => 'hidden',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        // Usable default type: string.
        'type' => 'hidden',
        'region' => 'hidden',
        'label' => 'inline',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['logo_url'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Logo Overlay URL'))
      ->setSettings([
        'max_length' => 128,
        'link_type' => LinkItemInterface::LINK_GENERIC,
        'title' => DRUPAL_DISABLED,
      ])
      ->setDisplayOptions('form', [
        // Usable default type: link_default.
        'type' => 'hidden',
        'region' => 'hidden',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        // Usable default type: link.
        'type' => 'hidden',
        'region' => 'hidden',
        'label' => 'inline',
        'weight' => $weight,
        'settings' => [
          'trim_length' => 128,
          'target' => '_blank',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // FIXME: Couldn't find this on the Brightcove UI: https://studio.brightcove.com/products/videocloud/media/videos/4585854207001
    $fields['bumper_video'] = BaseFieldDefinition::create('file')
      ->setLabel(t('Bumper Video'))
      ->setDescription(t('FLV or H264 video file to playback before the Video content'))
      ->setSettings([
        'file_extensions' => 'flv',
        'file_directory' => '[random:hash:md5]',
      ])
      ->setDisplayOptions('form', [
        // Usable default type: file_generic.
        'type' => 'hidden',
        'region' => 'hidden',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        // Usable default type: file_url_plain.
        'type' => 'hidden',
        'region' => 'hidden',
        'label' => 'inline',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // FIXME: Couldn't find this on the Brightcove UI: https://studio.brightcove.com/products/videocloud/media/videos/4585854207001
    $fields['viral'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Viral Distribution'))
      ->setDescription(t('Enables the get code and blogging menu options for the video'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        // Usable default type: boolean_checkbox.
        'type' => 'hidden',
        'region' => 'hidden',
        'weight' => ++$weight,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayOptions('view', [
        // Usable default type: string.
        'type' => 'hidden',
        'region' => 'hidden',
        'label' => 'inline',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['marked_for_ingestion'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Marked for ingestion'))
      ->setDescription(t('Indicates whether the video is being ingested or not.'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    // FIXME: Cue points?
    return $fields;
  }

  /**
   * Gets the allowed values for video profile.
   *
   * @param string $api_client
   *   The API Client ID.
   *
   * @return array
   *   The list of profiles.
   */
  public static function getProfileAllowedValues(string $api_client): array {
    $profiles = [];

    if (!empty($api_client)) {
      $cid = 'brightcove:video:profiles:' . $api_client;

      // If we have a hit in the cache, return the results.
      if ($cache = \Drupal::cache()->get($cid)) {
        $profiles = $cache->data;
      }
      // Otherwise, download the profiles from brightcove and cache the results.
      else {
        /** @var \Drupal\brightcove\Entity\BrightcoveAPIClient $api_client_entity */
        $api_client_entity = BrightcoveAPIClient::load($api_client);

        try {
          if ($api_client_entity !== NULL) {
            $client = $api_client_entity->getClient();
            $json = $client->request('GET', 1, 'ingestion', $api_client_entity->getAccountId(), '/profiles', NULL);

            foreach ($json as $profile) {
              $profiles[$profile['id']] = $profile['name'];
            }

            // Order profiles by value.
            asort($profiles);

            // Save the results to cache.
            \Drupal::cache()->set($cid, $profiles);
          }
          else {
            $profiles[] = t('Error: unable to fetch the list');
          }
        }
        catch (APIException $exception) {
          $profiles[] = t('Error: unable to fetch the list');
          \Drupal::getContainer()->get('brightcove.logger')->logException($exception, 'Failed to fetch the Profile list.');
        }
      }
    }

    return $profiles;
  }

  /**
   * Implements callback_allowed_values_function().
   *
   * @param \Drupal\Core\Field\FieldStorageDefinitionInterface $definition
   *   The field storage definition.
   * @param \Drupal\Core\Entity\FieldableEntityInterface|null $entity
   *   (optional) The entity context if known, or NULL if the allowed values
   *   are being collected without the context of a specific entity.
   * @param bool &$cacheable
   *   (optional) If an $entity is provided, the $cacheable parameter should be
   *   modified by reference and set to FALSE if the set of allowed values
   *   returned was specifically adjusted for that entity and cannot not be
   *   reused for other entities. Defaults to TRUE.
   *
   * @return array
   *   The array of allowed values. Keys of the array are the raw stored values
   *   (number or text), values of the array are the display labels. If $entity
   *   is NULL, you should return the list of all the possible allowed values
   *   in any context so that other code (e.g. Views filters) can support the
   *   allowed values for all possible entities and bundles.
   */
  public static function profileAllowedValues(FieldStorageDefinitionInterface $definition, ?FieldableEntityInterface $entity = NULL, bool &$cacheable = TRUE): array {
    $profiles = [];

    if ($entity instanceof BrightcoveVideo) {
      // Collect profiles for all of the api clients if the ID is not set.
      if (empty($entity->id())) {
        $api_clients = \Drupal::entityQuery('brightcove_api_client')
          ->execute();

        foreach ($api_clients as $api_client_id) {
          $profiles[$api_client_id] = self::getProfileAllowedValues($api_client_id);
        }
      }
      // Otherwise, just return the results for the given api client.
      else {
        $profiles = self::getProfileAllowedValues($entity->getApiClient());
      }
    }

    return $profiles;
  }

  /**
   * Create or update an existing video from a Brightcove Video object.
   *
   * @param \Brightcove\Item\Video\Video $video
   *   Brightcove Video object.
   * @param \Drupal\brightcove\Entity\Storage\VideoStorageInterface $video_storage
   *   Video entity storage.
   * @param string|null $api_client_id
   *   The ID of the BrightcoveAPIClient entity.
   *
   * @return \Drupal\brightcove\Entity\BrightcoveVideo|null
   *   The saved BrightcoveVideo entity.
   *
   * @throws \Exception
   *   If BrightcoveAPIClient ID is missing when a new entity is being created.
   */
  public static function createOrUpdate(Video $video, VideoStorageInterface $video_storage, ?string $api_client_id = NULL): ?BrightcoveVideoInterface {
    $ingestion = \Drupal::getContainer()->get('brightcove.ingestion');

    // Try to get an existing video.
    $existing_video = $video_storage->getQuery()
      ->condition('video_id', $video->getId())
      ->accessCheck()
      ->execute();

    $needs_save = FALSE;

    // Update existing video.
    if (!empty($existing_video)) {
      // Load Brightcove Video.
      /** @var BrightcoveVideo $video_entity */
      $video_entity = self::load(reset($existing_video));

      // Update video if it is changed on Brightcove.
      if ($video_entity->getChangedTime() < strtotime($video->getUpdatedAt())) {
        $needs_save = TRUE;
      }
    }
    // Create video if it does not exist.
    else {
      // Make sure we got an api client id when a new video is being created.
      if (is_null($api_client_id)) {
        throw new \Exception('To create a new BrightcoveVideo entity, the api_client_id must be given.');
      }

      // Create new Brightcove video entity.
      $values = [
        'video_id' => $video->getId(),
        'api_client' => [
          'target_id' => $api_client_id,
        ],
        'created' => strtotime($video->getCreatedAt()),
      ];
      $video_entity = self::create($values);
      $needs_save = TRUE;
    }

    // Save entity only if it is being created or updated.
    if ($needs_save) {
      // Save or update changed time.
      $video_entity->setChangedTime(strtotime($video->getUpdatedAt()));

      // Save or update Description field if needed.
      if ($video_entity->getDescription() !== ($description = $video->getDescription())) {
        $video_entity->setDescription($description ?? '');
      }

      // Save or update duration field if needed.
      if ($video_entity->getDuration() !== ($duration = $video->getDuration())) {
        $video_entity->setDuration($duration);
      }

      // Save or update economics field if needed.
      if ($video_entity->getEconomics() !== ($economics = $video->getEconomics())) {
        $video_entity->setEconomics($economics);
      }

      // Save or update tags field if needed.
      BrightcoveUtil::saveOrUpdateTags($video_entity, $api_client_id, $video->getTags());

      // Get images.
      $images = $video->getImages();

      // Save or update thumbnail image if needed.
      if (!$ingestion->isFieldMarkedForIngestion($video_entity, self::IMAGE_TYPE_THUMBNAIL)) {
        if (!empty($images[self::IMAGE_TYPE_THUMBNAIL]) && !empty($images[self::IMAGE_TYPE_THUMBNAIL]->getSrc())) {
          $video_entity->saveImage(self::IMAGE_TYPE_THUMBNAIL, $images[self::IMAGE_TYPE_THUMBNAIL]);
        }
        // Otherwise, leave empty or remove thumbnail from BrightcoveVideo
        // entity.
        else {
          // Delete file.
          $video_entity->setThumbnail(NULL);
        }
      }

      // Save or update poster image if needed.
      if (!$ingestion->isFieldMarkedForIngestion($video_entity, self::IMAGE_TYPE_POSTER)) {
        if (!empty($images[self::IMAGE_TYPE_POSTER]) && !empty($images[self::IMAGE_TYPE_POSTER]->getSrc())) {
          $video_entity->saveImage(self::IMAGE_TYPE_POSTER, $images[self::IMAGE_TYPE_POSTER]);
        }
        // Otherwise, leave empty or remove poster from BrightcoveVideo entity.
        else {
          // Delete file.
          $video_entity->setPoster(NULL);
        }
      }

      // Save or update link url field if needed.
      $link = $video->getLink();
      $related_link_field = $video_entity->getRelatedLink() ?: NULL;
      $related_link = [];
      if (empty($related_link_field) && !empty($link)) {
        $related_link['uri'] = $link->getUrl();
        $related_link['title'] = $link->getText();
      }
      elseif (!empty($related_link_field) && !empty($link)) {
        if ($related_link_field['uri'] !== ($url = $link->getUrl())) {
          $related_link['uri'] = $url;
        }

        if ($related_link_field['title'] !== ($title = $link->getText())) {
          $related_link['title'] = $title;
        }
      }
      else {
        $video_entity->setRelatedLink(NULL);
      }
      if (!empty($related_link)) {
        $video_entity->setRelatedLink($related_link);
      }

      // Save or update long description if needed.
      if ($video_entity->getLongDescription() !== ($long_description = $video->getLongDescription())) {
        $video_entity->setLongDescription($long_description ?? '');
      }

      // Save or update Name field if needed.
      if ($video_entity->getName() !== ($name = $video->getName())) {
        $video_entity->setName($name);
      }

      // Save or update reference ID field if needed.
      if ($video_entity->getReferenceId() !== ($reference_id = $video->getReferenceId())) {
        $video_entity->setReferenceId($reference_id);
      }

      // Save or update custom field values.
      if ($video_entity->getCustomFieldValues() !== $custom_fields = $video->getCustomFields()) {
        $video_entity->setCustomFieldValues($custom_fields);
      }

      // Save or update schedule dates if needed.
      $schedule = $video->getSchedule();
      if (!is_null($schedule)) {
        if ($video_entity->getScheduleStartsAt() !== ($starts_at = $schedule->getStartsAt())) {
          $video_entity->setScheduleStartsAt(!empty($starts_at) ? BrightcoveUtil::convertDate($starts_at) : NULL);
        }
        if ($video_entity->getScheduleEndsAt() !== ($ends_at = $schedule->getEndsAt())) {
          $video_entity->setScheduleEndsAt(!empty($ends_at) ? BrightcoveUtil::convertDate($ends_at) : NULL);
        }
      }
      else {
        $video_entity->setScheduleStartsAt(NULL);
        $video_entity->setScheduleEndsAt(NULL);
      }

      // Save or update state.
      // We are settings the video as published only if the state is ACTIVE,
      // otherwise it is set as unpublished.
      $state = $video->getState() === self::STATE_ACTIVE;
      if ($video_entity->isPublished() !== $state) {
        $video_entity->setPublished($state);
      }

      // Save video entity.
      $video_entity->save();
    }

    return $video_entity;
  }

}
