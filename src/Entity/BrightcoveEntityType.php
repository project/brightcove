<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Entity;

use Drupal\Core\Entity\EntityType;

/**
 * Provides an implementation of a Brightcove entity type and its metadata.
 */
final class BrightcoveEntityType extends EntityType {

  /**
   * {@inheritdoc}
   */
  public function __construct($definition) {
    parent::__construct($definition);

    $this->handlers += [
      'view_builder' => 'Drupal\Core\Entity\EntityViewBuilder',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigDependencyKey(): string {
    return 'brightcove';
  }

}
