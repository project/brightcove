<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Entity;

use Drupal\brightcove\BrightcoveCMSEntityInterface;
use Drupal\brightcove\EntityChangedFieldsTrait;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\user\UserInterface;

/**
 * Common base class for CMS entities like Video and Playlist.
 *
 * phpcs:disable Drupal.Commenting.Deprecated.DeprecatedWrongSeeUrlFormat
 */
abstract class BrightcoveCmsEntity extends ContentEntityBase implements BrightcoveCMSEntityInterface {

  use EntityChangedTrait;
  use EntityChangedFieldsTrait;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName(string $name): BrightcoveCMSEntityInterface {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiClient(): ?string {
    return $this->get('api_client')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setApiClient(string $api_client): BrightcoveCMSEntityInterface {
    $this->set('api_client', $api_client);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription(string $description): BrightcoveCMSEntityInterface {
    $this->set('description', $description);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner(): UserInterface {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account): BrightcoveCMSEntityInterface {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId(): ?int {
    $uid = $this->get('uid');
    return $uid !== NULL ? (int) $uid->target_id : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid): BrightcoveCMSEntityInterface {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): ?int {
    $created = $this->get('created');
    return !empty($created->value) ? (int) $created->value : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp): BrightcoveCMSEntityInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    $this->checkUpdatedFields($storage);
    parent::preSave($storage);
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId(): array {
    return [
      \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values): void {
    parent::preCreate($storage, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * Load multiple CMS Entity for the given api client.
   *
   * @param string $api_client
   *   The ID of the BrightcoveAPIClient entity.
   * @param bool $status
   *   The status of the entity, if TRUE then published entities will be
   *   returned, otherwise the unpublished entities.
   *
   * @return \Drupal\brightcove\Entity\BrightcoveCmsEntity[]
   *   An array of BrightcoveCMSEntity objects.
   *
   * @throws \Drupal\Core\Entity\Exception\AmbiguousEntityClassException
   * @throws \Drupal\Core\Entity\Exception\NoCorrespondingEntityClassException
   *
   * @deprecated in brightcove:3.0.0 and is removed from brightcove:4.0.0.
   *   Please use the entity storage to load custom field entity via API Client.
   * @see \Drupal\Core\Entity\EntityStorageInterface::loadByProperties()
   */
  public static function loadMultipleByApiClient($api_client, $status = TRUE) {
    $repository = \Drupal::getContainer()->get('entity_type.repository');
    $entity_ids = \Drupal::entityQuery($repository->getEntityTypeFromClass(get_called_class()))
      ->condition('api_client', $api_client)
      ->condition('status', $status ? 1 : 0)
      ->accessCheck()
      ->execute();
    return self::loadMultiple($entity_ids);
  }

}
