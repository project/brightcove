<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Interface for a Brightcove type of entity.
 */
interface BrightcoveEntityInterface extends EntityInterface, FieldableEntityInterface {
}
