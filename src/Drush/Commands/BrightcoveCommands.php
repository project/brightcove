<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Drush\Commands;

use Drupal\brightcove\BrightcoveUtil;
use Drupal\brightcove\Exception\BrightcoveUtilException;
use Drupal\brightcove\Services\LoggerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drush\Attributes as CLI;
use Drush\Boot\DrupalBootLevels;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drush commands for Brightcove module.
 */
class BrightcoveCommands extends DrushCommands {

  /**
   * Initializes a Brightcove commands handler.
   *
   * @param \Drupal\brightcove\Services\LoggerInterface $moduleLogger
   *   Module logger.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue service.
   */
  public function __construct(
    protected readonly LoggerInterface $moduleLogger,
    protected readonly QueueFactory $queueFactory,
  ) {
    parent::__construct();
  }

  /**
   * Creates a new instance.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container.
   *
   * @return self
   *   A new instance.
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('brightcove.logger'),
      $container->get('queue'),
    );
  }

  #[CLI\Command(name: 'brightcove:sync-all', aliases: [
    'bcsa',
  ])]
  #[CLI\Bootstrap(level: DrupalBootLevels::FULL)]

  /**
   * Initiates a Brightcove-to-Drupal sync by adding API clients to the queue.
   */
  public function syncAll(): void {
    $this->output()->writeln('Initiating Brightcove-to-Drupal sync...');
    try {
      BrightcoveUtil::runStatusQueues('sync', $this->queueFactory);
      drush_backend_batch_process();
      $this->logger->notice('Sync complete.');
    }
    catch (BrightcoveUtilException $e) {
      $this->moduleLogger->logException($e, 'Failed to sync with Brightcove via drush.');
    }
  }

}
