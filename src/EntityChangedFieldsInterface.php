<?php

declare(strict_types = 1);

namespace Drupal\brightcove;

use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines required methods for entities that tracks changed fields.
 */
interface EntityChangedFieldsInterface {

  /**
   * Returns whether the field is changed or not.
   *
   * @param string $name
   *   The name of the field on the entity.
   *
   * @return bool
   *   The changed status of the field, TRUE if changed, FALSE otherwise.
   */
  public function isFieldChanged(string $name): bool;

  /**
   * Checked if the Entity has a changed field or not.
   *
   * @return bool
   *   TRUE if the entity has changed fields, FALSE otherwise.
   */
  public function hasChangedField(): bool;

  /**
   * Check for updated fields.
   *
   * Ideally it should be called from the entity's preSave() method before the
   * parent's preSave() call.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   Entity storage.
   */
  public function checkUpdatedFields(EntityStorageInterface $storage): void;

  /**
   * Get getter method from the name of the field.
   *
   * @param string $name
   *   The name of the field.
   * @param array $methods
   *   The available methods.
   *
   * @return string|null
   *   The name of the getter function.
   */
  public function getGetterName(string $name, array $methods): ?string;

  /**
   * Clears changed fields.
   */
  public function clearChangedFields(): void;

}
