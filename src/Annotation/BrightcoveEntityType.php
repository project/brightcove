<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Annotation;

use Drupal\Core\Entity\Annotation\EntityType;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a Brightcove entity type annotation object.
 *
 * @Annotation
 */
final class BrightcoveEntityType extends EntityType {

  /**
   * {@inheritdoc}
   *
   * phpcs:disable Drupal.NamingConventions.ValidVariableName.LowerCamelName
   */
  public $entity_type_class = '\Drupal\brightcove\Entity\BrightcoveEntityType';

  /**
   * {@inheritdoc}
   */
  public $group = 'brightcove';

  /**
   * {@inheritdoc}
   */
  public function __construct($values) {
    parent::__construct($values);

    $this->definition['group_label'] = new TranslatableMarkup('Brightcove', [], [
      'context' => 'Entity type group',
    ]);
  }

}
