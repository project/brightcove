<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Routing\ParameterConverter;

use Drupal\brightcove\Entity\BrightcoveSubscription;
use Drupal\brightcove\Entity\BrightcoveSubscriptionInterface;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Symfony\Component\Routing\Route;

/**
 * Subscription entity parameter upcasting.
 */
final class Subscription implements ParamConverterInterface {

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults): BrightcoveSubscriptionInterface {
    return BrightcoveSubscription::load((int) $value);
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route): bool {
    return !empty($definition['type']) && $definition['type'] == 'brightcove_subscription';
  }

}
