<?php

declare(strict_types = 1);

namespace Drupal\brightcove;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Brightcove Player.
 *
 * @ingroup brightcove
 */
interface BrightcovePlayerInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Returns the Brightcove Player ID.
   *
   * @return string|null
   *   The Brightcove Player ID (not the entity's).
   */
  public function getPlayerId(): ?string;

  /**
   * Sets The Brightcove Player ID.
   *
   * @param string $player_id
   *   The Brightcove Player ID (not the entity's).
   *
   * @return \Drupal\brightcove\BrightcovePlayerInterface
   *   The called Brightcove Player.
   */
  public function setPlayerId(string $player_id): BrightcovePlayerInterface;

  /**
   * Returns whether the player is adjusted for the playlist or not.
   *
   * @return bool|null
   *   TRUE or FALSE whether the player is adjusted or not, or NULL if not set.
   */
  public function isAdjusted(): ?bool;

  /**
   * Sets the Player as adjusted.
   *
   * @param bool|null $adjusted
   *   TRUE or FALSE whether the player is adjusted or not, or NULL to unset
   *   the value.
   *
   * @return \Drupal\brightcove\BrightcovePlayerInterface
   *   The called Brightcove Player.
   */
  public function setAdjusted(?bool $adjusted): BrightcovePlayerInterface;

  /**
   * Returns the height of the player.
   *
   * @return float|null
   *   The height of the player.
   */
  public function getHeight(): ?float;

  /**
   * Sets the height of the player.
   *
   * @param float|null $height
   *   The height of the player.
   *
   * @return \Drupal\brightcove\BrightcovePlayerInterface
   *   The called Brightcove Player.
   */
  public function setHeight(?float $height): BrightcovePlayerInterface;

  /**
   * Returns the width of the player.
   *
   * @return float|null
   *   The width of the player.
   */
  public function getWidth(): ?float;

  /**
   * Sets the width of the player.
   *
   * @param float|null $width
   *   The width of the player.
   *
   * @return \Drupal\brightcove\BrightcovePlayerInterface
   *   The called Brightcove Player.
   */
  public function setWidth(?float $width): BrightcovePlayerInterface;

  /**
   * Returns the units for the height and width.
   *
   * @return string|null
   *   The units for the height and width.
   */
  public function getUnits(): ?string;

  /**
   * Sets the units for the height and width.
   *
   * @param string|null $units
   *   The units for the height and width.
   *
   * @return \Drupal\brightcove\BrightcovePlayerInterface
   *   The called Brightcove Player.
   */
  public function setUnits(?string $units): BrightcovePlayerInterface;

  /**
   * Returns whether if the player is responsive or not.
   *
   * @return bool|null
   *   TRUE if the player is responsive, FALSE if fixed.
   */
  public function isResponsive(): ?bool;

  /**
   * Sets the responsive indicator.
   *
   * @param bool|null $is_responsive
   *   TRUE if the player is responsive, FALSE if fixed.
   *
   * @return \Drupal\brightcove\BrightcovePlayerInterface
   *   The called Brightcove Player.
   */
  public function setResponsive(?bool $is_responsive): BrightcovePlayerInterface;

  /**
   * Returns whether the player is a playlist player or a single video player.
   *
   * @return bool|null
   *   TRUE if playlist player, FALSE if single video player.
   */
  public function isPlaylist(): ?bool;

  /**
   * Sets if the player is a playlist player or single video player.
   *
   * @param bool $is_playlist
   *   TRUE if playlist player, FALSE if single video player.
   *
   * @return \Drupal\brightcove\BrightcovePlayerInterface
   *   The called Brightcove Player.
   */
  public function setPlaylist(bool $is_playlist): BrightcovePlayerInterface;

  /**
   * Gets the version of the player.
   *
   * @return string|null
   *   The version of the player.
   */
  public function getVersion(): ?string;

}
