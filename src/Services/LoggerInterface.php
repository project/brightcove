<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Services;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Url;
use Psr\Log\LoggerInterface as PsrLoggerInterface;

/**
 * Defines required methods for a module specific logger.
 */
interface LoggerInterface extends PsrLoggerInterface {

  /**
   * Logs an exception.
   *
   * @param \Throwable $exception
   *   The exception that is going to be logged.
   * @param string $subject
   *   The subject of the exception log message.
   * @param mixed[] $variables
   *   Array of variables to replace in the subject on display or an empty array
   *   if the subject is already translated or not possible to translate.
   * @param int $severity
   *   The severity of the message, as per RFC 3164, defaults to
   *   RfcLogLevel::ERROR.
   * @param \Drupal\Core\Url|null $url
   *   A URL to associate with the message.
   */
  public function logException(\Throwable $exception, string $subject, array $variables = [], int $severity = RfcLogLevel::ERROR, ?Url $url = NULL): void;

}
