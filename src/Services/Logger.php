<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Services;

use Drupal\brightcove\Module;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;

/**
 * Module specific logger.
 */
final class Logger implements LoggerInterface {

  use RfcLoggerTrait;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * Initializes a module logger.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   Logger channel factory.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_channel_factory) {
    $this->logger = $logger_channel_factory->get(Module::NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, string|\Stringable $message, array $context = []): void {
    $this->logger->log($level, $message, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function logException(\Throwable $exception, string $subject, array $variables = [], int $severity = RfcLogLevel::ERROR, ?Url $url = NULL): void {
    $this->log($severity, '@subject<br><br>%type: @message in %function (line %line of %file).<pre>@backtrace_string</pre>', Error::decodeException($exception) + [
      '@subject' => strtr($subject, $variables),
    ] + ($url !== NULL ? ['link' => $url->toString()] : []));
  }

}
