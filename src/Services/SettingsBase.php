<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Services;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Base functionality for a settings handler.
 */
abstract class SettingsBase {

  /**
   * Initializes settings base.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   */
  public function __construct(
    protected readonly ConfigFactoryInterface $configFactory,
  ) {
  }

  /**
   * Helper function to save settings.
   *
   * @param string $key
   *   The key of the setting.
   * @param mixed $value
   *   The value of the setting.
   */
  protected function set(string $key, $value): void {
    $this->configFactory->getEditable(SettingsInterface::STORAGE)
      ->set($key, $value)
      ->save();
  }

}
