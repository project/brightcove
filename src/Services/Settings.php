<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Services;

/**
 * Handles module specific settings.
 */
final class Settings extends SettingsBase implements SettingsInterface {

  /**
   * {@inheritdoc}
   */
  public function getDefaultApiClientId(): string {
    return $this->configFactory->get(self::STORAGE)->get(self::DEFAULT_API_CLIENT);
  }

  /**
   * {@inheritdoc}
   */
  public function getMarkedFieldExpiry(): int {
    return $this->configFactory->get(self::STORAGE)->get(self::INGESTION_MARKED_FIELD_EXPIRY);
  }

  /**
   * {@inheritdoc}
   */
  public function getNotificationCallbackExpirationTime(): int {
    return $this->configFactory->get(self::STORAGE)->get(self::NOTIFICATION_CALLBACK_EXPIRATION_TIME) ?? 86400;
  }

  /**
   * {@inheritdoc}
   */
  public function isCronDisabled(): bool {
    return $this->configFactory->get(self::STORAGE)->get(self::DISABLE_CRON);
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultApiClientId(string $api_client_id): void {
    $this->set(self::DEFAULT_API_CLIENT, $api_client_id);
  }

  /**
   * {@inheritdoc}
   */
  public function setCronDisabled(bool $is_disabled): void {
    $this->set(self::DISABLE_CRON, $is_disabled);
  }

  /**
   * {@inheritdoc}
   */
  public function setMarkedFieldExpiry(int $seconds): void {
    $this->set(self::INGESTION_MARKED_FIELD_EXPIRY, $seconds);
  }

}
