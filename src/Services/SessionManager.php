<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Services;

use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface;

/**
 * Module specific session handler.
 */
final class SessionManager implements SessionManagerInterface {

  /**
   * Attribute bag.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface
   */
  private $attributeBag;

  /**
   * Initializes a new module specific session handler.
   *
   * @param \Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface $attribute_bag
   *   Attribute bag.
   */
  public function __construct(AttributeBagInterface $attribute_bag) {
    $this->attributeBag = $attribute_bag;
  }

  /**
   * {@inheritdoc}
   */
  public function isDestinationIgnored(): bool {
    return $this->attributeBag->get(static::IGNORE_DESTINATION, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function setIgnoreDestination(bool $ignore): void {
    $this->attributeBag->set(static::IGNORE_DESTINATION, $ignore);
  }

}
