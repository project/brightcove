<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Services;

use Brightcove\API\Request\IngestImage;
use Brightcove\API\Request\IngestRequest;
use Brightcove\API\Response\IngestResponse;
use Drupal\brightcove\BrightcoveVideoInterface;

/**
 * Defines required methods for an ingestion helper service.
 */
interface IngestionInterface {

  /**
   * Last ingestion time storage for the fields.
   */
  public const LAST_INGESTION_TIME_STORAGE = '_last_ingestion';

  /**
   * Creates an ingestion request object.
   *
   * @return \Brightcove\API\Request\IngestRequest
   *   Returns a new ingestion request object.
   */
  public function createIngestRequest(): IngestRequest;

  /**
   * Helper function to get an image ingestion.
   *
   * @param int[]|null $image
   *   The image that needs to be ingested.
   *
   * @return \Brightcove\API\Request\IngestImage|null
   *   Ingest image object or NULL if the file cannot be found.
   */
  public function getImageIngestion(?array $image): ?IngestImage;

  /**
   * Get a random hash token for ingestion request callback.
   *
   * @return string|null
   *   The generated random token or NULL if an error happened.
   */
  public function getIngestionToken(BrightcoveVideoInterface $video): ?string;

  /**
   * Sends a Video ingestion request.
   *
   * Ingestion request will be sent only if there is an ingestible field changed
   * or if the entity is new and has such fields set.
   *
   * @param \Drupal\brightcove\BrightcoveVideoInterface $video
   *   The Video for which the ingestion should be created and sent.
   *
   * @return \Brightcove\API\Response\IngestResponse|null
   *   The ingestion response or NULL if there was no ingestion.
   *
   * @throws \Drupal\brightcove\Services\Exception\IngestionException
   *   In case if the ingestion was failed.
   */
  public function sendIngestVideo(BrightcoveVideoInterface $video): ?IngestResponse;

  /**
   * Gets whether the Video is marked for ingestion or not.
   *
   * @param \Drupal\brightcove\BrightcoveVideoInterface $video
   *   Video entity.
   * @param string $field
   *   Name of the field that should be checked if marked for ingestion.
   *
   * @return bool
   *   TRUE if the field is set for ingestion, FALSE otherwise.
   */
  public function isFieldMarkedForIngestion(BrightcoveVideoInterface $video, string $field): bool;

  /**
   * Sets mark for ingestion.
   *
   * @param \Drupal\brightcove\BrightcoveVideoInterface $video
   *   Video entity.
   * @param string $field
   *   Name of the field to mark for ingestion.
   * @param bool $mark_for_ingestion
   *   TRUE to mark the field for ingestion, FALSE otherwise.
   *
   * @return \Drupal\brightcove\BrightcoveVideoInterface
   *   The called Brightcove Video.
   */
  public function setFieldMarkedForIngestion(BrightcoveVideoInterface $video, string $field, bool $mark_for_ingestion): BrightcoveVideoInterface;

}
