<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Services;

/**
 * Defines required methods for a module specific session manager.
 */
interface SessionManagerInterface {

  /**
   * Ignore destination flag in session, attribute bag.
   */
  public const IGNORE_DESTINATION = 'brightcove_ignore_destination';

  /**
   * Gets whether the destination is being ignored or not.
   *
   * @return bool
   *   TRUE if the destination is ignored for redirection, FALSE otherwise.
   */
  public function isDestinationIgnored(): bool;

  /**
   * Sets whether to ignore the destination query parameter or not.
   *
   * Controls whether the destination parameter should be considered if set in
   * case of a redirect event.
   *
   * @param bool $ignore
   *   TRUE to ignore destination query parameter, FALSE otherwise.
   */
  public function setIgnoreDestination(bool $ignore): void;

}
