<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Services\Exception;

use Drupal\brightcove\Exception\BrightcoveExceptionInterface;

/**
 * Ingestion service specific exception.
 */
final class IngestionException extends \Exception implements BrightcoveExceptionInterface {}
