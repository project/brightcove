<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Services;

use Drupal\brightcove\Module;

/**
 * Defines required methods for a module settings provider.
 */
interface SettingsInterface {

  /**
   * Name of the storage.
   */
  public const STORAGE = Module::NAME . '.settings';

  public const DEFAULT_API_CLIENT = 'defaultAPIClient';
  public const DISABLE_CRON = 'disable_cron';
  public const INGESTION_MARKED_FIELD_EXPIRY = 'ingestion.marked_field_expiry';
  public const NOTIFICATION_CALLBACK_EXPIRATION_TIME = 'notification.callbackExpirationTime';

  /**
   * Gets the default API client ID.
   *
   * @return string
   *   The default API client.
   */
  public function getDefaultApiClientId(): string;

  /**
   * Gets marked field's expiry in seconds.
   *
   * @return int
   *   The expiry seconds.
   */
  public function getMarkedFieldExpiry(): int;

  /**
   * Gets the notification callback expiration time in seconds.
   *
   * @return int
   *   The expiration time in seconds.
   */
  public function getNotificationCallbackExpirationTime(): int;

  /**
   * Returns whether the module related cron is disabled or not.
   *
   * @return bool
   *   TRUE if the cron is disabled, FALSE otherwise.
   */
  public function isCronDisabled(): bool;

  /**
   * Sets the default API client.
   *
   * @param string $api_client_id
   *   The default API client.
   */
  public function setDefaultApiClientId(string $api_client_id): void;

  /**
   * Sets whether the cron should be disabled or not.
   *
   * @param bool $is_disabled
   *   TRUE to disable cron, FALSE to enable it.
   */
  public function setCronDisabled(bool $is_disabled): void;

  /**
   * Sets the marked field expiry.
   *
   * @param int $seconds
   *   Expiry seconds.
   */
  public function setMarkedFieldExpiry(int $seconds): void;

}
