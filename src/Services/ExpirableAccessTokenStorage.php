<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Services;

use Drupal\Core\KeyValueStore\DatabaseStorageExpirable;

/**
 * Expirable access token storage.
 *
 * Overrides the database expirable storage as the module requires the actual
 * _current_ time instead of the _request_ time.
 * The request time can be incorrect when it comes to check the expiry of the
 * access token of the API precisely.
 */
final class ExpirableAccessTokenStorage extends DatabaseStorageExpirable {

  /**
   * {@inheritdoc}
   */
  public function has($key): bool {
    try {
      return (bool) $this->connection->query('SELECT 1 FROM {' . $this->connection->escapeTable($this->table) . '} WHERE [collection] = :collection AND [name] = :key AND [expire] > :now', [
        ':collection' => $this->collection,
        ':key' => $key,
        ':now' => $this->time->getCurrentTime(),
      ])->fetchField();
    }
    catch (\Exception $e) {
      $this->catchException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getAll(): array {
    try {
      $values = $this->connection->query(
        'SELECT [name], [value] FROM {' . $this->connection->escapeTable($this->table) . '} WHERE [collection] = :collection AND [expire] > :now',
        [
          ':collection' => $this->collection,
          ':now' => $this->time->getCurrentTime(),
        ])->fetchAllKeyed();
      return array_map([$this->serializer, 'decode'], $values);
    }
    catch (\Exception $e) {
      $this->catchException($e);
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getMultiple(array $keys): array {
    try {
      $values = $this->connection->query(
        'SELECT [name], [value] FROM {' . $this->connection->escapeTable($this->table) . '} WHERE [expire] > :now AND [name] IN ( :keys[] ) AND [collection] = :collection',
        [
          ':now' => $this->time->getCurrentTime(),
          ':keys[]' => $keys,
          ':collection' => $this->collection,
        ])->fetchAllKeyed();
      return array_map([$this->serializer, 'decode'], $values);
    }
    catch (\Exception $e) {
      $this->catchException($e);
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function doSetWithExpire($key, $value, $expire): void {
    $this->connection->merge($this->table)
      ->keys([
        'name' => $key,
        'collection' => $this->collection,
      ])
      ->fields([
        'value' => $this->serializer->encode($value),
        'expire' => $this->time->getCurrentTime() + $expire,
      ])
      ->execute();
  }

}
