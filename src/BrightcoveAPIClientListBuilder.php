<?php

declare(strict_types = 1);

namespace Drupal\brightcove;

use Drupal\brightcove\Entity\Storage\PlayerStorageInterface;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of Brightcove API Client entities.
 */
class BrightcoveAPIClientListBuilder extends ConfigEntityListBuilder {

  /**
   * The ID of the default API Client.
   *
   * @var string
   */
  protected $defaultAPIClient;

  /**
   * Player storage.
   *
   * @var \Drupal\brightcove\Entity\Storage\PlayerStorageInterface
   */
  protected $playerStorage;

  /**
   * Initializes a new API Client list builder.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param string $default_api_client
   *   The ID of the default API client.
   * @param \Drupal\brightcove\Entity\Storage\PlayerStorageInterface $player_storage
   *   Player storage.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String translation.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, string $default_api_client, PlayerStorageInterface $player_storage, TranslationInterface $string_translation) {
    parent::__construct($entity_type, $storage);
    $this->defaultAPIClient = $default_api_client;
    $this->playerStorage = $player_storage;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type): self {
    $entity_type_manager = $container->get('entity_type.manager');

    return new static(
      $entity_type,
      $entity_type_manager->getStorage($entity_type->id()),
      $container->get('brightcove.settings')->getDefaultApiClientId(),
      $entity_type_manager->getStorage('brightcove_player'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    return [
      'label' => $this->t('Brightcove API Client'),
      'id' => $this->t('Machine name'),
      'account_id' => $this->t('Account ID'),
      'default_player' => $this->t('Default player'),
      'client_status' => $this->t('Status'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\brightcove\BrightcoveAPIClientInterface $entity */

    $row = [
      'label' => $entity->label(),
      'id' => $entity->id(),
      'account_id' => $entity->getAccountId(),
      'default_player' => $this->playerStorage->getList([$entity->id()])[$entity->getDefaultPlayer()],
    ];

    if ($this->defaultAPIClient === $entity->id()) {
      $row['id'] .= ' ' . $this->t('(default)');
    }

    // Try to authorize client to get client status.
    try {
      $entity->authorizeClient();
      $row['client_status'] = $entity->getClientStatus() ? $this->t('OK') : $this->t('Error');
    }
    catch (\Exception) {
      $row['client_status'] = $this->t('Error');
    }

    return $row + parent::buildRow($entity);
  }

}
