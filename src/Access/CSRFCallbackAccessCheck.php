<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Custom CSRF access check callback.
 */
class CSRFCallbackAccessCheck implements AccessInterface {

  /**
   * Callback token store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  private $callbackTokenStore;

  /**
   * Initializes a new CSRF callback access checker.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface $keyValueExpirableFactory
   *   Key-value expirable factory.
   */
  public function __construct(KeyValueExpirableFactoryInterface $keyValueExpirableFactory) {
    $this->callbackTokenStore = $keyValueExpirableFactory->get('brightcove_callback');
  }

  /**
   * Custom access callback.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   RouterMatch object.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Access allowed only if the token is exists and did not expired.
   */
  public function access(RouteMatchInterface $route_match): AccessResultInterface {
    $token = $route_match->getParameter('token');
    return AccessResult::allowedIf($this->callbackTokenStore->has($token));
  }

}
