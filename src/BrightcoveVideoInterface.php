<?php

declare(strict_types = 1);

namespace Drupal\brightcove;

use Brightcove\Item\Video\Image;

/**
 * Provides an interface for defining Brightcove Videos.
 */
interface BrightcoveVideoInterface extends BrightcoveVideoPlaylistCMSEntityInterface {

  /**
   * Denotes that the video is published.
   */
  const PUBLISHED = 1;

  /**
   * Denotes that the video is not published.
   */
  const NOT_PUBLISHED = 0;

  /**
   * Brightcove video thumbnail images path.
   */
  const VIDEOS_IMAGES_THUMBNAILS_DIR = 'brightcove/videos/images/thumbnails';

  /**
   * Brightcove video poster images path.
   */
  const VIDEOS_IMAGES_POSTERS_DIR = 'brightcove/videos/images/posters';

  /**
   * Brightcove thumbnail image type.
   */
  const IMAGE_TYPE_THUMBNAIL = 'thumbnail';

  /**
   * Brightcove poster image type.
   */
  const IMAGE_TYPE_POSTER = 'poster';

  /**
   * Brightcove economics type, free.
   */
  const ECONOMICS_TYPE_FREE = 'FREE';

  /**
   * Brightcove economics type, ad supported.
   */
  const ECONOMICS_TYPE_AD_SUPPORTED = 'AD_SUPPORTED';

  /**
   * Brightcove active state.
   */
  const STATE_ACTIVE = 'ACTIVE';

  /**
   * Brightcove inactive state.
   */
  const STATE_INACTIVE = 'INACTIVE';

  /**
   * Brightcove video tags vocabulary ID.
   */
  const TAGS_VID = 'brightcove_video_tags';

  /**
   * Helper function to save the image for the entity.
   *
   * @param string $type
   *   The type of the image. Possible values are:
   *     - IMAGE_TYPE_THUMBNAIL: Recommended aspect ratio of 16:9 and a minimum
   *                             width of 640px.
   *     - IMAGE_TYPE_POSTER: Recommended aspect ratio of 16:9 and a minimum
   *                          width of 160px.
   * @param \Brightcove\Item\Video\Image $image
   *   The image from Brightcove.
   *
   * @return \Drupal\brightcove\BrightcoveVideoInterface
   *   The called Brightcove Video.
   *
   * @throws \Exception
   *   If the type is not matched with the possible types.
   */
  public function saveImage(string $type, Image $image): BrightcoveVideoInterface;

  /**
   * Returns the video's duration.
   *
   * @return int|null
   *   Video duration.
   */
  public function getDuration(): ?int;

  /**
   * Sets the video duration.
   *
   * @param int $duration
   *   The duration of the video.
   *
   * @return \Drupal\brightcove\BrightcoveVideoInterface
   *   The called Brightcove Video.
   */
  public function setDuration(int $duration): BrightcoveVideoInterface;

  /**
   * Returns the video's related link.
   *
   * @return array
   *   An array list of links.
   */
  public function getRelatedLink(): array;

  /**
   * Sets the video's related link.
   *
   * @param array|null $related_link
   *   The related link.
   *
   * @return \Drupal\brightcove\BrightcoveVideoInterface
   *   The called Brightcove Video.
   */
  public function setRelatedLink(?array $related_link): BrightcoveVideoInterface;

  /**
   * Returns the long description of the video.
   *
   * @return string|null
   *   The long description of the video.
   */
  public function getLongDescription(): ?string;

  /**
   * Sets the video's long description.
   *
   * @param string $long_description
   *   The long description of the video.
   *
   * @return \Drupal\brightcove\BrightcoveVideoInterface
   *   The called Brightcove Video.
   */
  public function setLongDescription(string $long_description): BrightcoveVideoInterface;

  /**
   * Returns the economics state.
   *
   * @return string|null
   *   The economics state of the video.
   */
  public function getEconomics(): ?string;

  /**
   * Sets the video's economics state.
   *
   * @param string $is_economics
   *   The economics state of the video.
   *
   * @return \Drupal\brightcove\BrightcoveVideoInterface
   *   The called Brightcove Video.
   */
  public function setEconomics(string $is_economics): BrightcoveVideoInterface;

  /**
   * Returns the video file.
   *
   * @return array|null
   *   The video file entity.
   */
  public function getVideoFile(): ?array;

  /**
   * Sets the video file.
   *
   * @param array|null $video_file
   *   The video file entity.
   *
   * @return \Drupal\brightcove\BrightcoveVideoInterface
   *   The called Brightcove Video.
   */
  public function setVideoFile(?array $video_file): BrightcoveVideoInterface;

  /**
   * Returns the video URL.
   *
   * @return string|null
   *   The video URL.
   */
  public function getVideoUrl(): ?string;

  /**
   * Sets the video URL.
   *
   * @param string|null $video_url
   *   The video URL.
   *
   * @return $this
   */
  public function setVideoUrl(?string $video_url): BrightcoveVideoInterface;

  /**
   * Returns the video's profile.
   *
   * @return string|null
   *   The video profile.
   */
  public function getProfile(): ?string;

  /**
   * Sets the video's profile.
   *
   * @param string $profile
   *   Video's profile.
   *
   * @return \Drupal\brightcove\BrightcoveVideoInterface
   *   The called Brightcove Video.
   */
  public function setProfile(string $profile): BrightcoveVideoInterface;

  /**
   * Returns the video's poster image.
   *
   * @return array
   *   The poster image details on the entity.
   */
  public function getPoster(): array;

  /**
   * Sets the video's poster image.
   *
   * @param int|null $poster_id
   *   The poster image ID that needs to be saved on the entity.
   * @param int|null $height
   *   The height of the image.
   * @param int|null $width
   *   The width of the image.
   *
   * @return \Drupal\brightcove\BrightcoveVideoInterface
   *   The called Brightcove Video.
   */
  public function setPoster(?int $poster_id, ?int $height = NULL, ?int $width = NULL): BrightcoveVideoInterface;

  /**
   * Returns the video's thumbnail image.
   *
   * @return array
   *   The thumbnail image on the entity.
   */
  public function getThumbnail(): array;

  /**
   * Sets the video's thumbnail image.
   *
   * @param int|null $thumbnail_id
   *   The thumbnail image ID that needs to be saved on the entity.
   * @param int|null $height
   *   The height of the image.
   * @param int|null $width
   *   The width of the image.
   *
   * @return \Drupal\brightcove\BrightcoveVideoInterface
   *   The called Brightcove Video.
   */
  public function setThumbnail(?int $thumbnail_id, ?int $height = NULL, ?int $width = NULL): BrightcoveVideoInterface;

  /**
   * Returns the schedule starts at date.
   *
   * @return string|null
   *   The datetime of the schedule starts at date.
   */
  public function getScheduleStartsAt(): ?string;

  /**
   * Returns the custom field values.
   *
   * @return array
   *   Each field's value keyed by it's field ID.
   */
  public function getCustomFieldValues(): array;

  /**
   * Sets the custom field values.
   *
   * @param array $values
   *   Field values keyed by field's ID.
   *
   * @return \Drupal\brightcove\BrightcoveVideoInterface
   *   The called Brightcove Video.
   */
  public function setCustomFieldValues(array $values): BrightcoveVideoInterface;

  /**
   * Sets the video's schedule starts at date.
   *
   * @param string|null $schedule_starts_at
   *   The datetime of the schedule starts at date.
   *
   * @return \Drupal\brightcove\BrightcoveVideoInterface
   *   The called Brightcove Video.
   */
  public function setScheduleStartsAt(?string $schedule_starts_at): BrightcoveVideoInterface;

  /**
   * Returns the schedule ends at date.
   *
   * @return string|null
   *   The datetime of the schedule ends at date.
   */
  public function getScheduleEndsAt(): ?string;

  /**
   * Sets the video's schedule ends at date.
   *
   * @param string|null $schedule_ends_at
   *   The datetime of the schedule ends at date.
   *
   * @return \Drupal\brightcove\BrightcoveVideoInterface
   *   The called Brightcove Video.
   */
  public function setScheduleEndsAt(?string $schedule_ends_at): BrightcoveVideoInterface;

  /**
   * Gets Text Tracks.
   *
   * @return array
   *   A list of Text Tracks.
   */
  public function getTextTracks(): array;

  /**
   * Sets Text Tracks.
   *
   * @param array $text_tracks
   *   A list of Text Tracks.
   *
   * @return \Drupal\brightcove\BrightcoveVideoInterface
   *   The called Brightcove Video.
   */
  public function setTextTracks(array $text_tracks): BrightcoveVideoInterface;

  /**
   * Get marked fields for ingestion.
   *
   * @return array
   *   List of fields marked for ingestion.
   */
  public function getMarkedForIngestion(): array;

}
