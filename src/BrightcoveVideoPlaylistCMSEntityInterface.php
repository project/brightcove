<?php

declare(strict_types = 1);

namespace Drupal\brightcove;

/**
 * Provides a common interface for the Brightcove video and playlist entities.
 */
interface BrightcoveVideoPlaylistCMSEntityInterface extends BrightcoveCMSEntityInterface {

  /**
   * Returns the Brightcove ID.
   *
   * @return string|null
   *   The Brightcove ID (not the entity's).
   */
  public function getBrightcoveId(): ?string;

  /**
   * Sets The Brightcove ID.
   *
   * @param string $id
   *   The Brightcove ID (not the entity's).
   *
   * @return \Drupal\brightcove\BrightcoveVideoPlaylistCMSEntityInterface
   *   The called Brightcove entity.
   */
  public function setBrightcoveId(string $id);

  /**
   * Returns the player.
   *
   * @return int
   *   Target ID of the Brightcove Player.
   */
  public function getPlayer(): ?int;

  /**
   * Sets the player.
   *
   * @param int $player
   *   The Brightcove Player's entity ID.
   *
   * @return \Drupal\brightcove\BrightcoveVideoPlaylistCMSEntityInterface
   *   The called Brightcove Video or Playlist.
   */
  public function setPlayer(int $player): BrightcoveVideoPlaylistCMSEntityInterface;

  /**
   * Returns the reference ID.
   *
   * @return string|null
   *   Reference ID.
   */
  public function getReferenceId(): ?string;

  /**
   * Sets the reference ID.
   *
   * @param string|null $reference_id
   *   The reference ID.
   *
   * @return \Drupal\brightcove\BrightcoveVideoPlaylistCMSEntityInterface
   *   The called Brightcove Video or Playlist.
   */
  public function setReferenceId(?string $reference_id): BrightcoveVideoPlaylistCMSEntityInterface;

  /**
   * Returns the tags.
   *
   * @return string[]
   *   The list of tags.
   */
  public function getTags(): array;

  /**
   * Sets the tags.
   *
   * @param array $tags
   *   List of tags.
   *
   * @return \Drupal\brightcove\BrightcoveVideoPlaylistCMSEntityInterface
   *   The called Brightcove Video or Playlist.
   */
  public function setTags(array $tags): BrightcoveVideoPlaylistCMSEntityInterface;

  /**
   * Returns the entity published status indicator.
   *
   * Unpublished entities are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the entity is published.
   */
  public function isPublished(): bool;

  /**
   * Sets the published status of the entity.
   *
   * @param bool $published
   *   TRUE to set this entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\brightcove\BrightcoveVideoPlaylistCMSEntityInterface
   *   The called Brightcove Video or Playlist.
   */
  public function setPublished(bool $published);

}
