<?php

declare(strict_types = 1);

namespace Drupal\brightcove;

/**
 * Provides an interface defining a Brightcove custom field.
 */
interface BrightcoveCustomFieldInterface extends BrightcoveCMSEntityInterface {

  /**
   * Active status.
   */
  const STATUS_ACTIVE = 1;

  /**
   * Inactive status.
   */
  const STATUS_INACTIVE = 0;

  /**
   * Returns the Brightcove ID of the Custom Field.
   *
   * @return string|null
   *   Brightcove's Custom Field ID.
   */
  public function getCustomFieldId(): ?string;

  /**
   * Sets the Brightcove ID of the Custom Field.
   *
   * @param string $custom_field_id
   *   The ID of the Custom Field on Brightcove.
   *
   * @return \Drupal\brightcove\BrightcoveCustomFieldInterface
   *   The called Brightcove Custom Field.
   */
  public function setCustomFieldId(string $custom_field_id): BrightcoveCustomFieldInterface;

  /**
   * Returns enum values.
   *
   * @return array
   *   The enum values in array.
   */
  public function getEnumValues(): array;

  /**
   * Sets the enum values.
   *
   * @param array $enum_values
   *   The enum values array.
   *
   * @return \Drupal\brightcove\BrightcoveCustomFieldInterface
   *   The called Brightcove Custom Field.
   */
  public function setEnumValues(array $enum_values): BrightcoveCustomFieldInterface;

  /**
   * Returns whether the field is set to required or not.
   *
   * @return bool
   *   Whether the field is required or not.
   */
  public function isRequired(): bool;

  /**
   * Set the field's required value.
   *
   * @param bool $required
   *   TRUE if the field needs to be set, FALSE otherwise.
   *
   * @return \Drupal\brightcove\BrightcoveCustomFieldInterface
   *   The called Brightcove Custom Field.
   */
  public function setRequired(bool $required): BrightcoveCustomFieldInterface;

  /**
   * Returns the type of the field.
   *
   * @return string|null
   *   The type of the field, which can be either 'enum' or 'string'.
   */
  public function getType(): ?string;

  /**
   * Sets the type of the field.
   *
   * @param string $type
   *   The type of the field, it can be either 'enum' or 'string'.
   *
   * @return \Drupal\brightcove\BrightcoveCustomFieldInterface
   *   The called Brightcove Custom Field.
   */
  public function setType(string $type): BrightcoveCustomFieldInterface;

}
