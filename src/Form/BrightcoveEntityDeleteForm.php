<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Form;

use Brightcove\API\Exception\APIException;
use Drupal\brightcove\Services\LoggerInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds form for Brightcove content entity delete.
 */
class BrightcoveEntityDeleteForm extends ContentEntityDeleteForm {

  /**
   * Logger.
   *
   * @var \Drupal\brightcove\Services\LoggerInterface
   */
  private $logger;

  /**
   * Initializes an entity delete form.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Drupal\brightcove\Services\LoggerInterface $logger
   *   Logger.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, MessengerInterface $messenger, LoggerInterface $logger) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->messenger = $messenger;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('brightcove.logger')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getDeletionMessage(): TranslatableMarkup {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntity();

    if (!$entity->isDefaultTranslation()) {
      return $this->t('The @entity-type %label @language translation has been deleted.', [
        '@entity-type' => $entity->getEntityType()->getLabel(),
        '%label'       => $entity->label(),
        '@language'    => $entity->language()->getName(),
      ]);
    }

    return $this->t('The @entity-type %label has been deleted.', [
      '@entity-type' => $entity->getEntityType()->getLabel(),
      '%label' => $entity->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntity();

    if (!$entity->isDefaultTranslation()) {
      return $this->t('Are you sure you want to delete the @language translation of the @entity-type %label?', [
        '@language' => $entity->language()->getName(),
        '@entity-type' => $this->getEntity()->getEntityType()->getLabel(),
        '%label' => $this->getEntity()->label(),
      ]);
    }

    return $this->t('Are you sure you want to delete the @entity-type %label?', [
      '@entity-type' => $this->getEntity()->getEntityType()->getLabel(),
      '%label' => $this->getEntity()->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    try {
      parent::submitForm($form, $form_state);
    }
    catch (APIException $e) {
      $entity_type = $this->getEntity()->getEntityType()->getLabel();
      $arguments = [
        '@entity-type' => $entity_type instanceof TranslatableMarkup ? $entity_type->render() : $entity_type,
      ];

      if ($e->getCode() == 404) {
        $this->messenger->addWarning($this->t('The @entity-type was not found on Brightcove, only the local version was deleted.', $arguments));
        $this->logger->logException($e, '@entity-type not found, local version deleted.', $arguments, RfcLogLevel::WARNING);
        $form_state->setRedirectUrl($this->getRedirectUrl());
      }
      else {
        $arguments += [
          '@error' => $e->getMessage(),
        ];

        $this->messenger->addError($this->t('There was an error while trying to delete @entity-type: @error', $arguments));
        $this->logger->logException($e, 'Failed to delete @entity-type: @error', $arguments);

        $form_state->setRebuild(TRUE);
      }
    }
  }

}
