<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Form;

use Drupal\brightcove\BrightcoveUtil;
use Drupal\brightcove\Entity\Storage\PlayerStorageInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base form controller for Video and Playlist forms.
 */
abstract class BrightcoveVideoPlaylistForm extends ContentEntityForm {

  /**
   * The default API Client.
   *
   * @var string
   */
  protected $defaultAPIClient;

  /**
   * Player storage.
   *
   * @var \Drupal\brightcove\Entity\Storage\PlayerStorageInterface
   */
  protected $playerStorage;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   Entity repository.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\brightcove\Entity\Storage\PlayerStorageInterface $player_storage
   *   Player storage.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String translation.
   * @param string $defaultAPIClient
   *   The default API Client.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, PlayerStorageInterface $player_storage, MessengerInterface $messenger, TranslationInterface $string_translation, string $defaultAPIClient) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->playerStorage = $player_storage;
    $this->messenger = $messenger;
    $this->stringTranslation = $string_translation;
    $this->defaultAPIClient = $defaultAPIClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('entity_type.manager')->getStorage('brightcove_player'),
      $container->get('messenger'),
      $container->get('string_translation'),
      $container->get('brightcove.settings')->getDefaultApiClientId()
    );
  }

  /**
   * Helper function to alter the base form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Exception
   *   If the version for the given entity is cannot be checked.
   */
  public function alterForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\brightcove\Entity\BrightcoveVideoPlaylistCmsEntity $entity */
    $entity = $this->entity;
    $triggering_element = $form_state->getTriggeringElement();

    // Check for an updated version of the Video.
    if ($entity->id() && empty($triggering_element)) {
      BrightcoveUtil::checkUpdatedVersion($entity);
    }

    if ($entity->isNew()) {
      // Set default api client.
      if (!$form['api_client']['widget']['#default_value']) {
        $form['api_client']['widget']['#default_value'] = $this->defaultAPIClient;
      }

      // Update player list on client selection.
      $form['api_client']['widget']['#ajax'] = [
        'callback' => [$this, 'apiClientUpdateForm'],
        'event' => 'change',
        'wrapper' => 'player-ajax-wrapper',
      ];

      // Add ajax wrapper for player.
      $form['player']['widget']['#prefix'] = '<div id="' . $form['api_client']['widget']['#ajax']['wrapper'] . '">';
      $form['suffix']['widget']['#suffix'] = '</div>';
    }
    else {
      // Disable api client selection after the playlist is created.
      $form['api_client']['widget']['#disabled'] = TRUE;
    }

    // Change none option's name.
    $form['player']['widget']['#options'] = $this->getPlayerOptions($form, $form_state);
  }

  /**
   * Get the player options for the given api client.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The list of options for the player selection.
   */
  protected function getPlayerOptions(array $form, FormStateInterface $form_state): array {
    if (empty($form_state->getValue('api_client'))) {
      $api_client = $form['api_client']['widget']['#default_value'][0] ?? NULL;
    }
    else {
      $api_client = $form_state->getValue('api_client')[0]['target_id'];
    }

    return [
      '_none' => $this->t("Use API Client's default player"),
    ] + $this->playerStorage->getList([$api_client], TRUE);
  }

  /**
   * Ajax callback to update the player options list.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Ajax command response.
   */
  public function apiClientUpdateForm(array $form, FormStateInterface $form_state): AjaxResponse {
    $form['player']['widget']['#options'] = $this->getPlayerOptions($form, $form_state);

    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand(
      '#' . $form['api_client']['widget']['#ajax']['wrapper'],
      $form['player']
    ));

    return $response;
  }

}
