<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Form;

use Drupal\brightcove\Services\SettingsInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the form for managing Brightcove cron settings.
 */
class BrightcoveCronSettingsForm extends FormBase {

  /**
   * Module specific settings.
   *
   * @var \Drupal\brightcove\Services\SettingsInterface
   */
  private $settings;

  /**
   * Initializes a new cron settings form.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Drupal\brightcove\Services\SettingsInterface $settings
   *   Module specific settings.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String translation.
   */
  public function __construct(MessengerInterface $messenger, SettingsInterface $settings, TranslationInterface $string_translation) {
    $this->messenger = $messenger;
    $this->settings = $settings;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('messenger'),
      $container->get('brightcove.settings'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'brightcove_cron_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['disable_cron'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable cron'),
      '#description' => $this->t('Enabling this option will prevent a Brightcove-to-Drupal sync running from cron.'),
      '#default_value' => $this->settings->isCronDisabled(),
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#button_type' => 'primary',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->settings->setCronDisabled((bool) $form_state->getValue('disable_cron'));
    $this->messenger->addStatus($this->t('The settings have been saved.'));
  }

}
