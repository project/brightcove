<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Form;

use Brightcove\API\Client;
use Brightcove\API\CMS;
use Brightcove\API\Exception\APIException;
use Brightcove\API\Exception\AuthenticationException;
use Brightcove\API\PM;
use Drupal\brightcove\BrightcoveAPIClientInterface;
use Drupal\brightcove\BrightcoveUtil;
use Drupal\brightcove\Entity\BrightcoveSubscription;
use Drupal\brightcove\Services\SettingsInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form builder for API client.
 */
class BrightcoveAPIClientForm extends EntityForm {

  /**
   * API Client storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $apiClientStorage;

  /**
   * Module specific settings.
   *
   * @var \Drupal\brightcove\Services\SettingsInterface
   */
  protected $settings;

  /**
   * The brightcove_player storage.
   *
   * @var \Drupal\brightcove\Entity\Storage\PlayerStorageInterface
   */
  protected $playerStorage;

  /**
   * The player queue object.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $playerQueue;

  /**
   * The custom field queue object.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $customFieldQueue;

  /**
   * The subscriptions queue object.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $subscriptionsQueue;

  /**
   * Key/Value expirable store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected $keyValueExpirableStore;

  /**
   * Constructs a new BrightcoveAPIClientForm.
   *
   * @param \Drupal\brightcove\Services\SettingsInterface $settings
   *   Module specific settings.
   * @param \Drupal\Core\Entity\EntityStorageInterface $player_storage
   *   Player entity storage.
   * @param \Drupal\Core\Queue\QueueInterface $player_queue
   *   Player queue.
   * @param \Drupal\Core\Queue\QueueInterface $custom_field_queue
   *   Custom field queue.
   * @param \Drupal\Core\Queue\QueueInterface $subscriptions_queue
   *   Custom field queue.
   * @param \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface $key_value_expirable_store
   *   Key/Value expirable store for "brightcove_access_token".
   * @param \Drupal\Core\Entity\EntityStorageInterface $api_client_storage
   *   API Client storage.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String translation service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   */
  public function __construct(SettingsInterface $settings, EntityStorageInterface $player_storage, QueueInterface $player_queue, QueueInterface $custom_field_queue, QueueInterface $subscriptions_queue, KeyValueStoreExpirableInterface $key_value_expirable_store, EntityStorageInterface $api_client_storage, TranslationInterface $string_translation, MessengerInterface $messenger) {
    $this->settings = $settings;
    $this->playerStorage = $player_storage;
    $this->playerQueue = $player_queue;
    $this->customFieldQueue = $custom_field_queue;
    $this->subscriptionsQueue = $subscriptions_queue;
    $this->keyValueExpirableStore = $key_value_expirable_store;
    $this->apiClientStorage = $api_client_storage;
    $this->stringTranslation = $string_translation;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $entity_type_manager = $container->get('entity_type.manager');

    return new static(
      $container->get('brightcove.settings'),
      $entity_type_manager->getStorage('brightcove_player'),
      $container->get('queue')->get('brightcove_player_queue_worker'),
      $container->get('queue')->get('brightcove_custom_field_queue_worker'),
      $container->get('queue')->get('brightcove_subscriptions_queue_worker'),
      $container->get('brightcove.expirable_access_token_storage'),
      $entity_type_manager->getStorage('brightcove_api_client'),
      $container->get('string_translation'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\brightcove\Entity\BrightcoveAPIClient $brightcove_api_client */
    $brightcove_api_client = $this->entity;

    // Don't even try reporting the status/error message of a new client.
    if (!$brightcove_api_client->isNew()) {
      try {
        $brightcove_api_client->authorizeClient();
        $status = $brightcove_api_client->getClientStatus() ? $this->t('OK') : $this->t('Error');
      }
      catch (\Exception) {
        $status = $this->t('Error');
      }

      $form['status'] = [
        '#type' => 'item',
        '#title' => $this->t('Status'),
        '#markup' => $status,
      ];

      if ($brightcove_api_client->getClientStatus() === 0) {
        $form['status_message'] = [
          '#type' => 'item',
          '#title' => $this->t('Error message'),
          '#markup' => $brightcove_api_client->getClientStatusMessage(),
        ];
      }
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $brightcove_api_client->label(),
      '#description' => $this->t('A label to identify the API Client (authentication credentials).'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $brightcove_api_client->id(),
      '#machine_name' => [
        'exists' => '\Drupal\brightcove\Entity\BrightcoveAPIClient::load',
      ],
      '#disabled' => !$brightcove_api_client->isNew(),
    ];

    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Brightcove API Client ID'),
      '#description' => $this->t('The Client ID of the Brightcove API Authentication credentials, available <a href=":link" target="_blank">here</a>.', [':link' => 'https://studio.brightcove.com/products/videocloud/admin/oauthsettings']),
      '#maxlength' => 255,
      '#default_value' => $brightcove_api_client->getClientId(),
      '#required' => TRUE,
    ];

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Brightcove API Secret Key'),
      '#description' => $this->t('The Secret Key associated with the Client ID above, only visible once when Registering New Application.'),
      '#maxlength' => 255,
      '#default_value' => $brightcove_api_client->getSecretKey(),
      '#required' => TRUE,
    ];

    $form['account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Brightcove Account ID'),
      '#description' => $this->t('The number of one of the Brightcove accounts "selected for authorization" with the API Client above.'),
      '#maxlength' => 255,
      '#default_value' => $brightcove_api_client->getAccountId(),
      '#required' => TRUE,
    ];

    $form['default_player'] = [
      '#type' => 'select',
      '#title' => $this->t('Default player'),
      '#options' => $this->playerStorage->getlist([$brightcove_api_client->id()]),
      '#default_value' => $brightcove_api_client->getDefaultPlayer() ?: BrightcoveAPIClientInterface::DEFAULT_PLAYER,
      '#required' => TRUE,
    ];
    if ($brightcove_api_client->isNew()) {
      $form['default_player']['#description'] = $this->t('The rest of the players will be available after saving.');
    }

    // Count BrightcoveAPIClients.
    $api_clients_number = $this->apiClientStorage->getQuery()->accessCheck()->count()->execute();
    $form['default_client'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Default API Client'),
      '#description' => $this->t('Enable to make this the default API Client.'),
      '#default_value' => $api_clients_number === 0 || ($this->settings->getDefaultApiClientId() === $brightcove_api_client->id()),
      '#disabled' => $api_clients_number < 2,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    try {
      // Try to authorize client and save values on success.
      $client = Client::authorize($form_state->getValue('client_id'), $form_state->getValue('secret_key'));

      // Test account ID.
      $cms = new CMS($client, $form_state->getValue('account_id'));
      $cms->countVideos();

      $this->keyValueExpirableStore->setWithExpire($form_state->getValue('id'), $client->getAccessToken(), intval($client->getExpiresIn()) - 30);
    }
    catch (AuthenticationException $e) {
      $form_state->setErrorByName('client_id', $e->getMessage() ?: $this->t('Invalid credentials.'));
      $form_state->setErrorByName('secret_key');
    }
    catch (APIException $e) {
      $form_state->setErrorByName('account_id', $e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\brightcove\Entity\BrightcoveAPIClient $entity */
    $entity = $this->entity;

    $client = new Client($this->keyValueExpirableStore->get($form_state->getValue('id')));
    $cms = new CMS($client, $form_state->getValue('account_id'));

    /** @var \Brightcove\Item\CustomFields $video_fields */
    $video_fields = $cms->getVideoFields();
    $entity->setMaxCustomFields((int) $video_fields->getMaxCustomFields());

    foreach ($video_fields->getCustomFields() as $custom_field) {
      // Create queue item.
      $this->customFieldQueue->createItem([
        'api_client_id' => $this->entity->id(),
        'custom_field' => $custom_field,
      ]);
    }

    parent::submitForm($form, $form_state);

    if ($entity->isNew()) {
      // Get Players the first time when the API client is being saved.
      $pm = new PM($client, $form_state->getValue('account_id'));
      $player_list = $pm->listPlayers();
      $players = [];
      if (!empty($player_list) && !empty($player_list->getItems())) {
        $players = $player_list->getItems();
      }
      foreach ($players as $player) {
        // Create queue item.
        $this->playerQueue->createItem([
          'api_client_id' => $entity->id(),
          'player' => $player,
        ]);
      }

      // Get Custom fields the first time when the API client is being saved.
      /** @var \Brightcove\Item\CustomFields $video_fields */
      $video_fields = $cms->getVideoFields();
      foreach ($video_fields->getCustomFields() as $custom_field) {
        // Create queue item.
        $this->customFieldQueue->createItem([
          'api_client_id' => $entity->id(),
          'custom_field' => $custom_field,
        ]);
      }

      // Create new default subscription.
      $subscription = new BrightcoveSubscription(TRUE);
      $subscription->setStatus(FALSE)
        ->setApiClient($entity)
        ->setEndpoint(BrightcoveUtil::getDefaultSubscriptionUrl())
        ->setEvents(['video-change'])
        ->save();

      // Get Subscriptions the first time when the API client is being saved.
      $this->subscriptionsQueue->createItem($entity->id());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $brightcove_api_client = $this->entity;
    $status = $brightcove_api_client->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger->addStatus($this->t('Created the %label Brightcove API Client.', [
          '%label' => $brightcove_api_client->label(),
        ]));

        // Initialize batch.
        batch_set([
          'title' => $this->t('Brightcove sync'),
          'operations' => [
            [
              [BrightcoveUtil::class, 'runQueue'], ['brightcove_player_queue_worker'],
            ],
            [
              [BrightcoveUtil::class, 'runQueue'], ['brightcove_custom_field_queue_worker'],
            ],
            [
              [BrightcoveUtil::class, 'runQueue'], ['brightcove_subscriptions_queue_worker'],
            ],
            [
              [BrightcoveUtil::class, 'runQueue'], ['brightcove_subscription_queue_worker'],
            ],
          ],
        ]);
        break;

      case SAVED_UPDATED:
        $this->messenger->addStatus($this->t('Saved the %label Brightcove API Client.', [
          '%label' => $brightcove_api_client->label(),
        ]));
        break;

      default:
        $this->messenger->addError($this->t('There was an error saving the Brightcove API Client.'));
        $form_state->setRebuild(TRUE);
        return;
    }

    if ($form_state->getValue('default_client')) {
      $this->settings->setDefaultApiClientId($brightcove_api_client->id());
    }

    $form_state->setRedirectUrl($brightcove_api_client->toUrl('collection'));
  }

}
