<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Form;

use Drupal\brightcove\BrightcoveCustomFieldInterface;
use Drupal\brightcove\Entity\BrightcoveVideo;
use Drupal\brightcove\Entity\Storage\PlayerStorageInterface;
use Drupal\brightcove\Entity\Storage\VideoStorageInterface;
use Drupal\brightcove\Services\Exception\IngestionException;
use Drupal\brightcove\Services\LoggerInterface;
use Drupal\brightcove\Services\SessionManagerInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Brightcove Video edit forms.
 */
class BrightcoveVideoForm extends BrightcoveVideoPlaylistForm {

  use DependencySerializationTrait;

  /**
   * Custom field storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $customFieldStorage;

  /**
   * Video entity.
   *
   * @var \Drupal\brightcove\Entity\BrightcoveVideo
   */
  protected $entity;

  /**
   * Logger.
   *
   * @var \Drupal\brightcove\Services\LoggerInterface
   */
  private $logger;

  /**
   * Module specific session manager.
   *
   * @var \Drupal\brightcove\Services\SessionManagerInterface
   */
  private $sessionManager;

  /**
   * Video storage.
   *
   * @var \Drupal\brightcove\Entity\Storage\VideoStorageInterface
   */
  private $videoStorage;

  /**
   * Initializes a new Brightcove Video form builder.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   Entity repository.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\brightcove\Entity\Storage\PlayerStorageInterface $player_storage
   *   Player storage.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String translation.
   * @param string $defaultAPIClient
   *   The default API Client.
   * @param \Drupal\Core\Entity\EntityStorageInterface $custom_field_storage
   *   Custom field storage.
   * @param \Drupal\brightcove\Entity\Storage\VideoStorageInterface $video_storage
   *   Video storage.
   * @param \Drupal\brightcove\Services\LoggerInterface $logger
   *   Logger.
   * @param \Drupal\brightcove\Services\SessionManagerInterface $session_manager
   *   Module specific session manager.
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    PlayerStorageInterface $player_storage,
    MessengerInterface $messenger,
    TranslationInterface $string_translation,
    string $defaultAPIClient,
    EntityStorageInterface $custom_field_storage,
    VideoStorageInterface $video_storage,
    LoggerInterface $logger,
    SessionManagerInterface $session_manager,
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time, $player_storage, $messenger, $string_translation, $defaultAPIClient);

    $this->customFieldStorage = $custom_field_storage;
    $this->videoStorage = $video_storage;
    $this->logger = $logger;
    $this->sessionManager = $session_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): BrightcoveVideoPlaylistForm {
    $entity_type_manager = $container->get('entity_type.manager');

    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $entity_type_manager->getStorage('brightcove_player'),
      $container->get('messenger'),
      $container->get('string_translation'),
      $container->get('brightcove.settings')->getDefaultApiClientId(),
      $entity_type_manager->getStorage('brightcove_custom_field'),
      $entity_type_manager->getStorage('brightcove_video'),
      $container->get('brightcove.logger'),
      $container->get('brightcove.session_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $this->alterForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function alterForm(array &$form, FormStateInterface $form_state): void {
    parent::alterForm($form, $form_state);

    $form['#attached']['library'][] = 'brightcove/brightcove.video';
    $is_new = $this->entity->isNew();

    // Get api client from the form settings.
    if (!empty($form_state->getValue('api_client'))) {
      $api_client = $form_state->getValue('api_client')[0]['target_id'];
    }
    else {
      $api_client = $form['api_client']['widget']['#default_value'];
    }

    if (is_array($api_client)) {
      $api_client = reset($api_client) ?: '';
    }

    // Remove _none value to make sure the first item is selected.
    if (isset($form['profile']['widget']['#options']['_none'])) {
      unset($form['profile']['widget']['#options']['_none']);
    }

    // Get the correct profiles' list for the selected api client.
    if ($is_new) {
      // Class this class's update form method instead of the supper class's.
      $form['api_client']['widget']['#ajax']['callback'] = [
        $this, 'apiClientUpdateForm',
      ];

      // Add ajax wrapper for profile.
      $form['profile']['widget']['#ajax_id'] = 'ajax-profile-wrapper';
      $form['profile']['widget']['#prefix'] = '<div id="' . $form['profile']['widget']['#ajax_id'] . '">';
      $form['profile']['widget']['#suffix'] = '</div>';

      // Update allowed values for profile.
      $form['profile']['widget']['#options'] = BrightcoveVideo::getProfileAllowedValues($api_client);
    }

    // Set default profile.
    if (!$form['profile']['widget']['#default_value']) {
      $profile_keys = array_keys($form['profile']['widget']['#options']);
      $form['profile']['widget']['#default_value'] = reset($profile_keys);
    }

    // Add pseudo title for status field.
    $form['status']['pseudo_title'] = [
      '#markup' => $this->t('Status'),
      '#prefix' => '<div id="status-pseudo-title">',
      '#suffix' => '</div>',
      '#weight' => -100,
    ];

    $upload_type = [
      '#type' => 'select',
      '#title' => $this->t('Upload type'),
      '#options' => [
        'file' => $this->t('File'),
        'url' => $this->t('URL'),
      ],
      '#default_value' => !empty($form['video_url']['widget'][0]['value']['#default_value']) ? 'url' : 'file',
      '#weight' => -100,
    ];

    $form['video_file']['#states'] = [
      'visible' => [
        'select[name="upload_type"]' => ['value' => 'file'],
      ],
    ];

    $form['video_url']['#states'] = [
      'visible' => [
        'select[name="upload_type"]' => ['value' => 'url'],
      ],
    ];

    // Group video fields together.
    $form['video'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Video'),
      '#weight' => $form['economics']['#weight'] += 0.001,
      'upload_type' => &$upload_type,
      'video_file' => $form['video_file'],
      'video_url' => $form['video_url'],
      'profile' => $form['profile'],
    ];
    unset($form['video_file']);
    unset($form['video_url']);
    unset($form['profile']);

    // Group image fields together.
    $form['images'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Images'),
      '#weight' => $form['video']['#weight'] += 0.001,
      '#description' => $this->t('For best results, use JPG or PNG format with a minimum width of 640px for video stills and 160px for thumbnails. Aspect ratios should match the video, generally 16:9 or 4:3. <a href=":link" target="_blank">Read More</a>', [':link' => 'https://support.brightcove.com/en/video-cloud/docs/uploading-video-still-and-thumbnail-images']),
      'poster' => $form['poster'],
      'thumbnail' => $form['thumbnail'],
    ];
    unset($form['poster']);
    unset($form['thumbnail']);

    // Group scheduling fields together.
    $form['availability'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Availability'),
      '#weight' => $form['images']['#weight'] += 0.001,
      'schedule_starts_at' => $form['schedule_starts_at'],
      'schedule_ends_at' => $form['schedule_ends_at'],
    ];
    unset($form['schedule_starts_at']);
    unset($form['schedule_ends_at']);

    /** @var \Drupal\brightcove\Entity\BrightcoveCustomField[] $custom_fields */
    $custom_fields = $this->customFieldStorage->loadByProperties([
      'api_client' => $api_client,
      'status' => BrightcoveCustomFieldInterface::STATUS_ACTIVE,
    ]);

    // Add ajax wrapper for custom fields.
    if ($is_new) {
      $form['custom_fields']['#ajax_id'] = 'ajax-custom-fields-wrapper';
      $form['custom_fields']['#prefix'] = '<div id="' . $form['custom_fields']['#ajax_id'] . '">';
      $form['custom_fields']['#suffix'] = '</div>';
    }

    // Show custom fields.
    if (count($custom_fields) > 0) {
      $form['custom_fields']['#type'] = 'details';
      $form['custom_fields']['#title'] = $this->t('Custom fields');
      $form['custom_fields']['#weight'] = $form['availability']['#weight'] += 0.001;

      $has_required = FALSE;
      $custom_field_values = $this->entity->getCustomFieldValues();
      foreach ($custom_fields as $custom_field) {
        // Indicate whether that the custom fields has required field(s) or
        // not.
        if (!$has_required && $custom_field->isRequired()) {
          $has_required = TRUE;
        }

        switch ($custom_field_type = $custom_field->getType()) {
          case $custom_field::TYPE_STRING:
            $type = 'textfield';
            break;

          case $custom_field::TYPE_ENUM:
            $type = 'select';
            break;

          default:
            continue 2;
        }

        // Assemble form field for the custom field.
        $custom_field_id = $custom_field->getCustomFieldId();
        $custom_field_name = "custom_field_{$custom_field_id}";
        $form['custom_fields'][$custom_field_name] = [
          '#type' => $type,
          '#title' => $custom_field->getName(),
          '#description' => $custom_field->getDescription(),
          '#required' => $custom_field->isRequired(),
        ];

        // Set custom field value if it is set.
        if (isset($custom_field_values[$custom_field_id])) {
          $form['custom_fields'][$custom_field_name]['#default_value'] = $custom_field_values[$custom_field_id];
        }

        // Add options for enum types.
        if ($custom_field_type === $custom_field::TYPE_ENUM) {
          $options = [];

          // Add none option if the field is not required.
          if (!$form['custom_fields'][$custom_field_name]['#required']) {
            $options[''] = $this->t('- None -');
          }

          foreach ($custom_field->getEnumValues() as $enum) {
            $options[$enum['value']] = $enum['value'];
          }
          $form['custom_fields'][$custom_field_name]['#options'] = $options;
        }
      }

      // Show custom field group opened if it has at least one required field.
      if ($has_required) {
        $form['custom_fields']['#open'] = TRUE;
      }
    }

    $form['text_tracks']['widget']['actions']['ief_add']['#value'] = $this->t('Add Text Track');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    $is_new = $this->entity->isNew();
    switch ($form_state->getValue('upload_type')) {
      case 'file':
        if ($is_new || !empty($form_state->getValue('video_file')[0]['fids'])) {
          $form_state->unsetValue('video_url');
          $this->entity->setVideoUrl(NULL);
        }
        break;

      case 'url':
        if ($is_new) {
          $form_state->unsetValue('video_file');
        }
        elseif (!empty($form_state->getValue('video_url')[0]['value'])) {
          $form_state->unsetValue('video_file');
          $this->entity->setVideoFile(NULL);
        }
        break;
    }
  }

  /**
   * Sets the custom fields on the entity.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function setCustomFields(array $form, FormStateInterface $form_state): void {
    // Save custom field values.
    $custom_field_values = [];
    if (!empty($form['custom_fields'])) {
      foreach (Element::children($form['custom_fields']) as $field_name) {
        if (preg_match('/^custom_field_(.+)$/i', $field_name, $matches) === 1) {
          $custom_field_values[$matches[1]] = $form_state->getValue($field_name);
        }
      }
      $this->entity->setCustomFieldValues($custom_field_values);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    try {
      $this->setCustomFields($form, $form_state);
      $status = $this->entity->save(TRUE);

      switch ($status) {
        case SAVED_NEW:
          $this->messenger->addStatus($this->t('Created the %label Brightcove Video.', [
            '%label' => $this->entity->label(),
          ]));
          break;

        default:
          $this->messenger->addStatus($this->t('Saved the %label Brightcove Video.', [
            '%label' => $this->entity->label(),
          ]));
      }
      $form_state->setRedirect('entity.brightcove_video.canonical', [
        'brightcove_video' => $this->entity->id(),
      ]);
    }
    catch (IngestionException $e) {
      $previous = $e->getPrevious();
      $this->messenger->addError($this->t('Failed to create ingestion request for Video assets: @error.<br><b>Newly uploaded assets (images, text tracks) may have been removed.</b>', [
        '@error' => $previous->getMessage(),
      ]));
      $this->handleException($previous, 'Failed to create ingestion request.', $form_state);
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
      $this->handleException($e, 'Failed to upload the Video.', $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function apiClientUpdateForm(array $form, FormStateInterface $form_state): AjaxResponse {
    $response = parent::apiClientUpdateForm($form, $form_state);

    // Update profile field.
    $response->addCommand(new ReplaceCommand(
      '#' . $form['profile']['widget']['#ajax_id'],
      $form['profile']
    ));

    // Update custom fields.
    $response->addCommand(new ReplaceCommand(
      '#' . $form['custom_fields']['#ajax_id'],
      $form['custom_fields']
    ));

    return $response;
  }

  /**
   * Handles an exception.
   *
   * @param \Throwable $e
   *   The thrown exception.
   * @param string $subject
   *   Subject for the exception.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  private function handleException(\Throwable $e, string $subject, FormStateInterface $form_state): void {
    $this->logger->logException($e, $subject);

    $entity_id = $this->entity->id();
    if (!empty($entity_id)) {
      $this->sessionManager->setIgnoreDestination(TRUE);
      $form_state->setRedirect('entity.brightcove_video.edit_form', [
        'brightcove_video' => $entity_id,
      ]);
    }
  }

}
