<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Form;

use Brightcove\API\Exception\APIException;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the form for Brightcove Subscription delete.
 */
class BrightcoveSubscriptionDeleteForm extends ConfirmFormBase {

  /**
   * Brightcove Subscription object.
   *
   * @var \Drupal\brightcove\Entity\BrightcoveSubscription
   */
  protected $brightcoveSubscription;

  /**
   * Initializes a subscription deletion form.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String translation.
   */
  public function __construct(MessengerInterface $messenger, TranslationInterface $string_translation) {
    $this->brightcoveSubscription = $this->getRequest()->get('brightcove_subscription');
    $this->messenger = $messenger;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('messenger'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'brightcove_subscription_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure that you want to delete the subscription with the %endpoint endpoint?', [
      '%endpoint' => $this->brightcoveSubscription->getEndpoint(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return Url::fromRoute('entity.brightcove_subscription.list');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    // Prevent deletion of the default Subscription entity.
    if (!empty($this->brightcoveSubscription) && $this->brightcoveSubscription->isDefault()) {
      $this->messenger->addError($this->t('The API client default Subscription cannot be deleted.'));
      $form_state->setRedirect('entity.brightcove_subscription.list');
      return [];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    try {
      $this->brightcoveSubscription->delete(FALSE);
    }
    catch (APIException $e) {
      $this->messenger->addError($e->getMessage());
    }

    $this->messenger->addStatus($this->t('Subscription has been successfully deleted.'));
    $form_state->setRedirect('entity.brightcove_subscription.list');
  }

}
