<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Form;

use Drupal\brightcove\Entity\BrightcoveSubscription;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the form for Brightcove Subscription add, edit.
 */
class BrightcoveSubscriptionForm extends FormBase {

  /**
   * API Client storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $apiClientStorage;

  /**
   * Initializes a subscription form.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $api_client_storage
   *   API Client storage.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String translation.
   */
  public function __construct(EntityStorageInterface $api_client_storage, MessengerInterface $messenger, TranslationInterface $string_translation) {
    $this->apiClientStorage = $api_client_storage;
    $this->messenger = $messenger;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity_type.manager')->getStorage('brightcove_api_client'),
      $container->get('messenger'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'brightcove_subscription_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\brightcove\Entity\BrightcoveAPIClient[] $api_clients */
    $api_clients = $this->apiClientStorage->loadMultiple();
    $api_client_options = [];
    foreach ($api_clients as $api_client) {
      $api_client_options[$api_client->id()] = $api_client->label();
    }

    $form['api_client_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Client'),
      '#options' => $api_client_options,
      '#required' => TRUE,
    ];

    if (empty($api_client_options)) {
      $form['api_client_id']['#empty_option'] = $this->t('No API clients available');
    }
    elseif (empty($form['api_client_id']['#default'])) {
      $api_client_ids = array_keys($api_client_options);
      $default = reset($api_client_ids);
      $form['api_client_id']['#default_value'] = $default;
    }

    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#description' => $this->t('The notifications endpoint.'),
      '#required' => TRUE,
    ];

    // Hard-code "video-change" event since it's the only one.
    $form['events'] = [
      '#type' => 'checkboxes',
      '#required' => TRUE,
      '#title' => 'Events',
      '#options' => [
        'video-change' => $this->t('Video change'),
      ],
      '#default_value' => ['video-change'],
      '#disabled' => TRUE,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // Validate endpoint, it should be unique.
    if (!empty(BrightcoveSubscription::loadByEndpoint($form_state->getValue('endpoint')))) {
      $form_state->setErrorByName('endpoint', $this->t('A subscription with the %endpoint endpoint already exists.', ['%endpoint' => $form_state->getValue('endpoint')]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    try {
      $brightcove_subscription = BrightcoveSubscription::createFromArray([
        'api_client_id' => $form_state->getValue('api_client_id'),
        'endpoint' => $form_state->getValue('endpoint'),
        'events' => array_values($form_state->getValue('events')),
      ]);
      $brightcove_subscription->save(TRUE);

      $this->messenger->addStatus($this->t('Created Brightcove Subscription with %endpoint endpoint.', [
        '%endpoint' => $brightcove_subscription->getEndpoint(),
      ]));
    }
    catch (\Exception $e) {
      // In case of an exception, show an error message and rebuild the form.
      if ($e->getMessage()) {
        $this->messenger->addError($this->t('Failed to create subscription: %error', ['%error' => $e->getMessage()]));
      }
      else {
        $this->messenger->addError($this->t('Failed to create subscription.'));
      }

      $form_state->setRebuild(TRUE);
    }

    // Redirect back to the Subscriptions list.
    $form_state->setRedirect('entity.brightcove_subscription.list');
  }

}
