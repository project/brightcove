<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Form;

use Drupal\brightcove\Services\Exception\IngestionException;
use Drupal\brightcove\Services\LoggerInterface;
use Drupal\brightcove\Services\SessionManagerInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\inline_entity_form\Form\EntityInlineForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Extends the inline form to pass along TRUE to the video/playlist save method.
 */
final class BrightcoveInlineForm extends EntityInlineForm {

  /**
   * Initializes a Brightcove inline form.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Entity type.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   Theme manager.
   * @param \Drupal\brightcove\Services\LoggerInterface $logger
   *   Logger.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Drupal\brightcove\Services\SessionManagerInterface $sessionManager
   *   Session manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String translation.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time.
   */
  public function __construct(
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
    EntityTypeInterface $entity_type,
    ThemeManagerInterface $themeManager,
    private readonly LoggerInterface $logger,
    private readonly MessengerInterface $messenger,
    private readonly SessionManagerInterface $sessionManager,
    TranslationInterface $string_translation,
    private readonly TimeInterface $time,
  ) {
    parent::__construct($entity_field_manager, $entity_type_manager, $module_handler, $entity_type, $themeManager);

    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type): self {
    return new self(
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $entity_type,
      $container->get('theme.manager'),
      $container->get('brightcove.logger'),
      $container->get('messenger'),
      $container->get('brightcove.session_manager'),
      $container->get('string_translation'),
      $container->get('datetime.time')
    );
  }

  /**
   * Helper function to get the base entity form.
   *
   * @param array $entity_form
   *   An associative array containing the structure of the entity form.
   */
  private function getBaseEntityForm(array $entity_form): EntityFormInterface {
    $form_base = $this->entityTypeManager->getFormObject($this->entityType->id(), $entity_form['#op']);
    $form_base->setEntity($entity_form['#entity']);
    return $form_base;
  }

  /**
   * {@inheritdoc}
   */
  public function entityForm(array $entity_form, FormStateInterface $form_state): array {
    $parent = parent::entityForm($entity_form, $form_state);
    // Apply the same form alteration on the inline form as for the entity form.
    $this->getBaseEntityForm($entity_form)->alterForm($parent, $form_state);
    return $parent;
  }

  /**
   * {@inheritdoc}
   */
  public function entityFormSubmit(array &$entity_form, FormStateInterface $form_state): void {
    // Set custom field values from the form.
    $ief_parents = $entity_form['#parents'];
    $first_ief_parent = array_shift($ief_parents);
    $ief_values = $form_state->getValue($first_ief_parent);
    foreach ($ief_parents as $parent) {
      $ief_values = $ief_values[$parent];
    }
    $ief_form_state = new FormState();
    $ief_form_state->setValues($ief_values['custom_fields']);
    $this->getBaseEntityForm($entity_form)->setCustomFields($entity_form, $ief_form_state);

    parent::entityFormSubmit($entity_form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(EntityInterface $entity): void {
    try {
      // Update changed date then save the form.
      $entity->setChangedTime($this->time->getRequestTime());
      $entity->save(TRUE);
    }
    catch (IngestionException $e) {
      $previous = $e->getPrevious();
      $this->messenger->addError($this->t('Failed to create ingestion request for Video assets: @error.<br><b>Newly uploaded assets (images, text tracks) may have been removed.</b>', [
        '@error' => $previous->getMessage(),
      ]));
      $this->handleException($previous, 'Failed to create ingestion request.', $entity);
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
      $this->handleException($e, 'Failed to upload the Video.', $entity);
    }
  }

  /**
   * Handles an exception.
   *
   * @param \Throwable $e
   *   The thrown exception.
   * @param string $subject
   *   Subject for the exception.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The related entity.
   */
  private function handleException(\Throwable $e, string $subject, EntityInterface $entity): void {
    $this->logger->logException($e, $subject);

    $entity_id = $entity->id();
    if (!empty($entity_id)) {
      $this->sessionManager->setIgnoreDestination(TRUE);
    }
  }

}
