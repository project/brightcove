<?php

declare(strict_types = 1);

namespace Drupal\brightcove\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Builds form for playlist settings.
 */
class BrightcovePlaylistSettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'BrightcovePlaylist_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Empty implementation of the abstract submit class.
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['BrightcovePlaylist_settings']['#markup'] = 'Settings form for Brightcove Playlists. Manage field settings here.';
    return $form;
  }

}
